﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Painter.Utils;
using Photon.Realtime;
using Painter.VR.Input;

namespace Painter.Communication
{
    [RequireComponent(typeof(PhotonView))]
    public abstract class Highlightable : Validator
    {
        protected PhotonView photonView;

        // Start is called before the first frame update
        protected virtual void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }

        // Defines a custom behaviour for enabling and disabling the highlight
        protected abstract void Update();

        // Calls an RPC method to highlight objects in other clients
        public override void ExecuteAction()
        {
            photonView.RPC("Highlight", RpcTarget.All);
        }
        
        protected abstract void Highlight(PhotonMessageInfo info);
    }
}
