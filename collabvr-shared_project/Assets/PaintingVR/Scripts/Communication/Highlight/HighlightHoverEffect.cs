﻿using Painter.Utils;
using UnityEngine;

namespace Painter.Communication
{
    public class HighlightHoverEffect :
            HighlightEffect
    {
        public override void Create(GameObject parent)
        {
            m_finished = false;
            // Get components from original object
            var renderer = parent.GetComponent<MeshRenderer>();

#if DEBUG
            // Don't spawn a new highlight object 
            // if one already exists
            if (renderer == null)
            {
                Debug.LogError("<b>HighlightHoverEffect</b>: no mesh to copy.");
                return;
            }
#endif

            MeshMethods.SpawnCopyMesh(parent, out m_renderer);

            // Change this object's material to use a different shader
            // Do this by changing the material or the shader
            m_renderer.material = effect;
        }

        public override void Update()
        {}

        public override void Destroy()
        {
            Object.Destroy(m_renderer.gameObject);
            m_finished = true;
        }
    }
}
