﻿using Painter.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Communication
{
    public class HighlightReceivedEffect :
            HighlightEffect
    {
        private Collider m_collider;
        private float m_time;

        [Header("Highlight Effect Parameters")]
        [Range(0.01f, 1.0f)]
        public float fovRatio = 0.8f;
        [Range(0.01f, 10.0f)]
        public float decrementRatio = 0.1f;
        [Range(0.01f, 10.0f)]
        public float incrementRatio = 0.2f;
        
        public override void Create(GameObject parent)
        {
            m_finished = false;
            // Get components from original object
            m_collider = parent.GetComponent<Collider>();
            // Update the timer
            m_time = 0.0f;

#if DEBUG
            // Don't spawn a new highlight object 
            // if one already exists
            var renderer = parent.GetComponent<MeshRenderer>();
            if (renderer == null)
            {
                Debug.LogError("<b>HighlightReceivedEffect</b>: no mesh to copy.");
                return;
            }
#endif

            // Make a copy of the parent object
            CopyMesh(parent);
            // Call atention to highlighted object
            HighlightController.Instance.CallAtentionTo(transform);
        }

        public override void Update()
        {
            if (Finished)
                return;
            UpdateHiglightEffect();
        }

        public override void Destroy()
        {
            // Remove atention from highlighted object
            HighlightController.Instance.RemoveAttentionTo(transform);
            
            DestroyCopyMesh();

            m_finished = true;
        }

        #region Highlight Effect methods

        private void CopyMesh(GameObject parent)
        {
            MeshMethods.SpawnCopyMesh(parent, out m_renderer);

            // Change this object's material to use a different shader
            // Do this by changing the material or the shader
            m_renderer.material = effect;
        }

        private void UpdateHiglightEffect()
        {
            // If m_time is equal or greater than 1
            // destroy the copy object
            if (m_time >= 1.0f)
            {
                Destroy();
                return;
            }
            else
            {
                // m_time will increment if the object is within
                // the camera's fov
                // and will decrement when it is not

                bool is_within_fov = MathCalculations.IsWithinCameraFOV(Camera.main, m_collider.bounds.center, fovRatio);
                m_time += (is_within_fov ? incrementRatio : -decrementRatio) * Time.deltaTime;
                m_time = m_time < 0.0f ? 0.0f : m_time;
            }

            // Update the material in the copy object
            m_renderer.material.SetFloat("_MTime", m_time);
        }

        private void DestroyCopyMesh()
        {
            Destroy(m_renderer.gameObject);
            m_renderer = null;
        }

        #endregion

    }
}
