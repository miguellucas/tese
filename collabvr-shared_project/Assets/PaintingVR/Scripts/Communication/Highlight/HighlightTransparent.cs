﻿using UnityEngine;

namespace Painter.Communication
{
    public class HighlightTransparent : MonoBehaviour
    {
        public Transform highlight;

        private Material m_oldMaterial;
        public Material transparentMaterial;

        private Material m_material;
        
        private bool m_isTransparent;
        
        private void Awake()
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            m_oldMaterial = meshRenderer.sharedMaterial;
        }

        public void SetTransparent(bool active)
        {
            if (active)
            {
                OnTransparency();
            }
            else
            {
                OnRevert();
            }
        }


        private void OnTransparency()
        {
            if (m_isTransparent)
                return;
            
            var renderer = GetComponent<MeshRenderer>();
            if(renderer == null || renderer.material == null)
            {
                Destroy(this);
                return;
            }

            var meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.material = transparentMaterial;

            m_material = meshRenderer.material;

            m_isTransparent = true;
        }

        private void OnRevert()
        {
            GetComponent<MeshRenderer>().sharedMaterial = m_oldMaterial;
            m_isTransparent = false;
        }

        private Vector4 ToEyeScreenPosition(Transform objTransform, Camera.MonoOrStereoscopicEye eye)
        {
            return Camera.main.WorldToViewportPoint(objTransform.position, eye);
        }

        private void SetEyeScreenPosition(Camera.MonoOrStereoscopicEye eye)
        {
            string variableName;
            Vector4 screenPosition =
                ToEyeScreenPosition(highlight, eye);

            if (eye == Camera.MonoOrStereoscopicEye.Left ||
                eye == Camera.MonoOrStereoscopicEye.Mono)
            {
                variableName = "_LeftScreenPosition";
            }
            else
            {
                variableName = "_RightScreenPosition";
            }

            m_material.SetVector(variableName, screenPosition);
        }

        private void SetMaterialVariables()
        {
            if (Camera.main.stereoEnabled)
            {
                SetEyeScreenPosition(Camera.MonoOrStereoscopicEye.Left);
                SetEyeScreenPosition(Camera.MonoOrStereoscopicEye.Right);
            }
            else
            {
                SetEyeScreenPosition(Camera.MonoOrStereoscopicEye.Mono);
            }

            m_material.SetFloat("_ScreenMinDistance", 0.1f);
            m_material.SetFloat("_ScreenMaxDistance", 0.15f);
        }

        // Update is called once per frame
        private void LateUpdate()
        {
            if(m_isTransparent)
            {
                SetMaterialVariables();
            }
        }
    }
}
