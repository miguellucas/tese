﻿using Painter.Core;
using Painter.Utils;
using Painter.VR.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Communication
{
    [RequireComponent(typeof(Collider))]
    public class HighlightValidator : Validator
    {
        #region Singleton
        private static HighlightValidator instance;
        public static HighlightValidator Instance
        {
            get
            {
                return instance;
            }
        }

        public HighlightValidator()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>HighlightValidator</b>: Multiple instances of HighlightValidator are not supported.");
                return;
            }
#endif

            instance = this;
        }
        #endregion

        private Highlightable m_validHighlight;

        public override bool Validate(RaycastHit hit)
        {
            var highlitable = hit.collider.gameObject.GetComponent<Highlightable>();

            if (m_validHighlight != null && 
                (highlitable == null || highlitable != m_validHighlight))
            {
                m_validHighlight.Show(false);
            }

            if (highlitable != null && highlitable.Validate(hit))
            { 
                m_validHighlight = highlitable;
                return true;
            }

            m_validHighlight = null;
            return false;
        }

        public override void ExecuteAction()
        {
            m_validHighlight.ExecuteAction();
        }

        public override void Show(bool show, bool isAttached = false)
        {
            if (m_validHighlight != null)
            {
                m_validHighlight.Show(show, isAttached);
            }
        }

        public override Vector3 GetAttachmentPoint()
        {
            return m_validHighlight.GetAttachmentPoint();
        }
    }
}
