﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Painter.Utils;
using Photon.Realtime;

namespace Painter.Communication
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(MeshRenderer))]
    public class HighlightAlwaysVisible : Highlightable
    {
        [Header("Pointer variables")]
        public Transform attachmentPoint;

        [Space]
        [Header("Highlight Effects")]
        public HighlightEffect effectOnHoverHighlight;
        public HighlightEffect effectOnReceiveHighlight;
        public HighlightEffect effectOnSendHighlight;

        [Space]

        [Header("Audio clips")]
        public GameObject highlightAudio;
        public GameObject selectedAudio;
        public GameObject hoverAudio;

        private bool m_isShowing = false;

        public HighlightAlwaysVisible()
        {}


        protected override void Awake()
        {
            base.Awake();
        }

        protected void OnDestroy()
        {
            if(!effectOnHoverHighlight.Finished)
            {
                effectOnHoverHighlight.Destroy();
            }
            if (!effectOnSendHighlight.Finished)
            {
                effectOnSendHighlight.Destroy();
            }
            if (!effectOnReceiveHighlight.Finished)
            {
                effectOnReceiveHighlight.Destroy();
            }
        }


        public override bool Validate(RaycastHit hit)
        {
            return true;
        }
        
        public override Vector3 GetAttachmentPoint()
        {
            return attachmentPoint.position;
        }

        public override void Show(bool show, bool isAttached = false)
        {
            if (show && isAttached)
            {
                if (m_isShowing)
                    return;

                m_isShowing = true;
                CreateEffect(effectOnHoverHighlight);

                var tmpObj = Instantiate(hoverAudio, transform);
                AudioSource source = tmpObj.GetComponent<AudioSource>();
#if DEBUG
                if (source == null)
                {
                    Debug.LogError("<b>HighlightAlwaysVisible</b>: audio source does not exist in the call prefab object");
                    return;
                }
#endif
                StartCoroutine(WaitAudioOver(source));
            }
            else
            {
                if (!m_isShowing)
                    return;

                m_isShowing = false;
                effectOnHoverHighlight.Destroy();
            }
        }

        public override void ExecuteAction()
        {
            // Reverse receive effect if it has not finished yet
            if(!effectOnReceiveHighlight.Finished)
            {
                effectOnReceiveHighlight.Destroy();
                return;
            }

            photonView.RPC("Highlight", RpcTarget.All);
        }

        [PunRPC]
        protected override void Highlight(PhotonMessageInfo info)
        {
            // Reverse receive effect if it has not finished yet
            var effect = GetEffect(PhotonNetwork.LocalPlayer == info.Sender);
            if (PhotonNetwork.LocalPlayer != info.Sender && !effect.Finished)
            {
                effect.Destroy();
                return;
            }

            CreateEffect(effect);

            var tmpObj = Instantiate(PhotonNetwork.LocalPlayer == info.Sender ? selectedAudio : highlightAudio, transform);
            AudioSource source = tmpObj.GetComponent<AudioSource>();
#if DEBUG
            if (source == null)
            {
                Debug.LogError("<b>HighlightAlwaysVisible</b>: audio source does not exist in the call prefab object.");
                return;
            }
#endif
            StartCoroutine(WaitAudioOver(source));
        }

        protected override void Update()
        {
            if (!effectOnReceiveHighlight.Finished)
            {
                effectOnReceiveHighlight.Update();
            }
            if(!effectOnSendHighlight.Finished)
            {
                effectOnSendHighlight.Update();
            }
            if (!effectOnHoverHighlight.Finished)
            {
                effectOnHoverHighlight.Update();
            }
        }


        private HighlightEffect GetEffect(bool is_local)
        {
            if (is_local)
            {
                return effectOnSendHighlight;
            }
            return effectOnReceiveHighlight;
        }
        
        private void CreateEffect(HighlightEffect effect)
        {
            if(!effect.Finished)
            {
                effect.Destroy();
            }
            effect.Create(gameObject);
        }

        private IEnumerator WaitAudioOver(AudioSource audio)
        {
            audio.Play();
            yield return new WaitForSeconds(audio.clip.length);
            Destroy(audio.gameObject);
        }
    }

}
