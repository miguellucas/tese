﻿using UnityEngine;

namespace Painter.Communication
{ 
    public abstract class HighlightEffect : MonoBehaviour
    {
        protected MeshRenderer m_renderer;
        public Material effect;

        protected bool m_finished = true;
        public bool Finished { get { return m_finished; } }

        public HighlightEffect()
        {
            m_finished = true;
        }

        public abstract void Create(GameObject parent);
        public abstract void Update();
        public abstract void Destroy();
    }
}
