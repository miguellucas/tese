﻿using UnityEngine;

namespace Painter.Communication
{
    public class HighlightSelectedEffect :
            HighlightEffect
    {
        [Range(0.05f, 2.0f)]
        public float effectDuration = 0.75f;

        private float m_time;

        public override void Create(GameObject parent)
        {
            m_finished = false;
            // Update the timer
            m_time = 0.0f;

#if DEBUG
            var renderer = parent.GetComponent<MeshRenderer>();
            if (renderer == null)
            {
                Debug.LogError("<b>HighlightSelectedEffect</b>: no mesh to copy.");
                return;
            }
#endif

            MeshMethods.SpawnCopyMesh(parent, out m_renderer);

            // Change this object's material to use a different shader
            // Do this by changing the material or the shader
            m_renderer.material = effect;
        }

        public override void Update()
        {
            if (m_renderer == null)
                return;

            // If m_time is equal or greater than 1
            // destroy the copy object
            if (m_time >= 1.0f)
            {
                Destroy();
                m_renderer = null;
                return;
            }
            else
            {
                m_time += Time.deltaTime / effectDuration;
            }

            // Update the material in the copy object
            m_renderer.material.SetFloat("_MTime", m_time);
        }

        public override void Destroy()
        {
            Destroy(m_renderer.gameObject);
            m_finished = true;
        }
    }
}
