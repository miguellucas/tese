﻿using Painter.Core;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Communication.Preview
{
    public abstract class PreviewObject : MonoBehaviour
    {
        public bool isOriginal;
        public Player player;
        public GameObject otherObject;

        #region Public methods

        public bool CanMove
        {
            get
            {
                return !isOriginal;
            }
        }

        public bool PlayerCanAccept(Player player)
        {
            return player != this.player;
        }

        public void ObjectSelected()
        {
            SelectPreview();
        }

        public void OnHover()
        {
            StartHoverPreview();
        }


        public void OnPress()
        {
            StartPressPreview();
        }

        public void OnStopHover()
        {
            EndHoverPreview();
        }

        #endregion

        #region PUN RPC

        [PunRPC]
        protected void DestroyObjectForAllClients()
        {
            DestroyPreview();
        }

        #endregion

        #region Virtual methods

        // Object pressed
        protected abstract void SelectPreview();

        // OnStartHover effects
        protected abstract void StartHoverPreview();

        // OnEndHover effects
        protected abstract void EndHoverPreview();

        // OnPressDown effect
        protected abstract void StartPressPreview();

        // OnDestroy effects
        protected abstract void DestroyPreview();

        #endregion

        #region Switch Preview Object 

        protected void AcceptOriginalObject()
        {
            // If this is the original simply erase the preview for all clients
            PhotonView.Get(gameObject).RPC("DestroyObjectForAllClients", RpcTarget.All);
        }

        protected void AcceptPreviewObject()
        {            
            // If this is the preview, ask for ownership over the original
            // After receiving the ownership move the original to this transform's
            // location and delete the preview for all clients

            var originalPhotonView = PhotonView.Get(otherObject);
#if DEBUG
            if (originalPhotonView == null)
            {
                Debug.LogError("<b>PreviewObject</b>: No PhotonView component was found in the original Selectable object.");
                return;
            }
#endif
            // Check if ownership is mine
            if (originalPhotonView.IsMine)
            {
                SwitchPreviewObject();
                return;
            }

            // If not, request ownership with a callback that will run the logic of creating a preview
            var ownershipTransfer = originalPhotonView.GetComponent<OwnershipTransfer>();
#if DEBUG
            if(ownershipTransfer == null)
            {
                Debug.LogError("<b>PreviewObject</b>: No OwnershipTransfer component was found in the original Selectable object.");
                return;
            }
#endif
            ownershipTransfer.AskOwnershipTransfer(PhotonNetwork.LocalPlayer, true, SwitchPreviewObject);
        }

        protected void SwitchPreviewObject()
        {
#if DEBUG
            if (isOriginal)
            {
                Debug.LogError("<b>PreviewObject</b>: SwitchPreviewObject should only be called on the preview object and not in the orignal.");
                return;
            }
#endif
            // Set the transform of the original object to be the same
            // as the preview's transform
            otherObject.transform.position = transform.position;
            otherObject.transform.rotation = transform.rotation;
            otherObject.transform.localScale = transform.localScale;

            // Give back the ownership to another player
            var originalPhotonView = PhotonView.Get(otherObject);
#if DEBUG
            if (originalPhotonView == null)
            {
                Debug.LogError("<b>PreviewObject</b>: No PhotonView component was found in the original Selectable object.");
                return;
            }
#endif

            // If not, request ownership with a callback that will run the logic of creating a preview
            var ownershipTransfer = originalPhotonView.GetComponent<OwnershipTransfer>();
#if DEBUG
            if (ownershipTransfer == null)
            {
                Debug.LogError("<b>PreviewObject</b>: No OwnershipTransfer component was found in the original Selectable object.");
                return;
            }
#endif
            ownershipTransfer.AskOwnershipTransfer(PhotonNetwork.LocalPlayer, false);

            // Destroy the preview
            PhotonView.Get(gameObject).RPC("DestroyObjectForAllClients", RpcTarget.All);
        }

        #endregion
    }

}