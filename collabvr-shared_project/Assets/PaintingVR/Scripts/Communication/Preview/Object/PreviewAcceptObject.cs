﻿using Painter.Network.Selectable;
using UnityEngine;

namespace Painter.Communication.Preview
{
    public class PreviewAcceptObject : PreviewObject
    {
        public MeshRenderer meshRenderer;

        public Material hoverEffect;
        public Color hoverAcceptColor = Color.green;
        public Color pressAcceptColor = Color.cyan;

        protected override void SelectPreview()
        {
            if (isOriginal)
            {
                AcceptOriginalObject();
            }
            else
            {
                AcceptPreviewObject();
            }
        }

        protected override void StartHoverPreview()
        {
#if DEBUG
            var renderer = gameObject.GetComponent<MeshRenderer>();
            if (renderer == null)
            {
                Debug.LogError("<b>PreviewAcceptObject</b>: No mesh to copy.");
                return;
            }
#endif

            MeshMethods.SpawnCopyMesh(gameObject, out this.meshRenderer);
            this.meshRenderer.material = hoverEffect;
            this.meshRenderer.material.color = hoverAcceptColor;
        }

        protected override void EndHoverPreview()
        {
#if DEBUG
            if(meshRenderer == null)
            {
                Debug.LogError("<b>PreviewAcceptObject</b>: No copy object was previously created to stop the hover effect.");
                return;
            }
#endif
            // Simply destroy the hover effect
            Destroy(meshRenderer.gameObject);
        }

        protected override void DestroyPreview()
        {
            if(isOriginal)
            {
                var selectable = GetComponent<Selectable>();
                selectable.SetEnabledToAllClients(true);

                Destroy(otherObject.gameObject);
                if (meshRenderer != null)
                    Destroy(meshRenderer.gameObject);
                Destroy(this);
            }
            else
            {
                var selectable = otherObject.GetComponent<Selectable>();
                selectable.SetEnabledToAllClients(true);

                var previewAccept = otherObject.GetComponent<PreviewAcceptObject>();
                Destroy(previewAccept);
                Destroy(gameObject);
            }
        }

        protected override void StartPressPreview()
        {
#if DEBUG
            if (meshRenderer == null)
            {
                Debug.LogError("<b>PreviewAcceptObject</b>: No copy object was previously created to start the press effect.");
                return;
            }
#endif
            meshRenderer.material.color = pressAcceptColor;
        }
    }
}
