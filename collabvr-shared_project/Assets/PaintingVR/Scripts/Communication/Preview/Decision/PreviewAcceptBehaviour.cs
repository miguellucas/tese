﻿using System.Collections;
using UnityEngine;
using Painter.Utils;
using Painter.VR.Input;
using Painter.Network.Selectable;
using Valve.VR;
using Photon.Pun;
using Painter.Core;
using Painter.VR.Core;
using System.Collections.Generic;
using System.Linq;
using Painter.VR.Tool;

namespace Painter.Communication.Preview
{
    public class PreviewAcceptBehaviour : SimpleActionHandler
    {
        protected struct AcceptHandState
        {
            public SimpleHandVR hand;
            public PreviewObject previewObject;
            public bool inputPressed;
            public bool onPressedSent;
            public bool active;
        }

        protected AcceptHandState m_leftState;
        protected AcceptHandState m_rightState;

        /// <summary>
        /// Radius of the Sphere cast for discover selectables nearby
        /// </summary>
        public float checkRadius = 0.1f;

        [Space]
        public ToolManager defaultTool;

        protected SteamVR_Action_Vibration m_hapticAction;

        public PreviewAcceptBehaviour()
        {
            actionName = "GrabPinch";
        }

        #region Overrides

        protected override void StartBehaviour(SimpleHandVR hand)
        {
            base.StartBehaviour(hand);

            m_hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");

            var state = new AcceptHandState
            {
                hand = hand,
                previewObject = null,
                inputPressed = false,
                onPressedSent = false,
                active = true
            };

            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                m_leftState = state;
            }
            else if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                m_rightState = state;
            }
        }

        protected override void StopBehaviour(SimpleHandVR hand)
        {
            base.StopBehaviour(hand);

            var state = new AcceptHandState
            {
                hand = hand,
                previewObject = null,
                inputPressed = false,
                active = false
            };

            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                m_leftState = state;
            }
            else if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                m_rightState = state;
            }
        }


        /// <summary>
        /// Determine if this behaviour can be changed at this point in time.
        /// </summary>
        /// <param name="hand">VR hand that tiggered this action.</param>
        /// <returns>Return false only if it is being pressed while hovering over a PreviewObject with this hand</returns>
        public override bool CanChange(SimpleHandVR hand)
        {

            var state = new AcceptHandState();
            var foundHand = HandState(hand, ref state);

            return !foundHand ||
                   !state.inputPressed ||
                   state.previewObject == null;
        }

        /// <summary>
        /// Determine the course of action given the state of each of the hands and the input source that triggered this action.
        /// </summary>
        /// <param name="handInteraction">State of pressing for each of the hands.</param>
        /// <param name="fromSource">>VR hand that tiggered this action.</param>
        protected override void ActionStateChange(HandInteraction handInteraction, SteamVR_Input_Sources fromSource)
        {
            if (fromSource == SteamVR_Input_Sources.LeftHand)
            {
                if (!m_left_action && handInteraction.leftHand)
                {
                    OnActionPressed(m_left_hand);
                }
                else if (m_left_action && !handInteraction.leftHand)
                {
                    OnActionReleased(m_left_hand);
                }
                m_left_action = handInteraction.leftHand;
            }
            else if (fromSource == SteamVR_Input_Sources.RightHand)
            {
                if (!m_right_action && handInteraction.rightHand)
                {
                    OnActionPressed(m_right_hand);
                }
                else if (m_right_action && !handInteraction.rightHand)
                {
                    OnActionReleased(m_right_hand);
                }
                m_right_action = handInteraction.rightHand;
            }
        }

        /// <summary>
        /// Simply set a flag in the hand state to indicate that the button is pressed.
        /// </summary>
        /// <param name="origin">VR hand that tiggered this action.</param>
        protected override void OnActionPressed(SimpleHandVR origin)
        {
            // Set a variable to true for this hand
            var state = new AcceptHandState();
            var foundHand = HandState(origin, ref state);
            if (!foundHand)
                return;
            state.inputPressed = true;
        }

        /// <summary>
        /// If hovering over a PreviewObject with this hand then call
        /// a method in PreviewObject that accepts (or rejects) that 
        /// object and does the opposite to it's counterpart
        /// </summary>
        /// <param name="origin">VR hand that tiggered this action.</param>
        protected override void OnActionReleased(SimpleHandVR origin)
        {
            var state = new AcceptHandState();
            var foundHand = HandState(origin, ref state);
            if (!foundHand)
                return;
            state.inputPressed = false;
            
            var tool = ToolManager.HandTool(state.hand);
            tool.DisableTool(state.hand, defaultTool);
            defaultTool.EnableTool(state.hand, tool);

            if (state.previewObject == null)
                return;

            state.previewObject.ObjectSelected();
        }

        #endregion

        protected bool HandState(SimpleHandVR hand, ref AcceptHandState handState)
        {
            if(hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                handState = m_leftState;
            }
            else if(hand.handType == SteamVR_Input_Sources.RightHand)
            {
                handState = m_rightState;
            }
            else
            {
                return false;
            }

            return true;
        }

        protected void DisableHover(ref AcceptHandState handState)
        {
            handState.previewObject.OnStopHover();
            handState.previewObject = null;
            handState.onPressedSent = false;

#if DEBUG
            if (m_hapticAction == null)
            {
                Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                return;
            }
#endif
            m_hapticAction.Execute(0, 0.1f, 20, 1, handState.hand.handType);
        }

        protected void UpdateHand(ref AcceptHandState handState)
        {
            // Get all the preview object nearby
            var previewObjects = ObjectOperations.CheckForSelectables<PreviewObject>(transform,
                                                                                     handState.hand,
                                                                                     checkRadius);

            // Check if there are preview object nearby
            if (previewObjects.Length <= 0)
            {
                if(handState.previewObject != null)
                {
                    DisableHover(ref handState);
                }

                return;
            }

            // Check if the previously hovered object is still nearby
            PreviewObject preview;
            if (handState.previewObject != null &&
                previewObjects.Contains(handState.previewObject))
            {
                preview = handState.previewObject;
            }
            else
            {
                preview = previewObjects[0];

                if (handState.previewObject != null)
                    DisableHover(ref handState);
            }

            // Call OnHover in the PreviewObject
            // if this is the first frame where it is hovering
            if (handState.previewObject == null)
            {
                preview.OnHover();

#if DEBUG
                if (m_hapticAction == null)
                {
                    Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                    return;
                }
#endif
                m_hapticAction.Execute(0, 0.2f, 40, 2, handState.hand.handType);
            }

            // Call OnPress in the PreviewObject
            // if this input button is pressed and the pressed event was not yet sent
            if (handState.inputPressed && !handState.onPressedSent)
            {
                preview.OnPress();
                handState.onPressedSent = true;
            }

            // Store the hovered object in a variable with the respective hand
            if(handState.previewObject == null)
            {
                handState.previewObject = preview;
            }
        }

        private void Update()
        {
            if(m_leftState.active)
            {
                UpdateHand(ref m_leftState);
            }

            if(m_rightState.active)
            {
                UpdateHand(ref m_rightState);
            }
        }
    }
}
