﻿using System.Collections;
using UnityEngine;
using Painter.Utils;
using Painter.VR.Input;
using Painter.Network.Selectable;
using Valve.VR;
using Photon.Pun;
using Painter.Core;
using Painter.VR.Tool;

namespace Painter.Communication.Preview
{
    public class PreviewBehaviour : SelectableBehaviour
    {

        [Space]
        public ToolManager defaultTool;

        private CustomCopy copyObjects;

        private Selectable m_selectable;
        private SteamVR_Input_Sources m_inputSource;
        private bool m_isTryingToPreview = false;
        private IEnumerator m_copyCoroutine = null;

        private IEnumerator Start()
        {
            while(copyObjects == null)
            {
                yield return null;
                copyObjects = FindObjectOfType<PreviewObjectFromSelectable>();
            }
        }

        protected override void GrabSelectable(ref SelectableHandState state, Selectable selectable)
        {
            // Already trying to grab copy
            if (m_copyCoroutine != null || m_isTryingToPreview)
                return;

            var previewInSelectable = selectable.gameObject.GetComponent<PreviewObject>();
            if(previewInSelectable != null)
            {
                // Grab the preview if that's the case
                if(previewInSelectable.CanMove)
                    GrabCopy(selectable, state.hand.handType);
                return;
            }

            // Set variables that will be used throughout the process
            m_selectable = selectable;
            m_inputSource = state.hand.handType;
            m_isTryingToPreview = true;

            // Start process of creating a selectable object
            PreviewSelectable();
        }

        protected override void DropSelectable(ref SelectableHandState state, Selectable selectable)
        {
            base.DropSelectable(ref state, selectable);
            
            // Revert back to the default tool
            var tool = ToolManager.HandTool(state.hand);
            tool.DisableTool(state.hand, defaultTool);
            defaultTool.EnableTool(state.hand, tool);
        }

        protected void PreviewSelectable()
        {
            var photonView = PhotonView.Get(m_selectable.gameObject);
#if DEBUG
            if (photonView == null)
            {
                Debug.LogError("<b>PreviewBehaviour</b>: No PhotonView component was found in the Selectable object.");
                return;
            }
#endif
            // Check if ownership is mine and object is not currently selected
            if (photonView.IsMine)
            {
                m_isTryingToPreview = false;
                // If yes, create a preview, only if object is not currently selected
                if (!m_selectable.Selected)
                    CreatePreview(m_selectable, m_inputSource);
                return;
            }

            // If not, request ownership with a callback that will run the logic of creating a preview
            var ownershipTransfer = m_selectable.GetComponent<OwnershipTransfer>();
#if DEBUG
            if (ownershipTransfer == null)
            {
                Debug.LogError("<b>PreviewBehaviour</b>: No OwnershipTransfer component was found in the Selectable object.");
                m_isTryingToPreview = false;
                return;
            }

            if (m_hapticAction == null)
            {
                Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                m_isTryingToPreview = false;
                return;
            }
#endif

            ownershipTransfer.AskOwnershipTransfer(PhotonNetwork.LocalPlayer, true, OwnershipTransfered);
            m_hapticAction.Execute(0, 0.2f, 120, 0.5f, m_inputSource);
        }

        private void OwnershipTransfered()
        {
            // Revert ownership because client won't be interacting with object
            var ownershipTransfer = m_selectable.GetComponent<OwnershipTransfer>();
            ownershipTransfer.AskOwnershipTransfer(PhotonNetwork.LocalPlayer, false, OwnershipTransfered);

            // Set copy coroutine
            CreatePreview(m_selectable, m_inputSource);
        }

        protected void CreatePreview(Selectable selectable, SteamVR_Input_Sources inputSource)
        {
            // Set copy coroutine
            m_copyCoroutine = GrabCoroutine(selectable, inputSource);
            StartCoroutine(m_copyCoroutine);

            // Accomplishing this
            m_isTryingToPreview = false;
        }

        protected IEnumerator GrabCoroutine(Selectable selectable, SteamVR_Input_Sources inputSource)
        {
            copyObjects.CopyGameObject(selectable.gameObject);
            
            PreviewObject previewComponent = null; 
            while(previewComponent == null || previewComponent.otherObject == null)
            {
                yield return null;
                previewComponent = selectable.GetComponent<PreviewObject>();
            }
            
            m_copyCoroutine = null;

            selectable.SetEnabledToAllClients(false);

            var previewSelectable = previewComponent.otherObject.GetComponent<Selectable>();
            GrabCopy(previewSelectable, inputSource);

            m_hapticAction.Execute(0, 0.2f, 120, 0.5f, m_inputSource);
        }

        protected void GrabCopy(Selectable previewSelectable, SteamVR_Input_Sources inputSource)
        {
            if (inputSource == SteamVR_Input_Sources.LeftHand)
            {
                GrabCopyToHand(previewSelectable, ref m_leftState);
            }
            else if (inputSource == SteamVR_Input_Sources.RightHand)
            {
                GrabCopyToHand(previewSelectable, ref m_rightState);
            }
        }

        protected void GrabCopyToHand(Selectable previewSelectable, ref SelectableHandState state)
        {
            state.selected = previewSelectable;
            state.grabbed = false;

            state.selected.grabEvent.AddListener(GrabEvent);
            state.selected.RegisterGrabIntention(state.hand.attachmentPoint);
        }
    }
}
