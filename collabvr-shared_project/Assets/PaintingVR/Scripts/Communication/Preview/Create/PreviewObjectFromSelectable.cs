﻿using Painter.Utils;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Painter.Communication.Preview
{
    [RequireComponent(typeof(PhotonView))]
    public class PreviewObjectFromSelectable : CustomCopy
    {
        public Material previewMaterial;
        public Material previewHoverMaterial;

        [Tooltip("To avoid colliding with the original object a different layer is used where collisions are not checked.")]
        public LayerMask previewObjectsLayer;

        public static int NEXT_ID { get; private set; } = 250;

        public override GameObject CopyGameObject(GameObject original)
        {
            var photonView = original.GetComponent<PhotonView>();
            gameObject.GetPhotonView().RPC("NetworkCopy", 
                                           RpcTarget.All, 
                                           new object[] { photonView.ViewID, NEXT_ID++ });
            return original;
        }

        [PunRPC]
        private void NetworkCopy(int originalID, int newID)
        {
            // Received ID might be higher than the local one
            RecalculateNextID(newID);

            var originalPhotonView = PhotonView.Find(originalID);
            var originalObject = originalPhotonView.gameObject;

            // Deactivate parent object so instantiation doens't trigger the Awake 
            // method right away
            originalObject.transform.parent.gameObject.SetActive(false);

            // Big problems... This is a local instantiation
            // New clients won't have the preview object
            var previewObject = Instantiate(originalObject,
                                            originalObject.transform.position,
                                            originalObject.transform.rotation,
                                            originalObject.transform.parent);

            // Ignore collisions between these two objects
            IgnoreCollisions(previewObject, originalObject);

            // Add a PreviewObject script to the preview
            var previewScript = previewObject.AddComponent<PreviewAcceptObject>();
            previewScript.isOriginal = false;
            previewScript.otherObject = originalObject;
            previewScript.hoverEffect = previewHoverMaterial;

            // Add a PreviewObject script to the original
            previewScript = originalObject.AddComponent<PreviewAcceptObject>();
            previewScript.isOriginal = true;
            previewScript.otherObject = previewObject;
            previewScript.hoverEffect = previewHoverMaterial;

            // Find all the mesh renderer in the preview object and change 
            // their materials
            SetPreviewObjectMaterial(previewObject, previewMaterial);

            // Set the photonView ID of the new object to be constant throughout all clients
            var previewPhotonView = previewObject.GetComponent<PhotonView>();
            previewPhotonView.ViewID = newID;

            ResetPhotonViewComponent(previewPhotonView, originalPhotonView);

            // Activate parent object
            originalObject.transform.parent.gameObject.SetActive(true);
        }

        private static void SetPreviewObjectMaterial(GameObject preview, Material material)
        {
            var renderers = preview.GetComponentsInChildren<MeshRenderer>();
            foreach(var meshRenderer in renderers)
            {
                meshRenderer.material = material;
            }
        }

        private static void ResetPhotonViewComponent(PhotonView preview, PhotonView original)
        {
            preview.ObservedComponents.Clear();

            // Assumes that all components are of different type
            var originalComponents = original.ObservedComponents;
            foreach(var component in originalComponents)
            {
                var previewComponent = preview.GetComponent(component.GetType());
                preview.ObservedComponents.Add(previewComponent);
            }
        }

        private static void CopyComponents(GameObject preview, GameObject original, HashSet<Type> ignoreTypes)
        {
            var orignalComponents = original.GetComponents<Component>();
            foreach(var component in orignalComponents)
            {
                if(ignoreTypes.Contains(orignalComponents.GetType()))
                {
                    continue;
                }

                preview.AddComponent(orignalComponents.GetType());
            }
        }

        private static void IgnoreCollisions(GameObject preview, GameObject original)
        {
            var previewObjectColliders = preview.GetComponentsInChildren<Collider>();
            var originalObjectColliders = original.GetComponentsInChildren<Collider>();

            foreach(var prevCollider in previewObjectColliders)
            {
                foreach(var originalCollider in originalObjectColliders)
                {
                    Physics.IgnoreCollision(prevCollider, originalCollider, true);
                }
            }
        }

        private static void RecalculateNextID(int receivedID)
        {
            if (NEXT_ID < receivedID)
                NEXT_ID = receivedID + 1;
        }
    }
}
