﻿using Painter.Utils;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;

namespace Painter.Communication
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(Collider))]
    public class MeshUserTag : UserTag
    {
        private MeshRenderer m_mesh_renderer;
        private Collider m_collider;

        private bool m_user_called = false;

        [Header("Colors")]

        [Tooltip("When the local user calls other users the tags should temporarily change to this color")]
        public Color colorLocalCalled;
        [Tooltip("When a connected user calls other users (including the local player) the tags should temporarily change to this color")]
        public Color colorOtherCalled;

        private Color m_initial_color;

        [Space]
        [Header("Calling controllers")]

        [Range(0.01f, 1.0f)]
        public float fovRatio = 0.8f;
        [Range(0.01f, 10.0f)]
        public float decrementRatio = 0.1f;
        [Range(0.01f, 10.0f)]
        public float incrementRatio = 0.2f;

        private float m_time;

        private void Awake()
        {
            m_mesh_renderer = GetComponent<MeshRenderer>();
            m_collider = GetComponent<Collider>();
            m_initial_color = m_mesh_renderer.material.color;
        }

        protected override void LocalUserCalled()
        {
            // Don't run this effect if the owner
            // of this tag has called the local user
            if (m_user_called)
            {
                return;
            }

            // Don't run this effect if the owner
            // is the local player
            if (photonView.Owner == PhotonNetwork.LocalPlayer)
            {
                return;
            }

            // Apply effect to the tag
            ApplyEffect(colorLocalCalled);

            StartCoroutine(ResetEffectForLocalUserCalled());
        }

        protected override void OtherUserCalled(Player calling_player)
        {
            // Only change the color 
            // if this user is the calling user
            if (calling_player != photonView.Owner)
            {
                return;
            }

            // Apply effect to the tag
            ApplyEffect(colorOtherCalled);

            // Start the coroutine that'll determine when the effect is over
            StartCoroutine(ResetEffectForOtherUserCalled());
        }

        private IEnumerator ResetEffectForLocalUserCalled()
        {
            yield return new WaitForSeconds(1.0f);

            ResetEffect();
        } 

        private IEnumerator ResetEffectForOtherUserCalled()
        {
            HighlightController.Instance.CallAtentionTo(transform, true);
            m_user_called = true;
            
            // Wait for user to look at user's tag
            var stop = false;
            while(!stop)
            {

                yield return null;
                // If m_time is equal or greater than 1
                // reset the effect and stop the loop
                if (m_time >= 1.0f)
                {
                    ResetEffect();
                    stop = true;
                }
                else
                {
                    // m_time will increment if the object is within the camera's fov
                    // and will decrement when it is not
                    bool is_within_fov = MathCalculations.IsWithinCameraFOV(Camera.main, m_collider.bounds.center, fovRatio);
                    m_time += (is_within_fov ? incrementRatio : -decrementRatio) * Time.deltaTime;
                    m_time = m_time < 0.0f ? 0.0f : m_time;
                }
            }
            
            HighlightController.Instance.RemoveAttentionTo(transform);
            m_user_called = false;
        }

        private void ApplyEffect(Color color)
        {
            // Change the color of the tag
            m_mesh_renderer.material.color = color;

            // Indicator to point at here
        }

        private void ResetEffect()
        {
            m_time = 0.0f;

            m_mesh_renderer.material.color = m_initial_color;

            // Indicator to stop pointing at here
        }
    }
}
