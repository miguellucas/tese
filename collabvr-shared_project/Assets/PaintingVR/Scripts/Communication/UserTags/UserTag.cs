﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Communication
{
    public abstract class UserTag : MonoBehaviour
    {
        [SerializeField]
        protected PhotonView photonView;

        public void UserCalled(Player player)
        {
            if(player.IsLocal)
            {
                LocalUserCalled();
            }
            else
            {
                OtherUserCalled(player);
            }
        }

        protected abstract void LocalUserCalled();
        protected abstract void OtherUserCalled(Player calling_player);
    }
}
