﻿using UnityEngine;

namespace Painter.Communication
{
    public class UserTagController : MonoBehaviour
    {
        private float m_angle;
        public float angularVelocity;

        private float m_time;
        public float pingPongDuration;

        private float m_initial_y;
        public float translationLength;

        private Quaternion m_initial_quaternion;

        private void Start()
        {
            m_time = 0.0f;
            
            m_initial_y = transform.localPosition.y;
        }

        private void LateUpdate()
        {
            m_time += Time.deltaTime;

            UpdateTransform();
        }

        private void UpdateTransform()
        {
            MaintainTranslation();
            PingPongTranslation();

            MaintainRotation();
            RotateTag();
        }

        private void PingPongTranslation()
        {
            // Ping pong the time
            var time = Mathf.PingPong(m_time, pingPongDuration);

            // Using time determine the y position
            var y_position = /*transform.parent.position.y + */m_initial_y + time * translationLength / pingPongDuration;

            // Translate to the new y position
            //transform.Translate(Vector3.up * y_position, Space.Self);
            transform.localPosition = Vector3.up * y_position;
        }

        private void RotateTag()
        {
            // Increment the angle of rotation
            m_angle += angularVelocity * Time.deltaTime;

            // Rotate the tag with m_angle degrees
            transform.Rotate(Vector3.up, m_angle, Space.Self);
        }

        private void MaintainRotation()
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        }

        private void MaintainTranslation()
        {
            transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }
}
