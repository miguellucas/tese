﻿using Painter.Core;
using Painter.Utils;
using Painter.VR.Behaviour;
using Painter.VR.Core;
using Painter.VR.Input;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.Communication
{
    [RequireComponent(typeof(PhotonView))]
    public class CallUsers : VRBehaviour
    {
        #region Singleton
        private static CallUsers instance;
        public static CallUsers Instance
        {
            get
            {
                return instance;
            }
        }

        public CallUsers()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>CallUsers</b>: Multiple instances of CallUsers not supported.");
                return;
            }
#endif

            instance = this;
        }
        #endregion

        private PhotonView m_photon_view;

        public GameObject sendCallAudioSourcePrefab;
        public GameObject receiveCallAudioSourcePrefab;
        private List<GameObject> sendAudioSourceObjects;
        private List<GameObject> receiveAudioSourceObjects;


        private void Start()
        {
            m_photon_view = GetComponent<PhotonView>();
            sendAudioSourceObjects = new List<GameObject>();
            receiveAudioSourceObjects = new List<GameObject>();
        }


        private void UpdateUserTags(Player calling_user)
        {
            // Find all tags and run the UserCalled method
            var userTags = FindObjectsOfType<UserTag>();
            foreach (var tag in userTags)
                tag.UserCalled(calling_user);
        }


        private void DestroySendAudioSource()
        {
            Destroy(sendAudioSourceObjects[0]);
            sendAudioSourceObjects.RemoveAt(0);
        }

        private void DestroyReceivingAudioSource()
        {
            Destroy(receiveAudioSourceObjects[0]);
            receiveAudioSourceObjects.RemoveAt(0);
        }

        private void PlaySound(Player calling_user)
        {
            GameObject instantiatedAudio;
            string destroyAudioMethodName;

            var playerRoot = PlayerUtils.GetUserRoot(calling_user);
            var rootScript = playerRoot.GetComponent<RootVR>();
            if(rootScript != null)
            {
                var tmp = rootScript.GetHead();
                if(tmp != null)
                {
                    playerRoot = tmp.transform;
                }
            }

            if (calling_user == PhotonNetwork.LocalPlayer)
            {
                instantiatedAudio = Instantiate(sendCallAudioSourcePrefab, playerRoot);
                sendAudioSourceObjects.Add(instantiatedAudio);
                destroyAudioMethodName = "DestroySendAudioSource";
            }
            else
            {
                instantiatedAudio = Instantiate(receiveCallAudioSourcePrefab, playerRoot);
                receiveAudioSourceObjects.Add(instantiatedAudio);
                destroyAudioMethodName = "DestroyReceivingAudioSource";
            }

            AudioSource source = instantiatedAudio.GetComponent<AudioSource>();
#if DEBUG
            if (source == null)
            {
                Debug.LogError("<b>CallUsers</b>: audio source does not exist in the call prefab object.");
                return;
            }
#endif

            source.Play();
            Invoke(destroyAudioMethodName, source.clip.length);
        }
        
        private void CallEffect(Player calling_user)
        {
            // Find all tags and run the UserCalled method
            UpdateUserTags(calling_user);

            // Play a sound
            PlaySound(calling_user);
        }

        public void Execute()
        {
            // Run rpc method on all other users
            m_photon_view.RPC("UserCalling", RpcTarget.All);
        }

        [PunRPC]
        private void UserCalling(PhotonMessageInfo info)
        {
            CallEffect(info.Sender);
        }


        protected override void StartBehaviour(SimpleHandVR hand)
        {
            Execute();
        }

        protected override void StopBehaviour(SimpleHandVR hand)
        {}

        public override bool CanChange(SimpleHandVR hand)
        {
            // It is a single effect behaviour
            // Change is always possible
            return true;
        }
    }
}
