﻿using Painter.Utils;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Communication
{

    [RequireComponent(typeof(PhotonView))]
    public abstract class CallMe : MonoBehaviour
    {
        private PhotonView photonView;

        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }

        /// <summary>
        /// Checks that a player was found in the local instante of the game and calls a function responsible for the logic behind the call.
        /// </summary>
        /// <param name="callingPlayer">The player calling the local client.</param>
        [PunRPC]
        public void NotifyCall(PhotonMessageInfo info)
        {
            Transform callingPlayerRoot = PlayerUtils.GetUserRoot(info.Sender);

#if DEBUG
            if (callingPlayerRoot == null)
            {
                Debug.LogError("<b>CallMe</b>: the player " + info.Sender.UserId + " does not have a root object.");
                return;
            }
#endif

            OnUserIsCalling(callingPlayerRoot);
        }

        /// <summary>
        /// When a player calls something must happen. 
        /// The developer can play a sound, show some visual cues, etc... 
        /// </summary>
        /// <param name="callingPlayerRoot">The root transform of the calling player.</param>
        protected abstract void OnUserIsCalling(Transform callingPlayerRoot);
    }
}
