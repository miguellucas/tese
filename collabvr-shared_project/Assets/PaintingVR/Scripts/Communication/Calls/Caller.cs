﻿using Painter.Core;
using Painter.Utils;
using Painter.VR.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Communication
{
    public class Caller : Validator
    {
        #region Singleton
        private static Caller instance;
        public static Caller Instance
        {
            get
            {
                return instance;
            }
        }

        public Caller()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>Caller</b>: Multiple instances of Caller not supported.");
                return;
            }
#endif

            instance = this;
        }
        #endregion

        private Player m_valid_player = null;

        public override bool Validate(RaycastHit hit)
        {
            PhotonView photonView = hit.collider.gameObject.GetComponent<PhotonView>();
            if(photonView != null && hit.collider.gameObject.tag == "Player")
            {
                m_valid_player = photonView.Owner;
                return true;
            }
            return false;
        }

        public override void ExecuteAction()
        {
            Call(m_valid_player);
            m_valid_player = null;
        }

        public override void Show(bool show, bool isAttached = false)
        {}

        public override Vector3 GetAttachmentPoint()
        {
            return Vector3.zero;
        }
        
        private void Call(Player player)
        {
            // if 1
            if (player != null)
            {
                var playerRoot = PlayerUtils.GetUserRoot(player);

                // if 2
                if(playerRoot != null)
                {
                    var callmeScript = playerRoot.GetComponent<CallMe>();
                    var rpcAtPlayer = playerRoot.GetComponent<RPCMethodsAtOwner>();

                    // if 3
                    if(callmeScript != null && rpcAtPlayer != null)
                    {
                        rpcAtPlayer.RunRPCOnMe("NotifyCall", new[] { PhotonNetwork.LocalPlayer });
                    }
                }
            }
        }
    }
}
