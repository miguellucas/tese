﻿using System.Collections.Generic;
using UnityEngine;

namespace Painter.Communication
{
    public class CallMeExample : CallMe
    {
        public GameObject callAudioSourcePrefab;

        private List<GameObject> sourceObjects;

        public CallMeExample() {
            sourceObjects = new List<GameObject>();
        }
        
        private void DestroyAudioSource()
        {
            Destroy(sourceObjects[0]);
            sourceObjects.RemoveAt(0);
        }

        protected override void OnUserIsCalling(Transform callingPlayerRoot)
        {
            GameObject tmpObj = Instantiate(callAudioSourcePrefab, callingPlayerRoot);
            sourceObjects.Add(tmpObj);
            AudioSource source = tmpObj.GetComponent<AudioSource>();
#if DEBUG
            if (source == null)
            {
                Debug.LogError("<b>CallMeExample</b>: audio source does not exist in the call prefab object.");
                return;
            }
#endif

            source.Play();
            Invoke("DestroyAudioSource", source.clip.length);
        }
    }
}
