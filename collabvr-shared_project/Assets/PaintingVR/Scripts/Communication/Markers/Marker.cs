﻿using UnityEngine;

namespace Painter.Maker
{
    /// <summary>
    /// Basic marker class that handles the invokation of events.
    /// </summary>
    public class Marker : MonoBehaviour
    {
        /// <summary>
        /// MarkerHandler instance.
        /// </summary>
        private MarkerHandler markerHandler;

        /// <summary>
        /// Checks for the existance of a MarkerHandler and prints out an error if the instance is null.
        /// If the instance is not null, a SpawnMarker event is invoked.
        /// This method can be derived in a child class for the implementation of custom behaviour.
        /// </summary>
        protected virtual void Start()
        {
            markerHandler = MarkerHandler.Instance;
#if DEBUG
            if(markerHandler == null)
            {
                Debug.LogError("<b>Marker</b>: there is no instance of MarkerHandler in this scene");
                return;
            }
#endif

            markerHandler.OnSpawnMarker.Invoke(this);
        }

        /// <summary>
        /// Invokes a destroy marker event.
        /// This method can be derived in a child class for the implementation of custom behaviour.
        /// </summary>
        protected virtual void OnDestroy()
        {
            markerHandler.OnDestroyMarker.Invoke(this);
        }
    }
}
