﻿using UnityEngine;

namespace Painter.Maker
{
    public class MarkerGhost : MonoBehaviour
    {
        [HideInInspector]
        public Transform ghostFollow;

        private bool m_ghostIsShowing;

        private void LateUpdate()
        {
            if (transform == null)
                return;

            transform.position = ghostFollow.position;
            transform.rotation = ghostFollow.rotation;
        }

    }
}
