﻿using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Painter.Maker
{
    public class MarkerHandler : MonoBehaviour
    {
        #region Singleton

        /// <summary>
        /// Single instance of this class.
        /// </summary>
        private static MarkerHandler instance;
        /// <summary>
        /// Getter method for the instance of this class.
        /// </summary>
        public static MarkerHandler Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Constructor for this class that checks for singleton correctness.
        /// </summary>
        public MarkerHandler()
        {
#if DEBUG
            if(instance != null)
            {
                Debug.LogError("<b>MarkerHandler</b>: Multiple instances of MarkedHandler not supported.");
                return;
            }
#endif

            instance = this;

            myMarkers = new HashSet<Marker>();
            sceneMarkers = new HashSet<Marker>();
        }

        private void OnDestroy()
        {
            instance = null;
        }

        #endregion

        #region Scene markers

        /// <summary>
        /// Set of markers that were spawned by the player.
        /// </summary>
        protected HashSet<Marker> myMarkers;
        /// <summary>
        /// Set of markers that exist in the scene and where spawned by other players.
        /// </summary>
        protected HashSet<Marker> sceneMarkers;

        #endregion

        #region Marker events

        /// <summary>
        /// Spawn marker event class.
        /// </summary>
        public class SpawnEvent : UnityEvent<Marker> { };
        /// <summary>
        /// Destroy marker event class.
        /// </summary>
        public class DestroyEvent : UnityEvent<Marker> { };

        /// <summary>
        /// Spawn event that is invoked when a new marker has been instantiated.
        /// </summary>
        public SpawnEvent OnSpawnMarker;
        /// <summary>
        /// Destroy event that is invoked when a marker has been destroyed.
        /// </summary>
        public DestroyEvent OnDestroyMarker;

        /// <summary>
        /// Fuction called from the Spawn Marker event. 
        /// It'll add the marker to the corresponding data set.
        /// This function can be derived in a child class for the implementation of custom behavior.
        /// </summary>
        /// <param name="marker">The spawned marker.</param>
        protected virtual void MarkerSpawned(Marker marker)
        {
            PhotonView markerView = marker.GetComponent<PhotonView>();

#if DEBUG
            // Check that the ViewID of the marker is not yet registered
            if (myMarkers.Contains(marker))
            {
                Debug.LogError("<b>MarkerHandler</b>: spawned my marker with an already existing id.");
                return;
            }
            else if (sceneMarkers.Contains(marker))
            {
                Debug.LogError("<b>MarkerHandler</b>: spawned a marker with an already existing id.");
                return;
            }
#endif

            // Add the marker ViewID to the right data set
            if (markerView.IsMine)
            {
                myMarkers.Add(marker);
            }
            else
            {
                sceneMarkers.Add(marker);
            }
        }

        /// <summary>
        /// Function called from the Destory Marker event.
        /// It'll remove the marker from the data sets.
        /// This function can be derived in a child class for the implementation of custom behivor.
        /// </summary>
        /// <param name="marker">The of the marker to be destroyed.</param>
        protected virtual void MarkerDestoryed(Marker marker)
        {
#if DEBUG
            // Check that the markerID is not yet registered
            if (!myMarkers.Contains(marker) && !sceneMarkers.Contains(marker))
            {
                Debug.LogError("<b>MarkerHandler</b>: removing a marker that is not registered in any data set.");
                return;
            }
#endif

            // Remove markerID from the data sets
            myMarkers.Remove(marker);
            sceneMarkers.Remove(marker);
        }

        #endregion

        private void Start()
        {
            OnSpawnMarker = new SpawnEvent();
            OnSpawnMarker.AddListener(MarkerSpawned);

            OnDestroyMarker = new DestroyEvent();
            OnDestroyMarker.AddListener(MarkerDestoryed);
        }
    }
}
