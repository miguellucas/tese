﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

namespace Painter.Maker
{
    /// <summary>
    /// A marker that will stay in the scene for a given number of seconds.
    /// </summary>
    public class TimedMarker : Marker
    {
        /// <summary>
        /// Number of seconds the marker should be in the scene.
        /// </summary>
        public float destroyDelay;

        protected override void Start()
        {
            base.Start();

            StartCoroutine(WaitForDelay());
        }

        /// <summary>
        /// Waits for a number of seconds before destorying this game object.
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitForDelay()
        {
            yield return new WaitForSeconds(destroyDelay);

            PhotonNetwork.Destroy(this.gameObject);
        }


    }
}
