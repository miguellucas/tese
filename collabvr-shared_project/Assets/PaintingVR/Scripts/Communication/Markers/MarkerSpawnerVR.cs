﻿using Painter.Network.Utils;
using Painter.VR.Core;
using Painter.VR.Input;
using System.Collections;
using UnityEngine;
using Valve.VR;

namespace Painter.Maker
{
    public class MarkerSpawnerVR : MonoBehaviour
    {
        #region Input

        private SimpleHandVR m_leftHand;
        private SimpleHandVR m_rightHand;

        private bool m_leftIsMarker;
        private bool m_rightIsMarker;

        private bool m_leftIsPressed;
        private bool m_rightIsPressed;

        #endregion

        #region Markers

        public string markerPrefab;
        public GameObject ghostMarkerPrefab;
        
        private MarkerGhost m_markerGhost;

        #endregion

        private void Awake()
        {
            StartCoroutine(WaitForHands());

            if(m_markerGhost == null)
            {
                GameObject obj = Instantiate(ghostMarkerPrefab);
                m_markerGhost = obj.GetComponent<MarkerGhost>();
#if DEBUG
                if(m_markerGhost == null)
                {
                    Debug.LogError("<b>MarkerSpawnVR</b:> marker ghost prefab does not implement MarkerGhost.");
                    return;
                }
#endif
            }

            m_markerGhost.gameObject.SetActive(false);
        }

        private void Start()
        {
            InputHandler.Instance.NewAction("GrabGrip", SteamVR_Input_Sources.LeftHand, MarkerChangeHandler);
            InputHandler.Instance.NewAction("GrabGrip", SteamVR_Input_Sources.RightHand, MarkerChangeHandler);
        }

        private void OnDestroy()
        {
            InputHandler.Instance.RemoveAction("GrabGrip", SteamVR_Input_Sources.LeftHand, MarkerChangeHandler);
            InputHandler.Instance.RemoveAction("GrabGrip", SteamVR_Input_Sources.RightHand, MarkerChangeHandler);
            Destroy(m_markerGhost);
        }

        private IEnumerator WaitForHands()
        {
            while (true)
            {
                var hands = FindObjectsOfType<SimpleHandVR>();
                for (int i = 0; i < hands.Length; i++)
                {
                    SimpleHandVR hand = hands[i];
                    if (hand.handType == SteamVR_Input_Sources.LeftHand)
                    {
                        m_leftHand = hand;
                    }
                    else if (hand.handType == SteamVR_Input_Sources.RightHand)
                    {
                        m_rightHand = hand;
                    }
                }

                if (m_leftHand != null && m_rightHand != null)
                    break;
                yield return null;
            }
        }


        private void MarkerChangeHandler(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (fromSource == SteamVR_Input_Sources.LeftHand)
            {
                m_leftIsPressed = !m_leftIsPressed;
            }
            else if (fromSource == SteamVR_Input_Sources.RightHand)
            {
                m_rightIsPressed = !m_rightIsPressed;
            }

            TeleportStateChange();
        }

        private void InstantiateMarker()
        {
            InstantiationInfo info = new InstantiationInfo()
            {
                prefabs = new InstantiationPrefabs
                {
                    networkPrefab = markerPrefab
                },
                networkData = new NetworkInstantiationData()
                {
                    parentName = "",
                    gameObjectName = ""
                },
                position = m_markerGhost.transform.position,
                rotation = m_markerGhost.transform.rotation
            };

            NetworkInstantiation.Instantiate(info);
        }

        private void TeleportStateChange()
        {
            if (m_leftIsMarker && !m_leftIsPressed)
            {
                // Hide ghost and instantiate marker
                m_markerGhost.gameObject.SetActive(false);
                InstantiateMarker();

                m_leftIsMarker = false;
                return;
            }
            else if (m_rightIsMarker&& !m_rightIsPressed)
            {
                // Hide ghost and instantiate marker
                m_markerGhost.gameObject.SetActive(false);
                InstantiateMarker();

                m_rightIsMarker = false;
                return;
            }

            if (m_rightIsPressed)
            {
                m_leftIsMarker = false;
                m_rightIsMarker = true;

                // Show ghost in the right hand
                m_markerGhost.ghostFollow = m_rightHand.markerPoint;
                m_markerGhost.gameObject.SetActive(true);
            }
            else if (m_leftIsPressed)
            {
                m_rightIsMarker = false;
                m_leftIsMarker = true;

                // Show ghost in the left hand
                m_markerGhost.ghostFollow = m_leftHand.markerPoint;
                m_markerGhost.gameObject.SetActive(true);
            }

        }
    }
}
