﻿using Painter.VR.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbeToPlayer : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if(Camera.main != null)
        {
            transform.position = new Vector3
                (transform.position.x, 
                Camera.main.transform.position.y, 
                transform.position.z);
        }
    }
}
