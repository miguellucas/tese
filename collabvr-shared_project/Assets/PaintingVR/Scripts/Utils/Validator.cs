﻿using System;
using UnityEngine;

namespace Painter.VR.Input
{
    public abstract class Validator : MonoBehaviour
    {
        public abstract bool Validate(RaycastHit hit);
        public abstract void ExecuteAction();
        public abstract Vector3 GetAttachmentPoint();
        public abstract void Show(bool show, bool isAttached = false);
    }
}