﻿using UnityEngine;

namespace Painter.Utils
{
    public abstract class CustomCopy : MonoBehaviour
    {
        public abstract GameObject CopyGameObject(GameObject original);
    }
}
