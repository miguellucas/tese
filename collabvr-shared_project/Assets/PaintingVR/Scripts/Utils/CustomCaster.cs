﻿using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    /// <summary>
    /// Teleport caster allows for custom or already built-in solution to check for hits along a path.
    /// </summary>
    public abstract class CustomCaster : MonoBehaviour
    {
        /// <summary>
        /// This function will return all the hits it found and returns them.
        /// </summary>
        /// <param name="origin">The origin of the cast.</param>
        /// <returns>The objects the caster hit.</returns>
        public abstract RaycastHit[] CastAll(Transform origin);

        /// <summary>
        /// This function will return all the hits it found and returns them.
        /// </summary>
        /// <param name="origin">The origin of the cast.</param>
        /// <returns>The objects the caster hit.</returns>
        public abstract bool Cast(Transform origin, out RaycastHit hit);

        /// <summary>
        /// Shows the custom cast for the user to see.
        /// </summary>
        /// <param name="show">Flag that is set if the cast is to be rendered.</param>
        /// <param name="origin">Origin of the cast. It can be null if show is false.</param>
        public abstract void ShowCast(bool show, Transform origin = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="end"></param>
        public abstract void AttachTo(Vector3 end);
    }
}
