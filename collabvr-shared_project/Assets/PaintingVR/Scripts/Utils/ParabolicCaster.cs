﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    [RequireComponent(typeof(LineRenderer))]
    public class ParabolicCaster : CustomCaster
    {
        // y = position + velocity * t + 1/2acceleration * t^2
        // Y == Initial Y given by:
        //      velocity^2 * Sin (2 * angle) / acceleration;
        
        // public float linesPerMeter = 0.25f;
        // public float maximumDistance = 2.0f;
        public float minVelocity = 1.0f;
        public float maxVelocity = 3.0f;
        [Range(0.01f, 0.1f)]
        public float timeSteps = 0.01f;
        
        private LineRenderer m_renderer;
        private Transform m_origin;

        private bool m_hasEnd = false;
        private Vector3 m_end = default;

        private bool m_isShowing = false;

        public override RaycastHit[] CastAll(Transform origin)
        {
            var success = GetListPoints(origin, out RaycastHit hit, out Vector3[] points);
            if (success)
            {
                return new RaycastHit[1] { hit };
            }
            return new RaycastHit[0];
        }

        public override bool Cast(Transform origin, out RaycastHit hit)
        {
            return GetListPoints(origin, out hit, out Vector3[] points);
        }

        public override void ShowCast(bool show, Transform origin = null)
        {
            m_renderer.enabled = show;
            m_isShowing = show;
            m_origin = origin;
        }

        public override void AttachTo(Vector3 end = default)
        {
            m_hasEnd = end != default;
            m_end = end;
        }

        private bool GetListPoints(Transform origin, out RaycastHit hit, out Vector3[] points)
        {
            LinkedList<Vector3> listPoints = new LinkedList<Vector3>();
            Vector3 mPosition = origin.position;
            float time = 0.0f;

            var mVelocity = Mathf.Lerp(maxVelocity, minVelocity, Mathf.Abs(origin.forward.y));

            listPoints.AddLast(mPosition);

            while (mPosition.y > 0.0f)
            {
                var lastElement = mPosition;

                time += timeSteps;

                mPosition = mPosition + origin.forward * mVelocity * time;
                mPosition.y -= 5.0f * Mathf.Pow(time, 2);
                
                var forward = (mPosition - lastElement);
                if (Physics.Linecast(lastElement, mPosition, out hit))
                {
                    listPoints.AddLast(hit.point);
                    points = listPoints.ToArray();
                    return true;
                }
                else
                {
                    listPoints.AddLast(mPosition);
                }
            }

            points = listPoints.ToArray();
            hit = default;
            return false;
        }

        private void Awake()
        {
            m_renderer = GetComponent<LineRenderer>();
            m_renderer.widthMultiplier = 0.05f;
            m_renderer.positionCount = 0;
        }

        private void LateUpdate()
        {
            if (m_isShowing)
            {
                GetListPoints(m_origin, out RaycastHit hit, out Vector3[] points);
                m_renderer.positionCount = points.Length;
                m_renderer.SetPositions(points);
            }
        }

    }
}
