﻿using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    [RequireComponent(typeof(LineRenderer))]
    public class LineCaster : CustomCaster
    {
        private LineRenderer m_renderer;
        private Transform m_origin;

        private bool m_has_end = false;
        private Vector3 m_end = default;

        private bool m_isShowing = false;

        private const int COUNT_POINTS = 2;
        private const int MAXIMUM_DISTANCE = 20;

        public override RaycastHit[] CastAll(Transform origin)
        {
            return Physics.SphereCastAll(origin.position, 0.05f, origin.forward, MAXIMUM_DISTANCE);
        }

        public override bool Cast(Transform origin, out RaycastHit hit)
        {
            if(Physics.SphereCast(origin.position, 0.05f, origin.forward, out hit, MAXIMUM_DISTANCE))
            {
                return true;
            }
            return false;
        }

        public override void ShowCast(bool show, Transform origin = null)
        {
            m_renderer.enabled = show;
            m_isShowing = show;
            m_origin = origin;
        }

        private void Awake()
        {
            m_renderer = GetComponent<LineRenderer>();
            m_renderer.widthMultiplier = 0.05f;
            m_renderer.positionCount = COUNT_POINTS;
        }

        private void LateUpdate()
        {
            if(m_isShowing)
            {
                var points = new Vector3[COUNT_POINTS];
                float step;
                Vector3 direction;

                if(m_has_end)
                {
                    var vec = m_end - m_origin.position;
                    step = vec.magnitude / ((float)COUNT_POINTS - 1.0f);
                    direction = vec.normalized;
                }
                else
                {
                    step = (float)MAXIMUM_DISTANCE / ((float)COUNT_POINTS - 1.0f);
                    direction = m_origin.transform.forward;
                }
                
                for (int i = 0; i < COUNT_POINTS; i++)
                {
                    points[i] = m_origin.transform.position + direction * i * step;
                }
                m_renderer.SetPositions(points);
            }
        }

        public override void AttachTo(Vector3 end = default)
        {
            m_has_end = end != default;
            m_end = end;
        }
    }
}
