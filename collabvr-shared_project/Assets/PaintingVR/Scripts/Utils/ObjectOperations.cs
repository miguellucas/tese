﻿using Painter.VR.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Painter.Utils
{
    public abstract class ObjectOperations
    {
        /// <summary>
        /// Using a sphere cast, this function checks the for objects of a certain type near a GameObject and returns a list of potencial candidates.
        /// </summary>
        /// <param name="radius">Radius of the sphere cast.</param>
        /// <returns>List of nearby selectable objects.</returns>
        public static T[] CheckForSelectables<T>(Transform origin, SimpleHandVR hand, float radius)
        {
            LinkedList<T> previews = new LinkedList<T>();

            // Check for Selectable object right at this location
            RaycastHit[] objects;
            var position = origin.position;
            if ((objects = Physics.SphereCastAll(hand.attachmentPoint.position, radius, Vector3.forward, 0.05f)) != null)
            {
                foreach (RaycastHit hit in objects)
                {
                    T preview = hit.collider.gameObject.GetComponent<T>();
                    if (preview != null)
                        previews.AddLast(preview);
                }
            }

            return previews.ToArray();
        }
    }
}
