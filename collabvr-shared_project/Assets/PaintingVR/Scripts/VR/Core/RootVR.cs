﻿using System;
using UnityEngine;
using Photon.Pun;
using Painter.Network.Utils;
using Painter.Utils;

namespace Painter.VR.Core
{
    public class RootVR : MonoBehaviour
    {
        public GameObject GetHead()
        {
            foreach(Transform child in transform)
            {
                if (child.name.StartsWith("Camera"))
                    return child.gameObject;
            }
            return null;
        }
    }
}
