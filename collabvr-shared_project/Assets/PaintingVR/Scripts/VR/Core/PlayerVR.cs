﻿using System;
using UnityEngine;
using Photon.Pun;
using Painter.Network.Utils;
using Painter.Utils;
using Painter.Communication;

namespace Painter.VR.Core
{
    /// <summary>
    /// This class ensure the correct behavior of the a VR Player in the scene.
    /// It handles the display of graphics to all other clients, and swaps those graphics when it is not connected.
    /// </summary>
    public class PlayerVR : MonoBehaviourPunCallbacks
    {
        #region Impose Singleton

        static private PlayerVR instance;
        static public PlayerVR Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        static public SimpleHandVR[] Hands
        {
            get
            {
                var leftHandVR = instance.m_leftHand != null ? instance.m_leftHand.GetComponent<SimpleHandVR>() : null;
                var rightHandVR = instance.m_rightHand != null ? instance.m_rightHand.GetComponent<SimpleHandVR>() : null;
                return new SimpleHandVR[] { leftHandVR, rightHandVR };
            }
        }

        #region Local Objects

        [Header("Objects that won't be networked")]
        
        public GameObject m_camera;
        // [Tooltip("Left Hand object. WARNING: Objects will follow this, some make sure to place the camera script on the root of this object.")]
        // public GameObject leftHand;
        public GameObject m_leftHand;
        // [Tooltip("Right Hand object. WARNING: Objects will follow this, some make sure to place the camera script on the root of this object.")]
        // public GameObject rightHand;
        public GameObject m_rightHand;
        [Tooltip("These prefabs will also be spawned and locally controlled")]
        public GameObject[] otherPrefabs;
        private GameObject[] m_others;

        #endregion

        #region Network graphics

        [Space]
        [Header("Objects that will be networked")]

        [Tooltip("Camera graphics. WARNING: This object will follow the camera and will be networked. A photon view will be placed if there is none.")]
        public InstantiationPrefabs cameraGraphics;
        public InstantiationPrefabs hologramGraphics;

        #endregion Graphics objects on the scene

        /// <summary>
        /// The camera graphics that represent a player's head for all other players.
        /// It follows the camera without being a child object.
        /// This GameObject always placed in the scene (except for if it's null).
        /// </summary>
        private GameObject m_cameraGraphics;
        private GameObject m_hologramGraphics;

        #region 


        private string vrPlayer = "VR#undefined";
        private GameObject m_vrPlayer;
        private RootVR m_rootScript;
        private string rootName;

        #endregion

        /// <summary>
        /// Public Player_VR constructor that ensures the singleton pattern. 
        /// In case of the pattern being broken (more than one instance) an error is sent out to the Debug console.
        /// </summary>
        public PlayerVR()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>PlayerVR</b>: Multiple instances of PlayerVR are not supported.");
            }
#endif

            instance = this;
        }

        #region GameObject loading

        private void InstantiateNetworkPlayerRoot()
        {
            InstantiationInfo info = new InstantiationInfo()
            {
                prefabs = new InstantiationPrefabs
                {
                    networkPrefab = vrPlayer
                },
                networkData = new NetworkInstantiationData()
                {
                    gameObjectName = rootName,
                    parentName = "Root"
                },
                position = Vector3.zero,
                rotation = Quaternion.identity
            };

            m_vrPlayer = NetworkInstantiation.Instantiate(info);
            m_vrPlayer.name = rootName;

            m_rootScript = m_vrPlayer.AddComponent<RootVR>();
        }

        private void SetGraphicsLayer(Transform m_object, LayerMask layer)
        {
            m_object.gameObject.layer = layer;
            foreach(Transform m_child in m_object)
            {
                SetGraphicsLayer(m_child, layer);
            }
        }

        /// <summary>
        /// Instatiates a GameObject into the scence.
        /// If it is intended to be a local object then the local prefab is used and is spawned using the GameObject.Instantiate() method.
        /// If it is a networked object, all players have access to it, then it is spawned using PhotonNetwork.Instantiate().
        /// </summary>
        /// <param name="instantiationInfo">Holds the information about the prefabs of both the local and networked objects.</param>
        /// <param name="currentObject">The object that will be spawned.</param>
        /// <param name="switchToNetwork">Flag that is set when the player connects.</param>
        private void LoadObject(GameObject mirror, InstantiationPrefabs instantiationInfo, out GameObject spawnedObject, bool switchToNetwork)
        {
            InstantiationInfo info = new InstantiationInfo()
            {
                position = Vector3.zero,
                rotation = Quaternion.identity
            };

            if (!switchToNetwork)
            {
                info.prefabs = new InstantiationPrefabs
                {
                    localPrefab = instantiationInfo.localPrefab
                };
                info.localData = new LocalInstantiationData()
                {
                    name = mirror.name,
                    parent = this.transform
                };
            }
            else
            {
                info.prefabs = new InstantiationPrefabs
                {
                    networkPrefab = instantiationInfo.networkPrefab
                };
                info.networkData = new NetworkInstantiationData()
                {
                    gameObjectName = mirror.name,
                    parentName = rootName
                };
            }

            spawnedObject = NetworkInstantiation.Instantiate(info);
            if(switchToNetwork)
            {
                GraphicsObject script = spawnedObject.AddComponent<GraphicsObject>();
                script.follow = mirror.transform;
            }
        }

        /// <summary>
        /// Loads the graphics GameObjects that will be in the scene.
        /// This is due an update on the connection status of the player.
        /// </summary>
        /// <param name="switchToNetwork">Flag that is set when the player connects.</param>
        protected void LoadObjects(bool switchToNetwork)
        {
            // Instantiate the player's root object on all instances of the game
            if(switchToNetwork)
            {
                InstantiateNetworkPlayerRoot();
            }

            // Load the graphics objects
            LoadObject(m_camera, cameraGraphics, out m_cameraGraphics, switchToNetwork);
            //LoadObject(m_camera, hologramGraphics, out m_hologramGraphics, switchToNetwork);

            if (switchToNetwork)
            {
                SetGraphicsLayer(m_cameraGraphics.transform, LayerMask.NameToLayer("LocalPlayer"));
                //SetGraphicsLayer(m_hologramGraphics.transform, LayerMask.NameToLayer("LocalPlayer"));
            }
        }

        /// <summary>
        /// Destorys the graphics GameObjects that are currently in the scene.
        /// This is due to an update on the connection status of the player.
        /// </summary>
        /// <param name="switchToNetwork">Flag that is set when the player connects.</param>
        protected void DestroyObjects(bool switchToNetwork)
        {
            if (switchToNetwork)
            {
                Destroy(m_cameraGraphics);
                //Destroy(m_hologramGraphics);
            }
            else if(m_cameraGraphics != null)
            {
                PhotonNetwork.Destroy(m_cameraGraphics);
                //PhotonNetwork.Destroy(m_hologramGraphics);
                PhotonNetwork.Destroy(m_vrPlayer);
            }
        }

        /// <summary>
        /// Switchs the graphics GameObjects in the scene.
        /// This is due to an update on the connection status of the players.
        /// </summary>
        /// <param name="switchToNetwork">Flag that is set when the player connects.</param>
        protected void SwitchObjects(bool switchToNetwork)
        {
            DestroyObjects(switchToNetwork);
            LoadObjects(switchToNetwork);
        }

        #endregion

        #region Unity methods

        protected virtual void Start()
        {
            // Load local objects
            { 
                //m_leftHand = Instantiate(leftHand, this.transform);
                //m_leftHand.transform.localPosition = Vector3.zero;
                //m_leftHand.transform.localRotation = Quaternion.identity;

                //m_rightHand = Instantiate(rightHand, this.transform);
                //m_rightHand.transform.localPosition = Vector3.zero;
                //m_rightHand.transform.localRotation = Quaternion.identity;

                if (m_leftHand != null && m_rightHand != null) {
                    SimpleHandVR lhand = m_leftHand.GetComponent<SimpleHandVR>();
                    SimpleHandVR rhand = m_rightHand.GetComponent<SimpleHandVR>();
                    lhand.otherHand = rhand;
                    rhand.otherHand = lhand;
                }
                
            }

            // Load graphics objects
            LoadObjects(false);

            if(PhotonNetwork.IsConnected)
            {
                InitializePlayer(true);
            }

            // Subscribe to connection status events
            ConnectionStatus mStatus = ConnectionStatus.Instance;
            if (mStatus != null)
            {
                mStatus.OnRoomUpdate.AddListener(OnRoomUpdate);
            }
        }
        
        public void OnDestroy()
        {
            instance = null;
        }

        #endregion

        /// <summary>
        /// Called when the connection status of player to a room changes.
        /// </summary>
        /// <param name="joined">Flag that is set when the player joins a room.</param>
        private void OnRoomUpdate(bool joined)
        {
            InitializePlayer(joined);
        }

        /// <summary>
        /// Called to initialize the VR player.
        /// </summary>
        /// <param name="init">Flag that is set when the player should be initialized.</param>
        private void InitializePlayer(bool init)
        {
            rootName = PlayerUtils.RootName(PhotonNetwork.LocalPlayer);

            SwitchObjects(init);

            if (PhotonNetwork.CurrentRoom.PlayerCount > 1) {
                GameManager.instance.settings.setObserver();
                //GameManager.instance.settings.setManipulator();
                NetworkManager.instance.sendSetManipulator();
            } else {
                GameManager.instance.settings.setSinglePlayer();
                //GameManager.instance.settings.setObserver();
            }
        }
    }
}
