﻿using System;
using UnityEngine;
using Photon.Pun;
using Valve.VR;
using UnityEngine.Events;
using System.Collections;

namespace Painter.VR.Core
{
    /// <summary>
    /// Simple hand is an hand script that encapsulates all the data that might come from
    /// VR input sources. That is the translation and rotation, as well as, button clicks.
    /// This class should be overriden by a child class that handles all the inputs
    /// and represents the hand with external models.
    /// </summary>
    public class SimpleHandVR : MonoBehaviour
    {
        public SimpleHandVR otherHand;
        public virtual SimpleHandVR OtherHand
        {
            get
            {
                return otherHand;
            }
        }

        #region Attachment Points

        public Transform attachmentPoint;
        public Transform markerPoint;

        #endregion

        #region Steam VR input sources

        public SteamVR_Input_Sources handType;
        public SteamVR_Behaviour_Pose trackedObject;
        public SteamVR_Action_Boolean grabPinchAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
        public SteamVR_Action_Boolean grabGripAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabGrip");
        public SteamVR_Action_Vibration hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");
        public SteamVR_Action_Boolean uiInteractAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("InteractUI");
        public SteamVR_Action_Boolean teleportAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Teleport");
        protected SteamVR_Events.Action inputFocusAction;

        public bool isActive
        {
            get
            {
                if (trackedObject != null)
                    return trackedObject.isActive;

                return this.gameObject.activeInHierarchy;
            }
        }

        public bool isPoseValid
        {
            get
            {
                return trackedObject.isValid;
            }
        }

        #endregion

        protected PlayerVR playerInstance;

        #region Input events

        public class PoseActive : UnityEvent<SimpleHandVR> { };
        public PoseActive OnPoseActive;

        #endregion

        #region Unity methods

        /// <summary>
        /// These methods can be overriden by children classes, but make sure to
        /// call the base method.
        /// Grabs the virtual reality input components from this gameobject.
        /// </summary>
        protected virtual void Awake()
        {
            inputFocusAction = SteamVR_Events.InputFocusAction(OnInputFocus);

            if (trackedObject == null)
            {
                trackedObject = this.gameObject.GetComponent<SteamVR_Behaviour_Pose>();

                if (trackedObject != null)
                {
                    trackedObject.origin = PlayerVR.Instance.transform;
                    trackedObject.onTransformUpdatedEvent += OnTransformUpdated;
                }
            }
        }
        
        /// <summary>
        /// These methods can be overriden by children classes, but make sure to
        /// call the base method.
        /// Updates the player instance right at the Start and starts a coroutine
        /// that wait for the controllers to be active.
        /// </summary>
        protected virtual void Start()
        {
            // save off player instance
            playerInstance = PlayerVR.Instance;

#if DEBUG
            if (!playerInstance)
            {
                Debug.LogError("<b>SimpleHandVR</b>: No player instance found in VRCollab_Hand Start()");
            }
#endif
            if(OnPoseActive == null)
                OnPoseActive = new PoseActive();

            StartCoroutine(WaitController());
        }
        
        /// <summary>
        /// These methods can be overriden by children classes, but make sure to
        /// call the base method.
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (trackedObject != null)
            {
                trackedObject.onTransformUpdatedEvent -= OnTransformUpdated;
            }
            StopAllCoroutines();
        }

        #endregion

        #region Updates 

        /// <summary>
        /// Subscribed event that gets called every time there is a transform update to the object.
        /// This method should be overriden by child classes that intend to changes objects that get
        /// affected by the movement of the hand.
        /// </summary>
        /// <param name="updatedPose"></param>
        /// <param name="updatedSource"></param>
        protected virtual void OnTransformUpdated(SteamVR_Behaviour_Pose updatedPose, SteamVR_Input_Sources updatedSource)
        { }

        /// <summary>
        /// Checks, every frame, if this controller is active.
        /// When it is active it send the event OnPoseActive to all subscribers.
        /// </summary>
        /// <returns>yield return null makes it wait another update to continue executing</returns>
        protected virtual IEnumerator WaitController()
        {
            while (true)
            {
                if (isPoseValid)
                {
                    // Call an event onPoseActive
                    OnPoseActive.Invoke(this);
                    break;
                }

                yield return null;
            }
        }

        #endregion

        #region VR Input methods

        protected virtual void OnInputFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                BroadcastMessage("OnParentHandInputFocusAcquired", SendMessageOptions.DontRequireReceiver);
            }
            else
            {
                BroadcastMessage("OnParentHandInputFocusLost", SendMessageOptions.DontRequireReceiver);
            }
        }

        /// <summary>
        /// Getter for the index of the device being tracked by this script
        /// </summary>
        /// <returns>Index of the device being tracked</returns>
        public int GetDeviceIndex()
        {
            return trackedObject.GetDeviceIndex();
        }

        #endregion
    }
}
