﻿using UnityEngine;
using Photon.Pun;
using Valve.VR;
using System.Collections.Generic;
using Painter.Network.Utils;

namespace Painter.VR.Core
{
    /// <summary>
    /// Simple hand is an hand script that encapsulates all the data that might come from
    /// VR input sources. That is the translation and rotation, as well as, button clicks.
    /// This class should be overriden by a child class that handles all the inputs
    /// and represents the hand with external models.
    /// </summary>
    public class ArticulatedHandVR : SimpleHandVR
    {
        #region Render Models
        
        public InstantiationPrefabs prefabs;
        protected List<RenderModelVR> renderModels = new List<RenderModelVR>();
        protected RenderModelVR mainRenderModel;
        protected RenderModelVR hoverhighlightRenderModel;

        #endregion

        #region Network information

        private PhotonView photonView;
        private bool isConnected = false;

        private void OnRoomUpdate(bool joined)
        {
            InitializeHand(joined);
        }

        private void InitializeHand(bool init)
        {
            isConnected = true;
            // ToDo: swap the models here
        }

        #endregion

        #region Unity overrides

        protected override void Awake()
        {
            base.Awake();

            photonView = GetComponent<PhotonView>();
            if(photonView != null)
            {
                if(PhotonNetwork.IsConnected)
                {
                    InitializeHand(true);
                }

                ConnectionStatus mStatus = ConnectionStatus.Instance;
                if(mStatus != null)
                {
                    mStatus.OnRoomUpdate.AddListener(OnRoomUpdate);
                }
            }
        }

        #endregion

        #region Render Model methods

        public SteamVR_Behaviour_Skeleton skeleton
        {
            get
            {
                if (mainRenderModel != null)
                    return mainRenderModel.GetSkeleton();

                return null;
            }
        }

        public void ShowController(bool permanent = false)
        {
            if (mainRenderModel != null)
                mainRenderModel.SetControllerVisibility(true, permanent);

            if (hoverhighlightRenderModel != null)
                hoverhighlightRenderModel.SetControllerVisibility(true, permanent);
        }

        public void HideController(bool permanent = false)
        {
            if (mainRenderModel != null)
                mainRenderModel.SetControllerVisibility(false, permanent);

            if (hoverhighlightRenderModel != null)
                hoverhighlightRenderModel.SetControllerVisibility(false, permanent);
        }

        public void ShowSkeleton(bool permanent = false)
        {
            if (mainRenderModel != null)
                mainRenderModel.SetHandVisibility(true, permanent);

            if (hoverhighlightRenderModel != null)
                hoverhighlightRenderModel.SetHandVisibility(true, permanent);
        }

        public void HideSkeleton(bool permanent = false)
        {
            if (mainRenderModel != null)
                mainRenderModel.SetHandVisibility(false, permanent);

            if (hoverhighlightRenderModel != null)
                hoverhighlightRenderModel.SetHandVisibility(false, permanent);
        }

        public bool HasSkeleton()
        {
            return mainRenderModel != null && mainRenderModel.GetSkeleton() != null;
        }

        public void Show()
        {
            SetVisibility(true);
        }

        public void Hide()
        {
            SetVisibility(false);
        }

        public void SetVisibility(bool visible)
        {
            if (mainRenderModel != null)
                mainRenderModel.SetVisibility(visible);
        }

        public void SetSkeletonRangeOfMotion(EVRSkeletalMotionRange newRangeOfMotion, float blendOverSeconds = 0.1f)
        {
            for (int renderModelIndex = 0; renderModelIndex < renderModels.Count; renderModelIndex++)
            {
                renderModels[renderModelIndex].SetSkeletonRangeOfMotion(newRangeOfMotion, blendOverSeconds);
            }
        }

        public void SetTemporarySkeletonRangeOfMotion(SkeletalMotionRangeChange temporaryRangeOfMotionChange, float blendOverSeconds = 0.1f)
        {
            for (int renderModelIndex = 0; renderModelIndex < renderModels.Count; renderModelIndex++)
            {
                renderModels[renderModelIndex].SetTemporarySkeletonRangeOfMotion(temporaryRangeOfMotionChange, blendOverSeconds);
            }
        }

        public void ResetTemporarySkeletonRangeOfMotion(float blendOverSeconds = 0.1f)
        {
            for (int renderModelIndex = 0; renderModelIndex < renderModels.Count; renderModelIndex++)
            {
                renderModels[renderModelIndex].ResetTemporarySkeletonRangeOfMotion(blendOverSeconds);
            }
        }

        public void SetAnimationState(int stateValue)
        {
            for (int renderModelIndex = 0; renderModelIndex < renderModels.Count; renderModelIndex++)
            {
                renderModels[renderModelIndex].SetAnimationState(stateValue);
            }
        }

        public void StopAnimation()
        {
            for (int renderModelIndex = 0; renderModelIndex < renderModels.Count; renderModelIndex++)
            {
                renderModels[renderModelIndex].StopAnimation();
            }
        }

        protected virtual void InitController()
        {
            bool hadOldRendermodel = mainRenderModel != null;
            EVRSkeletalMotionRange oldRM_rom = EVRSkeletalMotionRange.WithController;
            if (hadOldRendermodel)
                oldRM_rom = mainRenderModel.GetSkeletonRangeOfMotion;


            foreach (RenderModelVR r in renderModels)
            {
                if (r != null)
                    Destroy(r.gameObject);
            }

            renderModels.Clear();
            
            InstantiationInfo info;
            if (!isConnected)
            {
                info = new InstantiationInfo()
                {
                    prefabs = new InstantiationPrefabs
                    {
                        localPrefab = prefabs.localPrefab
                    },
                    localData = new LocalInstantiationData()
                    {
                        name = prefabs.localPrefab.name,
                        parent = this.transform
                    },
                    position = Vector3.zero,
                    rotation = Quaternion.identity
                };
            }
            else
            {
                info = new InstantiationInfo()
                {
                    prefabs = new InstantiationPrefabs
                    {
                        networkPrefab = prefabs.networkPrefab
                    },
                    networkData = new NetworkInstantiationData()
                    {
                        gameObjectName = prefabs.networkPrefab,
                        parentName = this.transform.name
                    },
                    position = Vector3.zero,
                    rotation = Quaternion.identity
                };
            }

            GameObject renderModelInstance = NetworkInstantiation.Instantiate(info);
            renderModelInstance.transform.localPosition = Vector3.zero;
            renderModelInstance.transform.localRotation = Quaternion.identity;

            renderModelInstance.layer = gameObject.layer;
            renderModelInstance.tag = gameObject.tag;
            mainRenderModel = renderModelInstance.GetComponent<RenderModelVR>();
            renderModels.Add(mainRenderModel);

            int deviceIndex = trackedObject.GetDeviceIndex();

            if (hadOldRendermodel)
                mainRenderModel.SetSkeletonRangeOfMotion(oldRM_rom);

            this.BroadcastMessage("SetInputSource", handType, SendMessageOptions.DontRequireReceiver);          // let child objects know we've initialized
            this.BroadcastMessage("OnHandInitialized", deviceIndex, SendMessageOptions.DontRequireReceiver);    // let child objects know we've initialized
        }

        public void SetRenderModel(string prefab)
        {
            prefabs.networkPrefab = prefab;

            if (mainRenderModel != null && isPoseValid)
                InitController();
        }

        public void SetRenderModel(GameObject prefab)
        {
            prefabs.localPrefab = prefab;

            if (mainRenderModel != null && isPoseValid)
                InitController();
        }

        public void SetHoverRenderModel(RenderModelVR hoverRenderModel)
        {
            hoverhighlightRenderModel = hoverRenderModel;
            renderModels.Add(hoverRenderModel);
        }

        #endregion
    }
}
