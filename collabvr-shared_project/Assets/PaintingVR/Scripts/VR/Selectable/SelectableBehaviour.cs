﻿using Painter.Network.Selectable;
using Painter.VR.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Input
{

    public class SelectableBehaviour : SimpleActionHandler
    {
        protected struct SelectableHandState
        {
            public SimpleHandVR hand;
            public Selectable selected;
            public bool grabbed;

            public IEnumerator dropCoroutine;
            public MeshRenderer errorMesh;
        }

        /// <summary>
        /// Radius of the Sphere cast for discover selectables nearby
        /// </summary>
        public float checkRadius = 0.1f;

        public LayerMask selectableColliders;

        public Material dropErrorMaterial;

        protected SelectableHandState m_leftState;
        protected SelectableHandState m_rightState;

        protected SteamVR_Action_Vibration m_hapticAction;

        public SelectableBehaviour()
        {
            actionName = "GrabPinch";
        }
        
        protected override void StartBehaviour(SimpleHandVR hand)
        {
            base.StartBehaviour(hand);

            m_hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");

            var state = new SelectableHandState
            {
                hand = hand,
                selected = null,
                grabbed = false
            };

            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                m_leftState = state;
            }
            else if(hand.handType == SteamVR_Input_Sources.RightHand)
            {
                m_rightState = state;
            }
        }

        #region Grab

        /// <summary>
        /// Callback function to the grab event sent from the Selectable object
        /// </summary>
        /// <param name="info">The reference of the selected object and a flag that is set if the object was grabbed.</param>
        protected void GrabEvent(GrabInformation info)
        {
            if (info.isGrab)
            {
                SteamVR_Input_Sources source = SteamVR_Input_Sources.Any;
                if (
                    m_left_hand != null &&
                    (info.pivotAttachment == m_left_hand.attachmentPoint ||
                    info.otherAttachment == m_left_hand.attachmentPoint))
                {
                    source = SteamVR_Input_Sources.LeftHand;
                    m_leftState.grabbed = true;
                }
                else if (
                    m_right_hand != null && 
                    (info.pivotAttachment == m_right_hand.attachmentPoint ||
                    info.otherAttachment == m_right_hand.attachmentPoint))
                {
                    source = SteamVR_Input_Sources.RightHand;
                    m_rightState.grabbed = true;
                }
                
#if DEBUG
                if (m_hapticAction == null)
                {
                    Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                    return;
                }
#endif
                m_hapticAction.Execute(0, 0.2f, 120, 0.5f, source);
            }
        }

        /// <summary>
        /// Registers a grab intention to the object and subscribes to the grab events.
        /// </summary>
        /// <param name="selectable">Reference to the selectable object being grabbed.</param>
        protected virtual void GrabSelectable(ref SelectableHandState state, Selectable selectable)
        {
            state.selected = selectable;
            state.grabbed = false;

            state.selected.grabEvent.AddListener(GrabEvent);
            state.selected.RegisterGrabIntention(state.hand.attachmentPoint);
        }


        protected virtual bool SelectableIsDroppable(Selectable selectable)
        {
            // If more then one hand is grabbing the object
            // Drop it because the other will still be holding it
            if (selectable.MultipleGrabs && 
                (m_leftState.dropCoroutine == null || 
                 m_rightState.dropCoroutine == null))
                return true;

            var colliders = selectable.Colliders;
            foreach(var collider in colliders)
            {
                if (collider != null &&
                    selectableColliders == (selectableColliders | 1 << collider.gameObject.layer))
                    return false;
            }

            return true;
        }

        protected virtual IEnumerator CheckForDropSelectable(Selectable selectable, SteamVR_Input_Sources inputSource)
        {
            MeshRenderer copyMesh = null;

            while(true)
            {
                // boolean function that returns true if object is droppable
                var droppable = SelectableIsDroppable(selectable);
                if (droppable)
                {
                    if (inputSource == SteamVR_Input_Sources.LeftHand)
                    {
                        DropSelectable(ref m_leftState, selectable);
                        m_leftState.errorMesh = null;
                    }
                    else if (inputSource == SteamVR_Input_Sources.RightHand)
                    {
                        DropSelectable(ref m_rightState, selectable);
                        m_rightState.errorMesh = null;
                    }
                    break;
                }

                if(copyMesh == null)
                {
                    m_hapticAction.Execute(0, 0.5f, 60.0f, 1.0f, inputSource);

                    MeshMethods.SpawnCopyMesh(selectable.gameObject, out copyMesh);
                    copyMesh.material = dropErrorMaterial;

                    if (inputSource == SteamVR_Input_Sources.LeftHand)
                    {
                        m_leftState.errorMesh = copyMesh;
                    }
                    else if (inputSource == SteamVR_Input_Sources.RightHand)
                    {
                        m_rightState.errorMesh = copyMesh;
                    }
                }


                var closestPoints = selectable.ClosestPoints;

                Vector4[] sendClosestPoints = new Vector4[12];
                var sendClosestPointsCount = closestPoints.Length > 12 ? 12 : closestPoints.Length;

                Array.Copy(closestPoints, 0, sendClosestPoints, 0, sendClosestPointsCount);
                
                copyMesh.material.SetVectorArray("_ContactPoints", sendClosestPoints);
                copyMesh.material.SetInt("_ContactPointsCount", sendClosestPointsCount);
                copyMesh.material.SetFloat("_ContactMaxDistance", 0.4f);

                yield return null;
            }
        }


        /// <summary>
        /// Registers a drop intention to the object.
        /// </summary>
        /// <param name="selectable"></param>
        protected virtual void DropSelectable(ref SelectableHandState state, Selectable selectable)
        {
            if (state.errorMesh != null)
            {
                Destroy(state.errorMesh.gameObject);
                state.errorMesh = null;
            }

            state.dropCoroutine = null;

            state.selected.grabEvent.RemoveListener(GrabEvent);
            state.selected.RegisterDropIntention(state.hand.attachmentPoint);

            state.selected = null;
            state.grabbed = false;

#if DEBUG
            if (m_hapticAction == null)
            {
                Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                return;
            }
#endif
            m_hapticAction.Execute(0, 0.1f, 60, 0.5f, state.hand.handType);
        }

        /// <summary>
        /// Checks if the selectable object intended to be grabbed is in the list of reachable selectable objects.
        /// </summary>
        /// <param name="selectables">List of reachable and selectable objects.</param>
        /// <returns>A flag that is set if the object intended to be grabbed is in the list.</returns>
        protected bool IsGrabbingSelected(SelectableHandState state, Selectable[] selectables)
        {
            return (state.selected != null && selectables.Contains(state.selected));
        }

        /// <summary>
        /// Using a sphere cast, this function checks the selectable objects near this GameObject and returns a list of potencial candidates.
        /// </summary>
        /// <param name="radius">Radius of the sphere cast.</param>
        /// <returns>List of nearby selectable objects.</returns>
        protected Selectable[] CheckForSelectables(SimpleHandVR hand, float radius)
        {
            LinkedList<Selectable> selectables = new LinkedList<Selectable>();

            // Check for Selectable object right at this location
            Collider[] objects;
            if ((objects = Physics.OverlapSphere(hand.attachmentPoint.position, radius)) != null)
            {
                foreach (var hit in objects)
                {
                    Selectable selectable = hit.gameObject.GetComponent<Selectable>();
                    if (selectable != null)
                        selectables.AddLast(selectable);
                }
            }

            return selectables.ToArray();
        }

        #endregion

        protected override void ActionStateChange(HandInteraction handInteraction, SteamVR_Input_Sources fromSource)
        {
            if(fromSource == SteamVR_Input_Sources.LeftHand)
            {
                if(!m_left_action && handInteraction.leftHand)
                {
                    OnActionPressed(m_left_hand);
                }
                else if(m_left_action && !handInteraction.leftHand)
                {
                    OnActionReleased(m_left_hand);
                }
                m_left_action = handInteraction.leftHand;
            } 
            else if(fromSource == SteamVR_Input_Sources.RightHand)
            {
                if (!m_right_action && handInteraction.rightHand)
                {
                    OnActionPressed(m_right_hand);
                }
                else if (m_right_action && !handInteraction.rightHand)
                {
                    OnActionReleased(m_right_hand);
                }
                m_right_action = handInteraction.rightHand;
            }
        }

        protected override void OnActionPressed(SimpleHandVR origin)
        {
            if (origin.handType == SteamVR_Input_Sources.LeftHand)
            {
                ActionPressed(ref m_leftState);
            }
            else if (origin.handType == SteamVR_Input_Sources.RightHand)
            {
                ActionPressed(ref m_rightState);
            }
        }

        protected override void OnActionReleased(SimpleHandVR origin)
        {
            if(origin.handType == SteamVR_Input_Sources.LeftHand)
            {
                ActionReleased(ref m_leftState);
            }
            else if(origin.handType == SteamVR_Input_Sources.RightHand)
            {
                ActionReleased(ref m_rightState);
            }
        }

        protected void ActionPressed(ref SelectableHandState state)
        {
            if (state.grabbed)
            {
                if(state.dropCoroutine != null)
                {
                    StopCoroutine(state.dropCoroutine);
                    state.dropCoroutine = null;
                }
                if(state.errorMesh != null)
                {
                    Destroy(state.errorMesh.gameObject);
                    state.errorMesh = null;
                }
                return;
            }

            // Get list of selectable objects around this GameObject
            Selectable[] selectables = CheckForSelectables(state.hand, checkRadius);
            // If it is attempting to grab an object that is in the list then ignore
            if (IsGrabbingSelected(state, selectables))
            {
                return;
            }
            else if(selectables.Length > 0)
            {
                GrabSelectable(ref state, selectables[0]);
            }
        }

        protected void ActionReleased(ref SelectableHandState state)
        {
            if (state.selected != null &&
                state.dropCoroutine == null)
            {
                state.dropCoroutine = CheckForDropSelectable(state.selected, state.hand.handType);
                StartCoroutine(state.dropCoroutine);

                // DropSelectable(ref state, state.selected);
            }
        }

        protected bool StateFromVRHand(SimpleHandVR hand, out SelectableHandState state)
        {
            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                state = m_leftState;
                return true;
            }

            if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                state = m_rightState;
                return true;
            }

            state = new SelectableHandState();
            return false;
        }

        public override bool CanChange(SimpleHandVR hand)
        {
            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                return m_leftState.selected == null && m_leftState.dropCoroutine == null;
            }
            else if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                return m_rightState.selected == null && m_leftState.dropCoroutine == null;
            }

            return true;
        }
    }
}
