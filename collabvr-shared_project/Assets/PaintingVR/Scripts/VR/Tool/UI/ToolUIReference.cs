﻿using UnityEngine;
using UnityEngine.UI;

namespace Painter.VR.Tool
{
    public class ToolUIReference : MonoBehaviour
    {
        public IToolManager tool;
        public ToolUIManager uiManager;

        public RawImage image;
        public Color activeColor;
        public Color inactiveColor;
        
        private bool m_toolSelected = false;
        
        private bool ToolIsHighlighted()
        {
            if(tool is ToolManager)
            {
                ToolManager toolManager = (ToolManager)tool;
                return m_toolSelected ||
                    (uiManager.inputSource == Valve.VR.SteamVR_Input_Sources.LeftHand && toolManager.LeftEnabled) ||
                    (uiManager.inputSource == Valve.VR.SteamVR_Input_Sources.RightHand && toolManager.RightEnabled);
            }

            return m_toolSelected;
        }

        public void EnableTool(bool enable)
        {
            m_toolSelected = enable;
            ResetMaterial();
        }

        private void FixedUpdate()
        {
            ResetMaterial();
        }

        public void ResetMaterial()
        {
            if (ToolIsHighlighted())
            {
                image.color = activeColor;
            }
            else
            {
                image.color = inactiveColor;
            }
        }


    }
}
