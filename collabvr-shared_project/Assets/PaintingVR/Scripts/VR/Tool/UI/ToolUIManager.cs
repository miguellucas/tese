﻿using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

namespace Painter.VR.Tool
{
    public class ToolUIManager : MonoBehaviour
    {
        public Canvas canvas;
        public SteamVR_Input_Sources inputSource;

        [HideInInspector]
        public Tools availableTools;
        [HideInInspector]
        public ToolUIReference[] uiReferences;
        
        protected Vector2 PositionByIndex(int index, int length)
        {
            // Angle in radians
            float angle = index * Mathf.PI * 2.0f / length;
            float xValue = Mathf.Cos(angle);
            float yValue = Mathf.Sin(angle);
            return new Vector2(xValue, yValue);
        }

        protected Vector2 CanvasPositionByIndex(int index, int length, float width, float height)
        {
            Vector2 position = PositionByIndex(index, length);
            // Scale the position according to the canvas size
            position.Scale(new Vector2(width / 2.0f, height / 2.0f));
            return position;
        }
        
        public int IndexFromAngle(float angle)
        {
            if(angle < 0)
            {
                angle += Mathf.PI * 2.0f;
            }

            var intervalPerTool = Mathf.PI * 2.0f / availableTools.tools.Length;
            float approximateIndex = (angle + intervalPerTool / 2.0f) / intervalPerTool;
            int index = Mathf.FloorToInt(approximateIndex);
            index = index >= availableTools.tools.Length ? index % availableTools.tools.Length : index;
            return index;
        }

        protected bool ToolActiveForInput(ToolManager tool)
        {
            if (inputSource == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                return tool.LeftEnabled;
            }
            else if (inputSource == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                return tool.RightEnabled;
            }
            return false;
        }

        protected virtual GameObject InstantiateUITool(int index)
        {
            var toolUI = availableTools.tools[index];

            var toolUIGameObject = Instantiate(toolUI.UIRepresentation, this.transform);
            
            var toolReference = toolUIGameObject.GetComponent<ToolUIReference>();
#if DEBUG
            if(toolReference == null)
            {
                Debug.LogError("<b>ToolUIManager</b>: UI representations require the script ToolUIReference.");
                return null;
            }
#endif
            toolReference.tool = toolUI.tool;

            uiReferences[index] = toolReference;
            availableTools.tools[index] = toolUI;

            return toolUIGameObject;
        }

        protected virtual void CreateUITool(int index)
        {
            var canvasPosition = CanvasPositionByIndex(index, availableTools.tools.Length, 
                                                       600.0f / 1.2f, 
                                                       600.0f / 1.2f);

            var uiGameObject = InstantiateUITool(index);

#if DEBUG
            if (uiGameObject == null)
                return;
#endif

            uiGameObject.transform.localPosition = new Vector3(canvasPosition.x, canvasPosition.y, 0.0f);

            uiReferences[index].uiManager = this;
            uiReferences[index].ResetMaterial();
        }

        protected virtual void DestroyUITool(int index)
        {
            var toolUI = availableTools.tools[index];

            if(uiReferences[index] != null)
                Destroy(uiReferences[index].gameObject);

            uiReferences[index] = null;
            availableTools.tools[index] = toolUI;
        }

        protected virtual void CreateUI()
        {
            for(int i = 0; i < availableTools.tools.Length; i++)
            {
                CreateUITool(i);
            }
        }

        protected virtual void DestroyUI()
        {
            for (int i = 0; i < availableTools.tools.Length; i++)
            {
                DestroyUITool(i);
            }
        }

        public ToolUIReference GetReferenceFromAngle(float angle)
        {
            return uiReferences[IndexFromAngle(angle)];
        }

        public void SetAvailableTools(Tools tools)
        {
            if(availableTools != null)
                DestroyUI();

            availableTools = tools;
            uiReferences = new ToolUIReference[availableTools.tools.Length];

            CreateUI();
        }
    }
}
