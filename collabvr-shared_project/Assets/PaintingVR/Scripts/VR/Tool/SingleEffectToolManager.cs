﻿using Painter.VR.Core;
using System;
using System.Collections;
using UnityEngine;

namespace Painter.VR.Tool
{
    public class SingleEffectToolManager : IToolManager
    {        
        /// <summary>
        /// Object that defines this object's behaviour.
        /// </summary>
        public ToolBehaviour behaviour;

        public override void EnableTool(SimpleHandVR hand, IToolManager previousTool = null)
        {
            BehaviourEnable(hand);
        }

        public override void DisableTool(SimpleHandVR hand, IToolManager newTool = null)
        {
            BehaviourDisable(hand);
        }

        public override void BehaviourEnable(SimpleHandVR hand)
        {
            // Activate the effect
            behaviour.EnableTool(true, hand);
            // Disable this tool
            DisableTool(hand);
        }

        public override void BehaviourDisable(SimpleHandVR hand)
        {
            // Disable the effect
            behaviour.EnableTool(false, hand);
        }

        public override bool CanChange(SimpleHandVR hand)
        {
            // Since this tool only takes effect for an instante
            // change is always possible
            return true;
        }
    }
}
