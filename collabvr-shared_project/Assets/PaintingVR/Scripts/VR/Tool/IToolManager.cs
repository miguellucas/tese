﻿using Painter.VR.Core;
using System;
using System.Collections;
using UnityEngine;

namespace Painter.VR.Tool
{
    public abstract class IToolManager : MonoBehaviour
    {
        public abstract void EnableTool(SimpleHandVR hand, IToolManager previousTool = null);
        public abstract void DisableTool(SimpleHandVR hand, IToolManager newTool = null);
        public abstract void BehaviourEnable(SimpleHandVR hand);
        public abstract void BehaviourDisable(SimpleHandVR hand);
        public abstract bool CanChange(SimpleHandVR hand);
    }
}
