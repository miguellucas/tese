﻿using Painter.VR.Core;
using System;
using System.Collections;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Tool
{
    [Serializable]
    public struct ToolUI
    {
        public IToolManager tool;
        public GameObject UIRepresentation;
    }

    public class Tools : MonoBehaviour
    {
        #region Impose Singleton

        static private Tools instance;
        static public Tools Instance
        {
            get
            {
                return instance;
            }
        }

        public Tools()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>Tools</b> Multiple instances of Player_VR are not supported.");
            }
#endif

            instance = this;
        }

        #endregion

        public ToolManager defaultLeftHandTool;
        public ToolManager defaultRightHandTool;
        
        public ToolUI[] tools;

        public ToolUIManager leftHandToolManager;
        public ToolUIManager rightHandToolManager;

        private IEnumerator Start()
        {
            while (true)
            {
                var hands = PlayerVR.Hands;
                if (hands[0] != null && !defaultLeftHandTool.LeftEnabled)
                {
                    defaultLeftHandTool.EnableTool(hands[0]);
                    EnableUIToolManager(hands[0]);
                }

                if (hands[1] != null && !defaultRightHandTool.RightEnabled)
                {
                    defaultRightHandTool.EnableTool(hands[1]);
                    EnableUIToolManager(hands[1]);
                }

                if (hands[0] != null && hands[1] != null)
                {
                    break;
                }

                yield return null;
            }
        }

        public void EnableUIToolManager(SimpleHandVR hand)
        {
            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                leftHandToolManager.SetAvailableTools(this);
            }
            else if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                rightHandToolManager.SetAvailableTools(this);
            }
        }
    }
}
