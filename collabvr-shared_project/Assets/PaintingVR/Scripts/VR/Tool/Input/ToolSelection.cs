﻿using Painter.VR.Behaviour;
using Painter.VR.Core;
using Painter.VR.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Tool
{
    public class ToolSelection : VRBehaviour
    {
        public SteamVR_Action_Vector2 actionMove;

        public enum SelectionState { IDLE, CLICK, CENTER, WHEEL};

        [Serializable]
        public struct HandToolSelection
        {
            [HideInInspector]
            public SimpleHandVR hand;
            [HideInInspector]
            public Vector2 handAction;
            [HideInInspector]
            public bool handIsActive;

            [HideInInspector]
            public bool selectionActive;

            [HideInInspector]
            public ToolUIReference highlightedReference;
            [HideInInspector]
            public bool newToolHighlighted;
            
            public AudioController onActivateAudio;
            public AudioController onSelectedAudio;

            public SelectionState state;
        }
        
        public HandToolSelection leftHandTool;
        public HandToolSelection rightHandTool;

        protected SteamVR_Action_Vibration m_hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");
        protected SteamVR_Action_Boolean m_joystickClick = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Teleport");


        private IEnumerator Start()
        {
            while (true)
            {
                var hands = PlayerVR.Hands;
                if (hands[0] != null && hands[1] != null &&
                    ToolManager.HandTool(hands[0]) != null &&
                    ToolManager.HandTool(hands[1]) != null)
                {
                    leftHandTool.hand = hands[0];
                    leftHandTool.state = SelectionState.IDLE;
                    rightHandTool.hand = hands[1];
                    rightHandTool.state = SelectionState.IDLE;

                    StartBehaviour(hands[0]);
                    StartBehaviour(hands[1]);

                    break;
                }
                yield return null;
            }
        }

        private void Update()
        {
            UpdateHandAction(ref leftHandTool);
            UpdateHandAction(ref rightHandTool);
        }


        protected ToolUIManager GetToolUIManager(SimpleHandVR hand)
        {
            if(hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                return Tools.Instance.leftHandToolManager;
            }
            else if(hand.handType == SteamVR_Input_Sources.RightHand)
            {
                return Tools.Instance.rightHandToolManager;
            }
            return null;
        }

        protected void EnableCanvas(bool isEnable, ToolUIManager handUIManager, ref HandToolSelection handToolSelection)
        {
            // Activate the canvas
            handUIManager.gameObject.SetActive(isEnable);
        }

        protected ToolUIReference GetToolReferenceByAxis(Vector2 axis, ToolUIManager handUIManager)
        {
            // Use the angle formed throught the axis to get the tool that is 
            // being hovered over
            var angle = Mathf.Atan2(axis.y, axis.x);
            return handUIManager.GetReferenceFromAngle(angle);
        }

        protected void HighlightToolReference(bool isHighlight, ref HandToolSelection handToolSelection)
        {
            // Enable/Disable the tool reference and set the flag to notify of a change in tool
            handToolSelection.highlightedReference.EnableTool(isHighlight);
            handToolSelection.newToolHighlighted = isHighlight;
        }

        protected void EnableToolSelection(bool isEnable, ToolUIManager handUIManager, ref HandToolSelection handToolSelection)
        {
            // If there is not a state change return
            if (handToolSelection.selectionActive == isEnable)
                return;
            
            EnableCanvas(isEnable, handUIManager, ref handToolSelection);

            // Enable and Disable the current tool behaviour accordingly
            if (isEnable)
            {
                handToolSelection.onActivateAudio.PlayAudio();
                ToolManager.HandTool(handToolSelection.hand).BehaviourDisable(handToolSelection.hand);
            }
            else
            {
                handToolSelection.onActivateAudio.StopAudio();
                ToolManager.HandTool(handToolSelection.hand).BehaviourEnable(handToolSelection.hand);
            }

            // Set the current state
            handToolSelection.selectionActive = isEnable;
        }


        protected void HighlightTool(Vector2 axis, ToolUIReference toolReference, ref HandToolSelection handToolSelection)
        {
            if (handToolSelection.highlightedReference != null)
            {
                // If the highlighted tool is the same as the selected tool return
                if (handToolSelection.highlightedReference == toolReference)
                {
                    handToolSelection.newToolHighlighted = false;
                    return;
                }

                // Disable the previously highlighted tool
                handToolSelection.highlightedReference.EnableTool(false);
            }

            handToolSelection.highlightedReference = toolReference;
            if (handToolSelection.highlightedReference.tool == ToolManager.HandTool(handToolSelection.hand))
            {
                return;
            }

            HighlightToolReference(true, ref handToolSelection);
        }

        protected void SelectNewTool(ref HandToolSelection handToolSelection)
        {
            HighlightToolReference(false, ref handToolSelection);

            var currentTool = ToolManager.HandTool(handToolSelection.hand);

            // If the highlighted tool is the same as the selected tool return
            if (handToolSelection.highlightedReference.tool == currentTool)
                return;

            // Specific behaviour depending on the type of ToolManager (?)
            if (handToolSelection.highlightedReference.tool is ToolManager)
            {
                currentTool.DisableTool(handToolSelection.hand, handToolSelection.highlightedReference.tool);

                // Select a new tool
                handToolSelection.highlightedReference.tool.EnableTool(handToolSelection.hand, currentTool);

                // Play audio
                handToolSelection.onSelectedAudio.PlayAudio();
            }
            else
            {
                // Select a new tool
                handToolSelection.highlightedReference.tool.EnableTool(handToolSelection.hand, currentTool);

                handToolSelection.highlightedReference.EnableTool(false);
                handToolSelection.highlightedReference = null;
            }
            
#if DEBUG
            if (m_hapticAction == null)
            {
                Debug.LogError("<b>SteamVR</b> Action named Haptic not found for this device.");
                return;
            }
#endif
            m_hapticAction.Execute(0, 0.1f, 40, 0.5f, handToolSelection.hand.handType);
        }


        protected void AxisActive(Vector2 axis, ref HandToolSelection handToolSelection)
        {
            var handUIManager = GetToolUIManager(handToolSelection.hand);

            // If a change is not possible at this moment return
            if(!ToolManager.HandTool(handToolSelection.hand).CanChange(handToolSelection.hand))
            {
                EnableToolSelection(false, handUIManager, ref handToolSelection);
                return;
            }

            var toolReference = GetToolReferenceByAxis(axis, handUIManager); // Get the highlighted tool

            EnableToolSelection(true, handUIManager, ref handToolSelection);
            HighlightTool(axis, toolReference, ref handToolSelection);
        }

        protected void AxisInactive(ref HandToolSelection handToolSelection)
        {
            // On new tool selected
            if (handToolSelection.selectionActive)
            {
                var handManager = GetToolUIManager(handToolSelection.hand);

                EnableToolSelection(false, handManager, ref handToolSelection);
                SelectNewTool(ref handToolSelection);
            }
        }

        protected virtual void UpdateHandState(ref HandToolSelection handToolSelection)
        {
            var axis = actionMove.GetAxis(handToolSelection.hand.handType);
            var click = m_joystickClick.GetState(handToolSelection.hand.handType);

            if(handToolSelection.state == SelectionState.WHEEL)
            {
                // Check if it is still in the wheel
                if(axis.magnitude < 0.5f)
                {
                    handToolSelection.state = SelectionState.CENTER;
                }
                return;
            }

            if(click)
            {
                handToolSelection.state = SelectionState.CLICK;
                return;
            }
            else if(handToolSelection.state == SelectionState.CLICK)
            {
                handToolSelection.state = SelectionState.IDLE;
                return;
            }

            if(handToolSelection.state == SelectionState.CENTER)
            {
                // Check if it is still in the wheel
                if (axis.magnitude >= 0.5f)
                {
                    handToolSelection.state = SelectionState.WHEEL;
                }
                else if(axis == Vector2.zero)
                {
                    handToolSelection.state = SelectionState.IDLE;
                }
                return;
            }

            if(handToolSelection.state == SelectionState.IDLE)
            {
                if(axis.magnitude < 0.5f && axis != Vector2.zero)
                {
                    handToolSelection.state = SelectionState.CENTER;
                }
            }

        }


        protected virtual void UpdateHandAction(ref HandToolSelection handToolSelection)
        {
            if (!handToolSelection.handIsActive || handToolSelection.hand == null)
                return;

            UpdateHandState(ref handToolSelection);

            // Enable the UI
            // Show the currently selected option
            // Show the previous of the next option being hovered over
            if (handToolSelection.state == SelectionState.WHEEL)
            {
                AxisActive(actionMove.GetAxis(handToolSelection.hand.handType), ref handToolSelection);
            }
            else
            {
                AxisInactive(ref handToolSelection);
            }
        }


        protected override void StartBehaviour(SimpleHandVR hand)
        {
            m_hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");

            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                leftHandTool.handIsActive = true;
            }
            else if(hand.handType == SteamVR_Input_Sources.RightHand)
            {
                rightHandTool.handIsActive = true;
            }
        }

        protected override void StopBehaviour(SimpleHandVR hand)
        {
            if (hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                leftHandTool.handIsActive = false;
            }
            else if (hand.handType == SteamVR_Input_Sources.RightHand)
            {
                rightHandTool.handIsActive = false;
            }
        }

        public override bool CanChange(SimpleHandVR hand)
        {
            // Omnipresent behaviour
            return false;
        }
    }
}
