﻿using System;
using UnityEngine;
using Painter.VR.Behaviour;
using Painter.VR.Core;

namespace Painter.VR.Tool
{
    /// <summary>
    /// This class defines the behaviour of a tool.
    /// Using an array of behaviours, developers can mix and match different behaviours to work across multiple tools.
    /// </summary>
    public class ToolBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Array of behaviours that will run for this tool.
        /// </summary>
        public VRBehaviour[] behaviours;

        /// <summary>
        /// This function can be called by other scripts to switch tool.
        /// </summary>
        /// <param name="enable">Flag used to differentiate between enable and disable.</param>
        /// <param name="hand">VR hand that requires action.</param>
        public void EnableTool(bool enable, SimpleHandVR hand)
        {
            if (!enable)
                StopToolBehaviour(hand);
            else
                StartToolBehaviour(hand);
        }
        
        /// <summary>
        /// Query all behaviours if they can be stopped at this moment.
        /// </summary>
        /// <param name="hand">VR hand that is being queried.</param>
        /// <returns>True if it is possible to change all behaviours right at this moment and false otherwise.</returns>
        public bool CanChange(SimpleHandVR hand)
        {
            bool cannotChange = false;
            foreach (var behaviour in behaviours)
            {
                // If all canchange return true then cannotchange will
                // be false in the end
                cannotChange = cannotChange || !behaviour.CanChange(hand);
            }
            return !cannotChange;
        }

        /// <summary>
        /// Starts the behaviours for this tool.
        /// This function allows for customazation through an override.
        /// Make sure to call the base class or the function EnableAllBehaviours to enable the behaviours for this tool.
        /// </summary>
        /// <param name="hand">VR hand that requires action.</param>
        protected virtual void StartToolBehaviour(SimpleHandVR hand)
        {
            EnableAllBehaviours(true, hand);
        }

        /// <summary>
        /// Stop the behaviours for this tool.
        /// This function allows for customazation through an override.
        /// Make sure to call the base class or the function EnableAllBehaviours to disable the behaviours for this tool.
        /// </summary>
        /// <param name="hand">VR hand that requires action.</param>
        protected virtual void StopToolBehaviour(SimpleHandVR hand)
        {
            EnableAllBehaviours(false, hand);
        }

        /// <summary>
        /// Enables/Disables all the behaviours from this tool.
        /// </summary>
        /// <param name="enable">Flag used to differentiate between enable and disable.</param>
        /// <param name="hand">VR hand that requires action.</param>
        protected void EnableAllBehaviours(bool enable, SimpleHandVR hand)
        {
            foreach (var behaviour in behaviours)
            {
                behaviour.EnableBehaviour(enable, hand);
            }
        }
    }
}
