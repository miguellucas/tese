﻿using System;
using UnityEngine;
using Painter.Network.Utils;
using Painter.VR.Core;

namespace Painter.VR.Tool
{
    public abstract class ToolRepresentation : MonoBehaviour
    {
        /// <summary>
        /// Instantiated left hand graphics object for this tool.
        /// </summary>
        protected GameObject m_left_hand_graphics;
        /// <summary>
        /// Instantiated right hand graphics object for this tool.
        /// </summary>
        protected GameObject m_right_hand_graphics;

        /// <summary>
        /// This function can be called by other scripts to switch tool representations.
        /// </summary>
        /// <param name="show">Flag used to differentiate between enable and disable.</param>
        /// <param name="hand">VR hand that requires action.</param>
        public void ShowTool(bool show, SimpleHandVR hand)
        {
            if (!show)
                DestroyGraphics(hand);
            else
                InstantiateGraphics(hand);
        }

        /// <summary>
        /// Instantiate the graphics for this tool.
        /// This function is customized through inheritance, as it is an abstract method.
        /// </summary>
        /// <param name="hand">VR hand that is being initialized.</param>
        protected abstract void InstantiateGraphics(SimpleHandVR hand);
        /// <summary>
        /// Destroy the existing graphics for this tool.
        /// This function is customized through inheritance, as it is an abstract method.
        /// </summary>
        /// <param name="hand">VR hand that is being destroyed.</param>
        protected abstract void DestroyGraphics(SimpleHandVR hand);
    }
}
