﻿using Painter.Network.Utils;
using Painter.Utils;
using Painter.VR.Core;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;

namespace Painter.VR.Tool
{
    /// <summary>
    /// Class that inherites from ToolRepresentation.
    /// This class is used for networked representations of tools.
    /// </summary>
    public class ToolRepresentationNetwork : ToolRepresentation
    {
        [Tooltip("Name of the left hand tool prefab located at the Resources folder.")]
        /// <summary>
        /// Name of the left hand tool prefab located at the Resources folder.
        /// </summary>
        public string leftHandGraphics;
        [Tooltip("Name of the right hand tool prefab located at the Resources folder.")]
        /// <summary>
        /// Name of the right hand tool prefab located at the Resources folder.
        /// </summary>
        public string rightHandGraphics;


        /// <summary>
        /// Coroutine waiting for the left hand to be available.
        /// </summary>
        protected IEnumerator leftHandWait = null;
        /// <summary>
        /// Coroutine waiting for the right hand to be available.
        /// </summary>
        protected IEnumerator rightHandWait = null;

        

        /// <summary>
        /// Destroys the networked representation of this tool.
        /// </summary>
        /// <param name="hand">VR hand that is being destroyed.</param>
        protected override void DestroyGraphics(SimpleHandVR hand)
        {
            if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                if (leftHandWait != null)
                    StopCoroutine(leftHandWait);
                if (m_left_hand_graphics != null)
                    PhotonNetwork.Destroy(m_left_hand_graphics);
            }
            else if (hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                if (rightHandWait != null)
                    StopCoroutine(rightHandWait);
                if (m_right_hand_graphics != null)
                    PhotonNetwork.Destroy(m_right_hand_graphics);
            }
        }


        /// <summary>
        /// Coroutine that waits for the VR hands to be initialized.
        /// After the wait, it spawns the networked graphics for the tools.
        /// </summary>
        /// <param name="hand">VR hand that is being initialized.</param>
        /// <returns>IEnumerator since this method is asynchronous.</returns>
        protected virtual IEnumerator InitializeHand(SimpleHandVR hand)
        {
            if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                // Wait for player to be connected
                leftHandWait = WaitAvailable();
                yield return leftHandWait;
                leftHandWait = null;

                // Initialize hand
                m_left_hand_graphics = InstantiateObject(leftHandGraphics, hand);
            }
            else if(hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                // Wait for player to be connected
                rightHandWait = WaitAvailable();
                yield return rightHandWait;
                rightHandWait = null;

                // Initialize hand
                m_right_hand_graphics = InstantiateObject(rightHandGraphics, hand);
            }
        }

        /// <summary>
        /// Instantiates a networked representation of this tool.
        /// </summary>
        /// <param name="hand">VR hand that is being initialized.</param>
        protected override void InstantiateGraphics(SimpleHandVR hand)
        {
            StartCoroutine(InitializeHand(hand));
        }


        /// <summary>
        /// Coroutine that waits for the user to be connected and ready.
        /// </summary>
        /// <returns>IEnumerator since this method is asynchronous.</returns>
        protected IEnumerator WaitAvailable()
        {
            Transform networked_root = null;

            while (
                !PhotonNetwork.IsConnectedAndReady ||
                networked_root == null)
            {
                yield return null;

                var player = PhotonNetwork.LocalPlayer;
                networked_root = PlayerUtils.GetUserRoot(player);
            }
        }

        /// <summary>
        /// This method spawns the networked graphics of a given hand.
        /// </summary>
        /// <param name="graphics">The graphics prefab located at the Resources folder.</param>
        /// <param name="hand">VR hand that is being initialized.</param>
        /// <returns>Spawned graphics object.</returns>
        protected GameObject InstantiateObject(string graphics, SimpleHandVR hand)
        {
            var player = PhotonNetwork.LocalPlayer;
            var rootname = PlayerUtils.RootName(player);
            var info = new InstantiationInfo()
            {
                position = Vector3.zero,
                rotation = Quaternion.identity
            };

            // Instantiate hand tool
            info.prefabs = new InstantiationPrefabs
            {
                networkPrefab = graphics
            };
            info.networkData = new NetworkInstantiationData()
            {
                parentName = rootname,
                gameObjectName = hand.name
            };

            var instantiatedObject = NetworkInstantiation.Instantiate(info);

            var script = instantiatedObject.AddComponent<GraphicsObject>();
            script.follow = hand.transform;

            return instantiatedObject;
        }
    }
}
