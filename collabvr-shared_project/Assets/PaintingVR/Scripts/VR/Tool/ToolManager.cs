﻿using Painter.VR.Core;
using System;
using System.Collections;
using UnityEngine;

namespace Painter.VR.Tool
{
    /// <summary>
    /// The tool manager uses a representation object and a behaviour object to control the tool that it represents.
    /// </summary>
    public class ToolManager : IToolManager
    {
        /// <summary>
        /// Object responsible for representing the tool.
        /// </summary>
        public ToolRepresentation representation;
        /// <summary>
        /// Object that defines this object's behaviour.
        /// </summary>
        public ToolBehaviour behaviour;

        /// <summary>
        /// Flag that is set when the left hand is using this tool.
        /// </summary>
        private bool leftEnabled = false;
        public bool LeftEnabled
        {
            get
            {
                return leftEnabled;
            }
        }

        /// <summary>
        /// Flag that is set when the right hand is using this tool.
        /// </summary>
        private bool rightEnabled = false;
        public bool RightEnabled
        {
            get
            {
                return rightEnabled;
            }
        }

        /// <summary>
        /// This function can be called by other scripts to switch tool.
        /// </summary>
        /// <param name="enable">Flag used to differentiate between enable and disable.</param>
        /// <param name="hand">VR hand that requires action.</param>
        private void EnableTool(bool enable, SimpleHandVR hand)
        {
            if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                leftEnabled = enable;
            }
            else if (hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                rightEnabled = enable;
            }

            representation.ShowTool(enable, hand);
            behaviour.EnableTool(enable, hand);
        }

        
        private static ToolManager m_leftTool;
        public static ToolManager LeftTool { get { return m_leftTool; } }

        private static ToolManager m_rightTool;
        public static ToolManager RightTool { get { return m_rightTool; } }

        public static ToolManager HandTool(SimpleHandVR hand)
        {
            if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                return m_leftTool;
            }
            else if (hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                return m_rightTool;
            }
            return null;
        }


        private void SetToolForHand(SimpleHandVR hand)
        {
            if(hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
            {
                m_leftTool = this;
            }
            else if(hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
            {
                m_rightTool = this;
            }
        }


        public override void EnableTool(SimpleHandVR hand, IToolManager previousTool = null)
        {
            EnableTool(true, hand);
            SetToolForHand(hand);
        }

        public override void DisableTool(SimpleHandVR hand, IToolManager newTool = null)
        {
            EnableTool(false, hand);
        }

        public override void BehaviourEnable(SimpleHandVR hand)
        {
            behaviour.EnableTool(true, hand);
        }

        public override void BehaviourDisable(SimpleHandVR hand)
        {
            behaviour.EnableTool(false, hand);
        }

        public override bool CanChange(SimpleHandVR hand)
        {
            return behaviour.CanChange(hand);
        }
    }
}
