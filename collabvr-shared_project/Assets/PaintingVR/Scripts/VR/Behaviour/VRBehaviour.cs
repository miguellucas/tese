﻿using Painter.VR.Core;
using UnityEngine;

namespace Painter.VR.Behaviour
{
    /// <summary>
    /// This class defines a behaviour for VR users.
    /// The developers should implement new classes that inherite from this one. 
    /// Through the abstract methods developers can implement the necessary logic for the behaviour to work as intended.
    /// </summary>
    public abstract class VRBehaviour : MonoBehaviour
    {
        /// <summary>
        /// This function can be called by other scripts to switch behaviour.
        /// </summary>
        /// <param name="enable">Flag used to differentiate between enable and disable.</param>
        /// <param name="hand">VR hand that requires action.</param>
        public void EnableBehaviour(bool enable, SimpleHandVR hand)
        {
            if (!enable)
                StopBehaviour(hand);
            else
                StartBehaviour(hand);
        }

        /// <summary>
        /// Query if this behaviour can be stopped at this moment.
        /// </summary>
        /// <param name="hand">VR hand that is being queried.</param>
        /// <returns>True if it is possible to change the behaviour right at this moment and false otherwise.</returns>
        public abstract bool CanChange(SimpleHandVR hand);

        /// <summary>
        /// Starts this behaviour.
        /// This function is customized through inheritance, as it is an abstract method.
        /// </summary>
        /// <param name="hand">VR hand that requires action.</param>
        protected abstract void StartBehaviour(SimpleHandVR hand);
        /// <summary>
        /// Stops this behaviour.
        /// This function is customized through inheritance, as it is an abstract method.
        /// </summary>
        /// <param name="hand">VR hand that requires action.</param>
        protected abstract void StopBehaviour(SimpleHandVR hand);
    }
}
