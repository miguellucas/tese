﻿using Painter.Utils;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Input
{
    // TODO: Comment this class

    public struct HandInteraction
    {
        public bool leftHand;
        public bool rightHand;
    };

    public class InputHandler : MonoBehaviour
    {
        #region Singleton
        private static InputHandler instance;
        public static InputHandler Instance
        {
            get
            {
                return instance;
            }
        }

        public InputHandler()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>InputHandler</b>: Multiple instances of InputHandler are not supported.");
                return;
            }
#endif

            instance = this;
            
            inputMap = new Dictionary<SteamVR_Action_Boolean, HandInteraction>();
        }
        #endregion
                
        private Dictionary<SteamVR_Action_Boolean, HandInteraction> inputMap;
        
        public HandInteraction GetInputState(SteamVR_Action_Boolean action)
        {
            if (inputMap.TryGetValue(action, out HandInteraction item))
            {
                return item;
            }
            return default;
        }

        public void NewAction(string name, SteamVR_Input_Sources inputSource, SteamVR_Action_Boolean.ChangeHandler changeHandler)
        {
            SteamVR_Action_Boolean action = SteamVR_Input.GetAction<SteamVR_Action_Boolean>(name);
#if DEBUG
            if(action == null)
            {
                Debug.LogError("<b>InputHandler</b>: action of name " + name + " was not found.");
                return;
            }
#endif

            // Id already exists no need to add new again
            if (!inputMap.ContainsKey(action))
            {
                inputMap.Add(action, new HandInteraction { leftHand = false, rightHand = false});

                // This listeners are added only once at the start
                action.AddOnChangeListener(ActionChange, SteamVR_Input_Sources.LeftHand);
                action.AddOnChangeListener(ActionChange, SteamVR_Input_Sources.RightHand);
            }

            // Add listeners to action
            action.AddOnChangeListener(changeHandler, inputSource);
        }

        public void RemoveAction(string name, SteamVR_Input_Sources inputSource, SteamVR_Action_Boolean.ChangeHandler changeHandler)
        {
            SteamVR_Action_Boolean action = SteamVR_Input.GetAction<SteamVR_Action_Boolean>(name);
#if DEBUG
            if (action == null)
            {
                Debug.LogError("<b>InputHandler</b>: action of name " + name + " was not found.");
                return;
            }
#endif

            // Remove listeners from action
            action.RemoveOnChangeListener(changeHandler, inputSource);
        }

        private void ActionChange(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (inputMap.TryGetValue(fromAction, out HandInteraction item))
            {
                if(fromSource == SteamVR_Input_Sources.LeftHand)
                {
                    item.leftHand = !item.leftHand;
                } 
                else if(fromSource == SteamVR_Input_Sources.RightHand)
                {
                    item.rightHand = !item.rightHand;
                }

                inputMap[fromAction] = new HandInteraction { leftHand = item.leftHand, rightHand = item.rightHand };
            }
        }
    }
}
