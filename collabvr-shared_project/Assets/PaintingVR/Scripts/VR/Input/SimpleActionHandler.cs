﻿using Painter.VR.Behaviour;
using Painter.VR.Core;
using Painter.VR.Locomotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Input
{
    // TODO: Comment this class
    public abstract class SimpleActionHandler : VRBehaviour
    {
        public SimpleHandVR m_left_hand;
        public SimpleHandVR m_right_hand;

        public bool m_left_action;
        public bool m_right_action;
        
        protected string actionName;

        protected HashSet<SteamVR_Input_Sources> m_sources = new HashSet<SteamVR_Input_Sources>();
        
        protected override void StartBehaviour(SimpleHandVR hand)
        {
            m_sources.Add(hand.handType);

            if(hand.handType == SteamVR_Input_Sources.LeftHand)
            {
                m_left_hand = hand;
            }
            else if(hand.handType == SteamVR_Input_Sources.RightHand)
            {
                m_right_hand = hand;
            }
            
            // Add action
            InputHandler.Instance.NewAction(actionName, hand.handType, ActionChangeHandler);
        }

        protected override void StopBehaviour(SimpleHandVR hand)
        {
            m_sources.Remove(hand.handType);

            // Remove action
            InputHandler.Instance.RemoveAction(actionName, hand.handType, ActionChangeHandler);
        }

        private void ActionChangeHandler(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (!m_sources.Contains(fromSource))
                return;

            HandInteraction handInteraction = InputHandler.Instance.GetInputState(fromAction);
            ActionStateChange(handInteraction, fromSource);
        }

        protected abstract void ActionStateChange(HandInteraction handInteraction, SteamVR_Input_Sources fromSource);
        protected abstract void OnActionPressed(SimpleHandVR origin);
        protected abstract void OnActionReleased(SimpleHandVR origin);
    }
}
