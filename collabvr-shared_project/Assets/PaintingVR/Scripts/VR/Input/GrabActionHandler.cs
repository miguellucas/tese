﻿using Painter.VR.Core;
using Painter.VR.Locomotion;
using System.Collections;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Input
{
    public abstract class GrabActionHandler : SimpleActionHandler
    {
        public GrabActionHandler()
        {
            actionName = "GrabGrip";
        }

        protected override void ActionStateChange(HandInteraction handInteraction, SteamVR_Input_Sources fromSource)
        {
            if (m_left_hand && m_left_action && !handInteraction.leftHand && fromSource == SteamVR_Input_Sources.LeftHand)
            {
                OnActionReleased(m_left_hand);
                m_left_action = false;
                return;
            }

            if (m_right_hand && m_right_action && !handInteraction.rightHand && fromSource == SteamVR_Input_Sources.RightHand)
            {
                OnActionReleased(m_right_hand);
                m_right_action = false;
                return;
            }

            if (m_right_hand && handInteraction.rightHand && fromSource == SteamVR_Input_Sources.RightHand)
            {
                m_right_action = true;
                OnActionPressed(m_right_hand);
            }
            else if (m_left_hand && handInteraction.leftHand && fromSource == SteamVR_Input_Sources.LeftHand)
            {
                m_left_action = true;
                OnActionPressed(m_left_hand);
            }
        }
    }
}
