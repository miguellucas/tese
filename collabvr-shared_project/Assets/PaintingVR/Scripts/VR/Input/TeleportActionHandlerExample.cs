﻿using Painter.Communication;
using Painter.Utils;
using Painter.VR.Core;
using Painter.VR.Locomotion;
using System.Collections;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Input
{
    public class TeleportActionHandlerExample : TeleportActionHandler
    {
        #region Singleton

        private static TeleportActionHandler m_instance;
        public static TeleportActionHandler Instance
        {
            get
            {
                return m_instance;
            }
        }

        public TeleportActionHandlerExample()
        {
#if DEBUG
            if (m_instance != null)
            {
                Debug.LogError("<b>TeleportActionHandlerExample</b>: Multiple instances of TeleportActionHandlerExample are not supported.");
                return;
            }
#endif
            m_instance = this;
        }

        #endregion

        #region Teleport action

        public Validator[] validators;
        public CustomCaster caster;

        #endregion

        #region Action flags

        private bool m_is_pressed = false;
        private SimpleHandVR m_origin;
        private Validator m_attached = null;

        #endregion

        protected override void StopBehaviour(SimpleHandVR hand)
        {
            base.StopBehaviour(hand);

            if(hand == m_origin)
                ShowAction(false);
        }

        private void Update()
        {
            if (!m_is_pressed)
                return;

            AttachToSelected();
        }

        private void AttachToSelected()
        {
            if (caster.Cast(m_origin.transform, out RaycastHit hit))
            {
                foreach (var validator in validators)
                {
                    if(validator.Validate(hit))
                    {
                        if(m_attached != null && validator != m_attached)
                        {
                            m_attached.Show(true, false);
                        }

                        // Attach here
                        caster.AttachTo(hit.collider.gameObject.GetComponent<Validator>().GetAttachmentPoint());
                        validator.Show(true, true);

                        m_attached = validator;
                        return;
                    }
                }

                if(m_attached != null)
                {
                    caster.AttachTo(default);
                    m_attached.Show(true, false);
                    m_attached = null;
                }
            }
        }

        protected override void OnActionPressed(SimpleHandVR origin)
        {
            m_origin = origin;
            ShowAction(true, origin.transform);
        }

        protected override void OnActionReleased(SimpleHandVR origin)
        {
            m_origin = null;
            ShowAction(false);

            // Check for the first hit on this caster
            if (caster.Cast(origin.transform, out RaycastHit hit))
            {
                foreach (var validator in validators)
                {
                    if (validator.Validate(hit))
                    {
                        validator.ExecuteAction();
                        return;
                    }
                }
            }
        }

        private void ShowAction(bool show, Transform origin = null)
        {
            m_is_pressed = show;
            caster.ShowCast(show, origin);
            foreach (var validator in validators)
            {
                validator.Show(show);
            }
        }

        public override bool CanChange(SimpleHandVR hand)
        {
            // If the current origin belongs to this hand 
            // change is not possible
            return m_origin != hand;
        }
    }
}
