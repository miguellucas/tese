﻿using Painter.VR.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Locomotion
{
    /// <summary>
    /// Teleport point is a teleport pad in which teleports the player to the center of the object.
    /// </summary>
    public class TeleportArea : TeleportPad
    {
        [Header("Graphics")]
        [Tooltip("The indicator is a small object that is placed on the previously recorded hit location.")]
        public GameObject m_indicator;

        [Space]

        [Header("Audio")]
        public AudioSource audioSource;
        public AudioClip whileValid;
        public AudioClip onTeleport;
        private IEnumerator m_audioCoroutine;
        
        private enum TeleportAreaState { DISABLED, VALID };
        private TeleportAreaState m_state;

        private Vector3 m_hitLocation;


        private void Start()
        {
            OnDisabledPad();
        }

        public override void ExecuteAction()
        {
            Teleporter.Instance.Teleport(
                new TeleportInformation { pad = this, point = m_hitLocation, yrotation = 0.0f, valid = true },
                PlayerVR.Instance.transform);

            StopCoroutine(m_audioCoroutine);
            audioSource.clip = onTeleport;
            audioSource.Play();
        }

        public override void Show(bool show, bool isAttached = false)
        {
            if(show && isAttached && m_state != TeleportAreaState.VALID)
            {
                m_state = TeleportAreaState.VALID;
                OnValidPad();
            }
            else if((!show || !isAttached) && m_state != TeleportAreaState.DISABLED)
            {
                m_state = TeleportAreaState.DISABLED;
                OnDisabledPad();
            }
        }

        public override void Teleported(Transform root, bool here)
        {}

        public override bool Validate(RaycastHit hit)
        {
            m_hitLocation = hit.point;
            RelocateIndicator(m_hitLocation);
            return true;
        }

        private void RelocateIndicator(Vector3 location)
        {
            location.y = 0;
            m_indicator.transform.position = location;
            audioSource.transform.position = location;
        }

        public override Vector3 GetAttachmentPoint()
        {
            return m_hitLocation;
        }


        private void OnDisabledPad()
        {
            m_indicator.SetActive(false);
            // audioSource.Stop();
        }

        private void OnValidPad()
        {
            m_indicator.SetActive(true);

            if(m_audioCoroutine == null)
            {
                m_audioCoroutine = AudioSourceState();
                StartCoroutine(AudioSourceState());
            }
        }

        private IEnumerator AudioSourceState()
        {
            audioSource.clip = whileValid;
            audioSource.PlayDelayed(0.25f);

            float timeAudioStopped = 0.0f;
            while(true)
            {
                yield return null;

                if(m_state == TeleportAreaState.DISABLED)
                {
                    // Turn volume down or wait for audioSource to be over
                    // audioSource.volume -= 0.001f;
                    if(!audioSource.isPlaying)
                    {
                        break;
                    }
                }
                else if (m_state == TeleportAreaState.VALID)
                {
                    if(!audioSource.isPlaying)
                    {
                        timeAudioStopped += Time.deltaTime;

                        if(timeAudioStopped >= 0.5f)
                        {
                            audioSource.clip = whileValid;
                            audioSource.Play();
                        }
                    }
                    else
                    {
                        timeAudioStopped = 0.0f;
                    }
                }
            }

            m_audioCoroutine = null;
        }
    }
}
