﻿using Painter.VR.Input;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Locomotion
{
    /// <summary>
    /// Teleport information that can be later used to move the player to a new position.
    /// </summary>
    public struct TeleportInformation
    {
        public bool valid;
        public Vector3 point;
        public float yrotation;
        public TeleportPad pad;
        
        public TeleportInformation(bool valid)
        {
            this.valid = valid;
            point = Vector3.zero;
            yrotation = 0.0f;
            this.pad = null;
        }

        public TeleportInformation(Vector3 point, float yrotation)
        {
            this.valid = true;
            this.point = point;
            this.yrotation = yrotation;
            this.pad = null;
        }

        public TeleportInformation(Vector3 point, float yrotation, TeleportPad pad)
        {
            this.valid = true;
            this.point = point;
            this.yrotation = yrotation;
            this.pad = pad;
        }
    };

    /// <summary>
    /// Teleport pads are the areas that allow for teleportation.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public abstract class TeleportPad : Validator
    {
        /// <summary>
        /// Flag that is set if the user should spawn at this location
        /// </summary>
        public bool spawnHere;

        /// <summary>
        /// Given a wanted teleport information, this function checks if it's valid and returns different information if needed.
        /// </summary>
        /// <param name="root">The wanted point from this teleport pad.</param>
        /// <param name="wantedPoint">The wanted point from this teleport pad.</param>
        /// <returns>A valid teleport information according to this pad.</returns>
        //public abstract TeleportInformation TeleportHere(Transform root, TeleportInformation wantedPoint);
        /// <summary>
        /// Shows a teleport pad.
        /// </summary>
        /// <param name="show">Flag that is set if the pad is to be rendered.</param>
        //public abstract void ShowPad(bool show);
        /// <summary>
        /// Function can be used to know if the user is at this teleport location.
        /// </summary>
        /// <param name="here">Flag that is set if the player teleported here.</param>
        public abstract void Teleported(Transform root, bool here);
    }
}
