﻿using UnityEngine;
using Painter.Utils;

namespace Painter.VR.Locomotion
{
    /// <summary>
    /// Main teleportation handler. 
    /// It gathers all the teleportation methods in a class all calls them to achieve this locomotion method.
    /// </summary>
    public class Teleporter : MonoBehaviour
    {
        #region Singleton
        private static Teleporter instance;
        public static Teleporter Instance
        {
            get
            {
                return instance;
            }
        }

        public Teleporter()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>Teleporter</b>: Multiple instances of Teleporter are not supported.");
                return;
            }
#endif

            instance = this;
        }
        #endregion
                
        private TeleportPad m_currentPad;
        
        public bool Teleport(TeleportInformation information, Transform teleportRoot)
        {
            if(information.valid)
            {
                var cameraPosition = new Vector3(Camera.main.transform.position.x, 0.0f, Camera.main.transform.position.z);
                var rootPosition = new Vector3(teleportRoot.transform.position.x, 0.0f, teleportRoot.transform.position.z);
                teleportRoot.transform.position = information.point - (cameraPosition - rootPosition);

                var eulerAngles = teleportRoot.transform.rotation.eulerAngles;
                eulerAngles.y = information.yrotation;
                teleportRoot.transform.rotation = Quaternion.Euler(eulerAngles);

                // Todo: Do something later
                //----------------------------------------------------
                // This whole thing sounds bad :3
                information.pad.Teleported(teleportRoot, true);

                if(m_currentPad != null)
                {
                    m_currentPad.Teleported(teleportRoot, false);
                }
                m_currentPad = information.pad;
                //----------------------------------------------------

                return true;
            }

            return false;
        }
    }
}
