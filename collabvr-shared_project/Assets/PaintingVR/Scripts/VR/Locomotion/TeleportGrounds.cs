﻿using Painter.VR.Input;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Locomotion
{
    /// <summary>
    /// Gathers all teleport pads in one data structure that is optimized for lookpus.
    /// </summary>
    public class TeleportGrounds : Validator
    {
        /// <summary>
        /// Teleport pads in the scene.
        /// </summary>
        private HashSet<TeleportPad> m_pads;

        /// <summary>
        /// Spawn point.
        /// </summary>
        private TeleportPad m_point;

        private TeleportPad m_validPad;


        private void Start()
        {
            m_pads = new HashSet<TeleportPad>();
            UpdateTeleportPads();
        }


        public override bool Validate(RaycastHit hit)
        {            
            // Try match all raycast hits to a teleport pad
            var pad = hit.collider.GetComponent<TeleportPad>();
            if (pad != null)
            {
                // HashSet has a lookup of O(1)
                if (m_pads.Contains(pad))
                {
                    var valid = pad.Validate(hit);
                    if (valid) {
                        m_validPad = pad;
                        return true;
                    }
                }
            }

            m_validPad = null;
            return false;
        }

        public override void ExecuteAction()
        {
            if(m_validPad != null)
            {
                m_validPad.ExecuteAction();
                m_validPad = null;
            }
        }

        public override void Show(bool show, bool isAttached = false)
        {
            foreach (TeleportPad pad in m_pads)
            {
                if (m_validPad != null && pad == m_validPad)
                    pad.Show(show, true);
                else
                    pad.Show(show, false);
            }
        }

        public override Vector3 GetAttachmentPoint()
        {
            return m_validPad.GetAttachmentPoint();
        }


        /// <summary>
        /// This function should only be used if strickly necessary.
        /// </summary>
        public void UpdateTeleportPads()
        {
            m_pads.Clear();

            var teleportpads = FindObjectsOfType<TeleportPad>();
            foreach (var pad in teleportpads)
            {
                m_pads.Add(pad);
            }

            if (m_validPad != null
                && !m_pads.Contains(m_validPad))
                m_validPad = null;
        }
    }
}
