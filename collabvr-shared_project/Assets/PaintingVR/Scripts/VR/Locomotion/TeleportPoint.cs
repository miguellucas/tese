﻿using Painter.VR.Core;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.VR.Locomotion
{
    /// <summary>
    /// Teleport point is a teleport pad in which teleports the player to the center of the object.
    /// </summary>
    public class TeleportPoint : TeleportPad
    {
        /// <summary>
        /// Graphics for the teleport point that will be enabled and disabled during runtime.
        /// </summary>
        public MeshRenderer[] graphics;
        public Color isValidColor;
        public Color defaultColor;
        
        [Space]

        public AudioSource audioSource;

        private HashSet<Transform> objectsAtThisPoint;
        
        private enum TeleportPointState { ACTIVE_VALID, ACTIVE_INVALID, INACTIVE };
        private TeleportPointState m_state;

        private void Start()
        {
            objectsAtThisPoint = new HashSet<Transform>();
            m_state = TeleportPointState.INACTIVE;

            OnDiactivate();
        }


        public override void Teleported(Transform root, bool here)
        {
            var contains = objectsAtThisPoint.Contains(root);
            if (here)
            {
#if DEBUG
                if(contains)
                {
                    Debug.LogError("<b>TeleportPoint</b> teleported object was already at this location.");
                    return;
                }
#endif
                objectsAtThisPoint.Add(root);
            }
            else
            {
#if DEBUG
                if (!contains)
                {
                    Debug.LogError("<b>TeleportPoint</b> removing object that is not at this location.");
                    return;
                }
#endif

                objectsAtThisPoint.Remove(root);
            }
        }

        public override bool Validate(RaycastHit hit)
        {
            return !objectsAtThisPoint.Contains(PlayerVR.Instance.transform);
        }

        public override void ExecuteAction()
        {
            Teleporter.Instance.Teleport(
                new TeleportInformation { pad = this, point = transform.position, yrotation = 0.0f, valid = true },
                PlayerVR.Instance.transform);
        }

        public override void Show(bool show, bool isAttached = false)
        {
            if (objectsAtThisPoint.Count != 0)
                return;
            
            if (show && isAttached)
            {
                if(m_state != TeleportPointState.ACTIVE_VALID)
                    OnActivateValid();
            }
            else if(show && !isAttached)
            {
                if (m_state != TeleportPointState.ACTIVE_INVALID)
                    OnActivateInvalid();
            }
            else if(!show)
            {
                if (m_state != TeleportPointState.INACTIVE)
                    OnDiactivate();
            }
        }

        public override Vector3 GetAttachmentPoint()
        {
            return GetComponent<Collider>().bounds.center;
        }

        private void OnDiactivate()
        {
            m_state = TeleportPointState.INACTIVE;

            foreach (var obj in graphics)
            {
                obj.gameObject.SetActive(false);
            }

            //  Stop the audio if playing
            if (audioSource.isPlaying)
                audioSource.Stop();
        }

        private void OnActivateValid()
        {
            m_state = TeleportPointState.ACTIVE_VALID;

            // Set valid color and enable graphics objects
            foreach (var obj in graphics)
            {
                obj.gameObject.SetActive(true);
                obj.material.color = isValidColor;
            }

            // Play audio
            audioSource.Play();
        }

        private void OnActivateInvalid()
        {
            m_state = TeleportPointState.ACTIVE_INVALID;

            // Set default color and enable graphics objects
            foreach (var obj in graphics)
            {
                obj.gameObject.SetActive(true);
                obj.material.color = defaultColor;
            }

            //  Stop the audio if playing
            if (audioSource.isPlaying)
                audioSource.Stop();
        }

    }
}
