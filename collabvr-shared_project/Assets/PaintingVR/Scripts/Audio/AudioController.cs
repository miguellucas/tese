﻿using System;
using System.Collections;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource source;

    [Serializable]
    public struct AudioSettings
    {
        // 0 - stop imediatly; 1 - lower the volume until it's zero
        public bool volumeDownOnStop;
        // 0 - continue playing normally; 1 - restart
        public bool restartOnPlay;
        // 0 - stops after it's finished; 1 - loops
        public bool loopEffect;
        // Target volume
        [Range(0.0f, 1.0f)]
        public float volume;
        // Volume rise speed
        public float volumeUpSpeed;
        // Volume descend speed
        public float volumeDownSpeed;
        // Delay on start
        [Range(0.0f, 10.0f)]
        public float delayOnPlay;
        // Delay on replay (only valid if looping)
        [Range(0.0f, 10.0f)]
        public float delayOnReplay;
    }
    public AudioSettings audioSettings;

    private IEnumerator m_controllerCoroutine = null;
    private enum AudioState { IDLE, STARTING, PLAYING, LOOPING, STOPPING };
    private AudioState state = AudioState.IDLE;

    public virtual void PlayAudio()
    {
        if(audioSettings.restartOnPlay && m_controllerCoroutine != null)
        {
            StopCoroutine(m_controllerCoroutine);
            state = AudioState.IDLE;
        }

        // Reset state 
        if(state == AudioState.STOPPING)
        {
            state = AudioState.STARTING;
        }
        // Start a new controller
        else if(state == AudioState.IDLE)
        {
            CleanStart();
        }
    }

    public virtual void StopAudio()
    {
        // Nothing to do
        if (state == AudioState.IDLE)
            return;

        // Stop the coroutine at any of these state as it means the audio is not playing
        if(state == AudioState.STARTING || state == AudioState.LOOPING)
        {
            StopCoroutine(m_controllerCoroutine);
            state = AudioState.IDLE;
        }
        // Set state for the controller to take the appropriate action
        else
        {
            state = AudioState.STOPPING;
        }
    }

    private void CleanStart()
    {
        source.volume = audioSettings.volume;
        source.loop = false;

        m_controllerCoroutine = Controller();
        StartCoroutine(m_controllerCoroutine);
    }

    private IEnumerator Controller()
    {
        // Wait for a given delay
        if(audioSettings.delayOnPlay > 0.0f)
        {
            state = AudioState.STARTING;
            yield return new WaitForSeconds(audioSettings.delayOnPlay);
        }

        // Start playing audio source
        state = AudioState.PLAYING;
        source.Play();

        yield return null;

        // Does not stop until state is IDLE
        while (state != AudioState.IDLE)
        {
            if(state == AudioState.PLAYING)
            {
                PlayingController();
            }
            else if(state == AudioState.STOPPING)
            {
                StoppingController();
            }
            else if(state == AudioState.LOOPING)
            {
                if(audioSettings.delayOnReplay > 0.0f)
                {
                    yield return new WaitForSeconds(audioSettings.delayOnReplay);
                }
                
                // Start playing audio source
                state = AudioState.PLAYING;
                source.Play();
            }
            else if (state == AudioState.STARTING)
            {
                if (audioSettings.delayOnPlay > 0.0f)
                {
                    yield return new WaitForSeconds(audioSettings.delayOnPlay);
                }

                // Start playing audio source
                state = AudioState.PLAYING;
                source.Play();
            }

            yield return null;
        }
    }

    private void PlayingController()
    {
        if(audioSettings.volume != source.volume)
        {
            source.volume += audioSettings.volumeUpSpeed * Time.deltaTime;
            if(source.volume > audioSettings.volume)
            {
                source.volume = audioSettings.volume;
            }
        }

        if(!source.isPlaying)
        {
            if(audioSettings.loopEffect)
            {
                state = AudioState.LOOPING;
            }
            else
            {
                state = AudioState.STOPPING;
            }
        }
    }

    private void StoppingController()
    {
        if(source.isPlaying && audioSettings.volumeDownOnStop)
        {
            source.volume -= audioSettings.volumeDownSpeed * Time.deltaTime;
            if (source.volume < 0.0f)
            {
                source.volume = 0.0f;
            }
        }

        if(!source.isPlaying || source.volume == 0.0f || !audioSettings.volumeDownOnStop)
        {
            source.Stop();
            state = AudioState.IDLE;
        }
    }
}
