﻿using UnityEngine;
using Painter.Core;
using Painter.Network.Utils;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Events;

namespace Painter.Network.Selectable
{
    [System.Serializable]
    public struct GrabInformation
    {
        public Selectable selected;
        public Transform pivotAttachment;
        public Transform otherAttachment;
        public bool isGrab;

        public GrabInformation(Selectable selected, Transform pivotAttachment, Transform otherAttachment, bool isGrab)
        {
            this.selected = selected;
            this.pivotAttachment = pivotAttachment;
            this.otherAttachment = otherAttachment;
            this.isGrab = isGrab;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    [RequireComponent(typeof(OwnershipTransfer))]
    public class Selectable : Grabable
    {
        /// <summary>
        /// Flag that is set when the player is in a room.
        /// </summary>
        private bool inRoom = false;
        /// <summary>
        /// Ownership transfer is a required component for this script to work.
        /// </summary>
        private OwnershipTransfer ownershipTransfer;
        /// <summary>
        /// This objects photon view. This is a required component.
        /// </summary>
        private PhotonView photonView;

        #region Enabled
        /// <summary>
        /// Flag that is set when the selectable object is enabled.
        /// </summary>
        public bool IsEnabled { get; set; } = true;

        public void SetEnabledToAllClients(bool enabled)
        {
            photonView.RPC("SetEnabled", RpcTarget.All, new object[] { enabled });
        }

        [PunRPC]
        private void SetEnabled(bool enabled)
        {
            IsEnabled = enabled;
        }
        #endregion

        #region Pivot point and multiple grabs
        
        /// <summary>
        /// The intended pivot point before the ownership is transfered.
        /// </summary>
        private Transform firstGrab;
        /// <summary>
        /// The second attachment point. This second point can then be used to scale the object or switch pivot points.
        /// </summary>
        private Transform secondGrab;

        private float lastDistance;

        public bool Selected
        {
            get
            {
                return firstGrab != null;
            }
        }

        public bool MultipleGrabs
        {
            get
            {
                return firstGrab != null && secondGrab != null;
            }
        }
        
        #endregion

        /// <summary>
        /// Called when the connection status of player to a room changes.
        /// </summary>
        /// <param name="joined">Flag that is set when the player joins a room.</param>
        protected virtual void OnRoomUpdate(bool joined)
        {
            inRoom = joined;
        }

        /// <summary>
        /// Send the event to every other client
        /// </summary>
        /// <param name="isGrab"></param>
        private void SendOwnershipRequest(bool isGrab)
        {
            ownershipTransfer.AskOwnershipTransfer(PhotonNetwork.LocalPlayer, isGrab, OnOwnershipTransfered);
        }

        /// <summary>
        /// This function is used to start the process of requesting ownership 
        /// </summary>
        /// <param name="attachment"></param>
        public void RegisterGrabIntention(Transform attachment)
        {
            if (!inRoom || !IsEnabled)
            {
                return;
            }

            if(firstGrab == null)
            {
                firstGrab = attachment;

                // Grab the object if the view is already mine
                if (photonView.IsMine)
                {
                    OnGrab(firstGrab);
                }
            }
            else if(secondGrab == null)
            {
                if (attachment == firstGrab)
                    return;

                secondGrab = attachment;
                lastDistance = Vector3.Distance(firstGrab.position, secondGrab.position);
            }
            else
            {
#if DEBUG
                // This situation should never happen
                // Unless there are more than 2 grabbing objects
                if (attachment != secondGrab && attachment != firstGrab)
                    Debug.LogError("<b>Selectable</b> grabbing a selectable object from a third attachment point.");
#endif
                return;
            }

            SendOwnershipRequest(true);
        }

        /// <summary>
        /// This function is used to stop the intention of owning the object
        /// </summary>
        public void RegisterDropIntention(Transform attachment)
        {
            if (!inRoom || !IsEnabled)
            {
                return;
            }

            if (attachment != firstGrab)
            {
#if DEBUG
                if(attachment != secondGrab)
                {
                    // This situation should never happen
                    // Unless there are more than 2 grabbing objects
                    Debug.LogError("<b>Selectable</b> dropping a selectable object from an unregistered attachment point.");
                    return;
                }
#endif

                secondGrab = null;
            }
            else
            {
                if(secondGrab == null)
                {
                    // If there are no more attachment point left
                    // and the view is mine, drop the object
                    firstGrab = null;
                    // if(photonView.IsMine)
                    OnDrop();
                }
                else
                {
                    // Shift pivot points
                    ShiftAttachmentPoint(secondGrab);
                    firstGrab = secondGrab;
                    secondGrab = null;
                }
            }

            SendOwnershipRequest(false);
        }

        private void OnOwnershipTransfered()
        {
            if (firstGrab == null)
                ownershipTransfer.OwnershipCheck();
            else
                OnGrab(firstGrab);
        }



        [System.Serializable]
        public class GrabEvent : UnityEvent<GrabInformation> {}

        public GrabEvent grabEvent;

        public override void OnDrop(bool isVR=true)
        {
            base.OnDrop(isVR);

            grabEvent.Invoke(new GrabInformation(this, null, null, false));
        }

        protected override void OnGrab(Transform attachment)
        {
            base.OnGrab(attachment);

            grabEvent.Invoke(new GrabInformation(this, firstGrab, secondGrab, true));
        }

        protected override void OnUpdate()
        {
            if(secondGrab != null && AttachmentPoint != null)
            {
                float cdistance = Vector3.Distance(AttachmentPoint.position, secondGrab.position);
                Scale(cdistance / lastDistance);
                lastDistance = cdistance;
            }

            base.OnUpdate();
        }



        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            ownershipTransfer = GetComponent<OwnershipTransfer>();
        }

        protected void Start()
        {
            if(grabEvent == null)
                grabEvent = new GrabEvent();

            if (PhotonNetwork.IsConnected)
            {
                inRoom = true;
            }

            // Subscribe to connection status events
            ConnectionStatus mStatus = ConnectionStatus.Instance;
            if (mStatus != null)
            {
                mStatus.OnRoomUpdate.AddListener(OnRoomUpdate);
            }
        }

        private void LateUpdate()
        {
            OnUpdate();
        }

    }
}
