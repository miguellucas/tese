﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Network.Selectable
{
    public interface ISelectable
    {
        void OnGrab();
        void OnDrop();
        void OnUpdate();
    }
}
