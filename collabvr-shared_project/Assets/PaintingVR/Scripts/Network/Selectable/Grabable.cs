﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Network.Selectable
{
    /// <summary>
    /// Credit goes to AdenFlorian: https://answers.unity.com/questions/518399/simulate-child-parent-relationship.html
    /// 
    /// </summary>
    public abstract class Grabable : MonoBehaviourPun, IPunObservable
    {
        /// <summary>
        /// Attachment object to which this object is attached to.
        /// </summary>
        private Transform attachmentPoint;
        /// <summary>
        /// Getter method for the attachment point.
        /// </summary>
        public Transform AttachmentPoint
        {
            get
            {
                return attachmentPoint;
            }
        }

        private Vector3 startParentPosition;
        private Quaternion startParentRotationQ;

        private Vector3 startChildPosition;
        private Vector3 initialChildPosition;
        private Quaternion startChildRotationQ;

        private Matrix4x4 parentMatrix;

        private bool isTrigger = false;

        //------------------------------------------------------------
        //
        /// <summary>
        /// Setter for the attachment point.
        /// It handle the shift in position and rotation.
        /// </summary>
        /// <param name="nattachment"></param>
        public void ShiftAttachmentPoint(Transform nattachment)
        {
            attachmentPoint = nattachment;

#if DEBUG
            if(attachmentPoint == null)
            {
                Debug.LogError("<b>Grabable</b>: Attachment point is set to null.");
                Debug.LogError(StackTraceUtility.ExtractStackTrace());
            }
#endif

            startParentPosition = attachmentPoint.position;
            startParentRotationQ = attachmentPoint.rotation;

            initialChildPosition = transform.position;
            startChildPosition = transform.position;
            startChildRotationQ = transform.rotation;

            // Keeps child position from being modified at the start by the parent's initial transform
            startChildPosition =
                 DivideVectors(Quaternion.Inverse(attachmentPoint.rotation) * (startChildPosition - startParentPosition), Vector3.one);
        }

        //-----------------------------------------------------------
        //
        [Range(0.25f, 5.0f)]
        public float scaleFactor = 1.0f;

        /// <summary>
        /// Called when this object is to be dropped.
        /// </summary>
        /// <param name="isVR">Whether called by a VR user or not.</param>
        public virtual void OnDrop(bool isVR = true)
        {
            if (attachmentPoint == null && isVR)
                return;

            // photonView.RPC("SetAllAsTriggers", RpcTarget.All, false);
            isTrigger = false;
            SetAllAsTriggers(isTrigger);
            attachmentPoint = null;
        }

        /// <summary>
        /// Called when this object is to be attached to another object.
        /// </summary>
        /// <param name="attachment">The Transform of the attachment object.</param>
        protected virtual void OnGrab(Transform attachment)
        {
            // photonView.RPC("SetAllAsTriggers", RpcTarget.All, true);

            // SetAllAsTriggers(true);
            isTrigger = true;
            SetAllAsTriggers(isTrigger);
            ShiftAttachmentPoint(attachment);
        }

        /// <summary>
        /// Called when this object is grabbed by an AR user.
        /// </summary>
        public virtual void OnGrabAR()
        {
            isTrigger = true;
            SetAllAsTriggers(isTrigger);
        }

        /// <summary>
        /// Called to update the position of this object.
        /// </summary>
        protected virtual void OnUpdate()
        {
            if (attachmentPoint == null)
                return;

            if (Input.GetKeyDown(KeyCode.Space))
                Scale(scaleFactor);

            parentMatrix = Matrix4x4.TRS(attachmentPoint.position, attachmentPoint.rotation, attachmentPoint.lossyScale);
            transform.position = parentMatrix.MultiplyPoint3x4(startChildPosition);
            transform.rotation = (attachmentPoint.rotation * Quaternion.Inverse(startParentRotationQ)) * startChildRotationQ;
        }

        /// <summary>
        /// Scales the object around the pivot point by a given factor.
        /// </summary>
        protected virtual void Scale(float factor)
        {
            startParentPosition = attachmentPoint.position;
            startParentRotationQ = attachmentPoint.rotation;

            initialChildPosition = transform.position;
            Vector3 positionDiff = initialChildPosition - startParentPosition;
            positionDiff *= factor;
            initialChildPosition = startParentPosition + positionDiff;

            startChildPosition = initialChildPosition;
            startChildRotationQ = transform.rotation;

            transform.localScale *= factor;

            // Keeps child position from being modified at the start by the parent's initial transform
            startChildPosition =
                 DivideVectors(Quaternion.Inverse(startParentRotationQ) * (startChildPosition - startParentPosition), Vector3.one);
        }
        
        private Vector3 DivideVectors(Vector3 num, Vector3 den)
        {
            return new Vector3(num.x / den.x, num.y / den.y, num.z / den.z);
        }

        //-----------------------------------------------------------
        //
        private List<Collider> m_colliders = new List<Collider>();
        public Collider[] Colliders
        {
            get
            {
                m_colliders.RemoveAll(item => item == null);
                return m_colliders.ToArray();
            }
        }

        public Vector4[] ClosestPoints
        {
            get
            {
                Vector4[] closestPoints = new Vector4[m_colliders.Count];
                int i = 0;
                foreach(var collider in m_colliders)
                {
                    if(collider != null)
                        // This does an okay job :(
                        closestPoints[i] = collider.bounds.ClosestPoint(transform.position);
                    i++;
                }
                return closestPoints;
            }
        }

        /// <summary>
        /// Sets all the colliders as triggers. 
        /// </summary>
        /// <param name="isTrigger">Flag that is set to true if the colliders are to be triggers.</param>
        [PunRPC]
        private void SetAllAsTriggers(bool isTrigger)
        {
            var colliders = GetComponents<Collider>();
            foreach (var collider in colliders)
            {
                if(collider != null)
                    collider.isTrigger = isTrigger;
            }

            if (isTrigger)
            {
                m_colliders.Clear();
            }
        }

        private void OnTriggerEnter(Collider collider)
        {
            m_colliders.Add(collider);
        }

        private void OnTriggerExit(Collider collider)
        {
            m_colliders.Remove(collider);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if(stream.IsWriting)
            {
                stream.SendNext(isTrigger);
            }
            else if(stream.IsReading)
            {
                var objTrigger = (bool)stream.ReceiveNext();
                if(objTrigger != isTrigger)
                {
                    isTrigger = objTrigger;
                    SetAllAsTriggers(isTrigger);
                }
            }
        }
    }
}
