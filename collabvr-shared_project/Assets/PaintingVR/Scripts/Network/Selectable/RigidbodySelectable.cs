﻿using Painter.Network.Selectable;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidbodySelectable : MonoBehaviour, IPunObservable
{
    public bool onGrabTrigger = false;
    public bool estimateVelocityOnDetach = false;

    public float velocityThreshold = 0.1f;
    public float angularVelocityThreshold = 0.1f;

    private Rigidbody m_rigidbody;
    private Collider m_collider;

    private bool isGrabbed = false;

    private Vector3 m_deltaPosition = Vector3.zero;
    private Vector3 m_lastPosition;

    public struct RigidbodyState
    {
        public Vector3 velocity;
    }

    private Queue<RigidbodyState> last_states = new Queue<RigidbodyState>();

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();

        m_deltaPosition = Vector3.zero;
        m_lastPosition = transform.position;

        last_states.Enqueue(new RigidbodyState{ velocity = m_lastPosition });
    }

    public void Update()
    {
        if (!isGrabbed)
            return;
        
        m_deltaPosition = (transform.position - m_lastPosition) / Time.deltaTime;
        m_lastPosition = transform.position;

        if (last_states.Count >= 4)
            last_states.Dequeue();
        last_states.Enqueue(new RigidbodyState { velocity = m_deltaPosition });
    }

    public void OnGrab(GrabInformation grabInfo)
    {
        if(grabInfo.isGrab)
        {
            isGrabbed = true;

            m_rigidbody.isKinematic = true;
            m_collider.isTrigger = onGrabTrigger;

            last_states.Clear();
        }
        else
        {
            isGrabbed = false;

            m_rigidbody.isKinematic = false;
            m_collider.isTrigger = false;

            if(estimateVelocityOnDetach)
            {
                DetachVelocity(out RigidbodyState state);

                m_rigidbody.velocity = state.velocity;
                m_rigidbody.angularVelocity = Vector3.zero;
            }
            else
            {
                m_rigidbody.velocity = Vector3.zero;
                m_rigidbody.angularVelocity = Vector3.zero;
            }
        }
    }

    protected virtual void DetachVelocity(out RigidbodyState state)
    {
        Vector3 velocity = Vector3.zero;

        state = new RigidbodyState { velocity = Vector3.zero };

        float divide_delta = last_states.Count;

        while(last_states.Count > 0)
        {
            var elem_state = last_states.Dequeue();
            velocity += elem_state.velocity;
        }

        velocity /= divide_delta;

        if(velocity.magnitude > velocityThreshold)
        {
            state.velocity = velocity;
        }
        else
        {
            state.velocity = Vector3.zero;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsReading)
        {
            bool isKinematic = (bool)stream.ReceiveNext();
            Vector3 velocity = (Vector3)stream.ReceiveNext();
            Vector3 position = (Vector3)stream.ReceiveNext();

            m_rigidbody.isKinematic = isKinematic;
            m_rigidbody.velocity = velocity;
            transform.localPosition = position;
        }
        else if(stream.IsWriting)
        {
            stream.SendNext(m_rigidbody.isKinematic);
            stream.SendNext(m_rigidbody.velocity);
            stream.SendNext(transform.localPosition);
        }
    }
}
