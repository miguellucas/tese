﻿using UnityEngine;
using Painter.Communication;

namespace Painter.Utils
{
    public class BlinkController : MonoBehaviour
    {
        public Color closeColor = Color.green;
        public Color intermediateColor = Color.yellow;
        public Color farColor = Color.red;

        [Range(0,1)]
        public float blinkAlpha = 1;

        public float timeBlink = 0.5f;
        public int nBlinks = 4;

        public Hologram hologram;

        private float currentTime = 0;
        private int currentBlinks = 0;
        private bool blinking = false;

        private Transform cylinder = null;
        private Transform cone = null;

        private Material cylinderMaterial;
        private Material coneMaterial;
        private Material hologramMaterial;

        private Color materialColor;
        private Color iniColor;
        private Color targetColor;

        private float materialAlpha;
        private float iniAlpha;
        private float targetAlpha;

        private NetworkVectorController networkVectorController;

        public NetworkVectorController NetworkVectorController { get => networkVectorController; set => networkVectorController = value; }

        // Start is called before the first frame update
        void Start()
        {
            Transform arrow = hologram.transform.Find("Arrow");
            cylinder = arrow.Find("CylinderContainer").Find("Cylinder");
            cone = arrow.Find("Cone");

            cylinderMaterial = cylinder.gameObject.GetComponent<Renderer>().material;
            coneMaterial = cone.gameObject.GetComponent<Renderer>().material;

            materialColor = cylinderMaterial.GetVector("_MainColor");
            materialAlpha = cylinderMaterial.GetFloat("_Alpha");

            hologramMaterial = hologram.transform.Find("Renderer").GetComponent<Renderer>().material;

            networkVectorController = hologram.transform.Find("NetworkVectorController").GetComponent<NetworkVectorController>();
        }

        // Update is called once per frame
        void Update()
        {
            if (blinking) {
                if (currentTime >= timeBlink) {
                    // transition complete
                    // assign the target color

                    changeMaterialColor(targetColor);
                    changeMaterialAlpha(targetAlpha);

                    currentBlinks++;
                    currentTime = 0;

                    targetColor = iniColor;
                    iniColor = cylinderMaterial.GetVector("_MainColor");

                    targetAlpha = iniAlpha;
                    iniAlpha = cylinderMaterial.GetFloat("_Alpha");
                } else {

                    // transition in progress
                    // calculate interpolated color
                    Color newColor = Color.Lerp(iniColor, targetColor, currentTime / timeBlink);
                    float newAlpha = Mathf.Lerp(iniAlpha, targetAlpha, currentTime / timeBlink);
                    changeMaterialColor(newColor);
                    changeMaterialAlpha(newAlpha);

                    // update the timer
                    currentTime += Time.deltaTime;
                }
            }
            

            if (currentBlinks >= nBlinks) {
                blinking = false;
            }
        }

        public void startBlinkEffect(int signal)
        {
            Color color;

            switch (signal) {
                case NetworkManager.SIGNAL_CLOSE:
                    color = closeColor;
                    break;
                case NetworkManager.SIGNAL_INTERMEDIATE:
                    color = intermediateColor;
                    break;
                case NetworkManager.SIGNAL_FAR:
                    color = farColor;
                    break;
                default:
                    print("UNKNOWN SIGNAL RECEIVED");
                    color = Color.gray;
                    break;
            }

            currentTime = 0;
            currentBlinks = 0;

            iniColor = materialColor;
            targetColor = color;

            iniAlpha = materialAlpha;
            targetAlpha = blinkAlpha;

            blinking = true;

            networkVectorController.createVector(color, signal);
        }

        public void changeMaterialColor(Color newColor)
        {
            cylinderMaterial.SetVector("_MainColor", newColor);
            coneMaterial.SetVector("_MainColor", newColor);
            hologramMaterial.SetVector("_MainColor", newColor);
        }

        public void changeMaterialAlpha(float newAlpha)
        {
            cylinderMaterial.SetFloat("_Alpha", newAlpha);
            coneMaterial.SetFloat("_Alpha", newAlpha);
            hologramMaterial.SetFloat("_Alpha", newAlpha);
        }
    }
}