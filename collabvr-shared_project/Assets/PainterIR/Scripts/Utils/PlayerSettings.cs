﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    [System.Serializable]
    public class PlayerSettings 
    {
        public bool seeTrajectory;
        public bool seePartialTrajectory;
        public bool seeHologram;
        public bool seeCommunicationPanel;

        public bool canControlTime;

        public bool isObserver;
        public bool isSinglePlayer;

        private ToolSelector toolSelector;

        public void setSinglePlayer()
        {
            isSinglePlayer = true;

            isObserver = false;
            seeCommunicationPanel = false;
            seeHologram = true;
            seeTrajectory = true;
            seePartialTrajectory = false;
            canControlTime = true;

            Debug.Log("PLAYER SETTINGS: This player is now in single player mode");
        }

        public void setObserver()
        {
            isSinglePlayer = false;
            isObserver = true;

            seeCommunicationPanel = true;
            seeHologram = false;
            seeTrajectory = true;
            seePartialTrajectory = false;

            canControlTime = false;

            GameManager.instance.seeTrajectory = true;

            Debug.Log("PLAYER SETTINGS: This player is now an observer");
        }

        public void setManipulator()
        {
            isSinglePlayer = false;
            isObserver = false;

            seeCommunicationPanel = false;
            seeHologram = true;
            seeTrajectory = true; 
            seePartialTrajectory = true;

            canControlTime = true;

            GameManager.instance.basketball.GetComponent<OwnershipTakeover>().requestOwnership();

            Debug.Log("PLAYER SETTINGS: This player is now a manipulator");
        }
        
        public void changeRole()
        {
            Debug.Log("PLAYER SETTINGS: This player is now changing roles");

            if (toolSelector == null) {
                toolSelector = GameObject.Find("PlayerVR").GetComponentInChildren<ToolSelector>();
            }

            toolSelector.toggleHologramVisibility(false);
            toolSelector.toggleCommunicationPanelVisibility(false);

            if (isObserver) {
                setManipulator();
            } else {
                setObserver();
            }
        }
        
    }
}