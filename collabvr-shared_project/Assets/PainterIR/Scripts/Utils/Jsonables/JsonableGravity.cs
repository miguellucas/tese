﻿using UnityEngine;
using System.Globalization;


namespace Painter.Utils
{
    class JsonableGravity : JsonableObject
    {
        private Rigidbody rigidbody;
        private float gravity = -9.8f;
        private bool changeGravity = false;

        public float Gravity { get => gravity; set => gravity = value; }

        private float drag = 1f;

        void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
            rigidbody.drag = 0;
            getData();
        }

        private void FixedUpdate()
        {
            //FOR DEBUGGING

            //END DEBUGGING
            if (changeGravity) {
                //rigidbody.AddForce(new Vector3(0, gravity, 0));
                //rigidbody.velocity += Physics.gravity * Time.fixedDeltaTime;
                //rigidbody.velocity *= Mathf.Clamp01(1f - drag * Time.fixedDeltaTime);

                var velocity = rigidbody.velocity;

                velocity += new Vector3(0, gravity, 0) * Time.fixedDeltaTime;

                velocity *= Mathf.Clamp01(1f - drag * Time.deltaTime);

                rigidbody.velocity = velocity;
            }
        }

        protected override void getData()
        {
            JSONObject obj = JsonManager.instance.JSONObject.GetField(objectName);

            if (obj == null) {
                Debug.LogError("JSON MANAGER: The name \"" + objectName + "\" does not exist in the provided JSON file! Please check for uppercase/lowercase situations.");
                return;
            }

            float gravity;
            //get the string from the json
            string gravity_str = obj.GetField(PARAMETER_GRAVITY).ToString();
            //cut the start and end chars
            gravity_str = gravity_str.Trim(charsToTrim);
            //convert to float and apply the gravity if successful
            if (float.TryParse(gravity_str, NumberStyles.Any, CultureInfo.InvariantCulture, out gravity)) {
                applyGravity(gravity);
            } else {
                Debug.LogError("JSON MANAGER: Wrong value provided for parameter \"" + PARAMETER_GRAVITY + "\" for the object \"" + objectName + "\". Using default value.");
                return;
            }
        }

        private void applyGravity(float gravity)
        {
            if (gravity < JsonManager.instance.minGravity || gravity > JsonManager.instance.maxGravity) {
                Debug.LogWarning("JSON MANAGER: Gravity value must be between " + JsonManager.instance.minGravity + " and " + JsonManager.instance.maxGravity + ". Resetting to default value");
                this.gravity = -JsonManager.instance.defaultGravity;
                return;
            }

            Debug.Log("JSON MANAGER: Applying modified gravity of value " + gravity + " to object \"" + gameObject.name + "\"");
            this.gravity = -gravity;
            this.changeGravity = true;
            this.rigidbody.useGravity = false;
        }
    }
}
