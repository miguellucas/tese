﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Painter.Utils
{
    
    public abstract class JsonableObject : MonoBehaviour
    {
        [SerializeField]
        protected string objectName;

        protected const string PARAMETER_GRAVITY = "gravity";
        protected const string PARAMETER_DRAG = "drag";
        protected const string PARAMETER_WIND_INTENSITY = "intensity";
        protected const string PARAMETER_WIND_DIRECTION = "direction";

        protected char[] charsToTrim = { '"', '\\' };

        protected abstract void getData();
        
    }
}