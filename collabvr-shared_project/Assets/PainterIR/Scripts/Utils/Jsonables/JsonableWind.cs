﻿using System.Globalization;
using UnityEngine;

namespace Painter.Utils
{
    class JsonableWind : JsonableObject
    {
        private Rigidbody rigidbody;

        void Start()
        {

            rigidbody = GetComponent<Rigidbody>();
            getData();
        }


        protected override void getData()
        {
            JSONObject obj = JsonManager.instance.JSONObject.GetField(objectName);

            if (obj == null) {
                Debug.LogError("JSON MANAGER: The name \"" + objectName + "\" does not exist in the provided JSON file! Please check for uppercase/lowercase situations.");
                return;
            }

            //get wind intensity
            float intensity;
            //get the string from the json
            string intensity_str = obj.GetField(PARAMETER_WIND_INTENSITY).ToString();
            //cut the start and end chars

            intensity_str = intensity_str.Trim(charsToTrim);
            //convert to float and apply the gravity if successful
            if (float.TryParse(intensity_str, NumberStyles.Any, CultureInfo.InvariantCulture, out intensity)) {
                applyWindIntensity(intensity);
            } else {
                Debug.LogError("JSON MANAGER: Wrong value provided for parameter \"" + PARAMETER_WIND_INTENSITY + "\" for the object \"" + objectName + "\". Using default value.");
                return;
            }


            //wind direction section
            JSONObject windDirection = obj.GetField(PARAMETER_WIND_DIRECTION);

            float x, y, z;
            string x_str = windDirection.GetField("x").ToString();
            string y_str = windDirection.GetField("y").ToString();
            string z_str = windDirection.GetField("z").ToString();

            x_str = x_str.Trim(charsToTrim);
            y_str = y_str.Trim(charsToTrim);
            z_str = z_str.Trim(charsToTrim);

            if (float.TryParse(x_str, NumberStyles.Any, CultureInfo.InvariantCulture, out x) && float.TryParse(y_str, NumberStyles.Any, CultureInfo.InvariantCulture, out y) && float.TryParse(z_str, NumberStyles.Any, CultureInfo.InvariantCulture, out z)) {
                Vector3 windDirectionVec = new Vector3(x, y, z);
                applyWindDirection(windDirectionVec);
            } else {
                Debug.LogError("JSON MANAGER: Wrong value provided for parameter \"" + PARAMETER_WIND_DIRECTION + "\" for the object \"" + objectName + "\". Using default value.");
                return;
            }
        }

        private void applyWindIntensity(float intensity)
        {
            if (intensity < JsonManager.instance.minWindIntensity || intensity > JsonManager.instance.maxWindIntensity) {
                Debug.LogWarning("JSON MANAGER: Wind intensity value must be between " + JsonManager.instance.minWindIntensity + " and " + JsonManager.instance.maxWindIntensity + ". Resetting to default value " + JsonManager.instance.defaultWindIntensity);
                GameManager.instance.WindArea.Intensity = JsonManager.instance.defaultWindIntensity;
                return;
            }

            Debug.Log("JSON MANAGER: Applying modified wind intensity of value " + intensity);
            GameManager.instance.WindArea.Intensity = intensity;
        }

        private void applyWindDirection(Vector3 direction)
        {
            GameManager.instance.WindArea.Direction = direction;
        }
    }
}

