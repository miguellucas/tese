﻿using UnityEngine;

public class KillPlane : MonoBehaviour
{

    private Transform spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = transform.Find("SpawnPoint");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("KILL PLANE: spawning object " + collision.gameObject.name);
        collision.transform.position = spawnPoint.position;
    }
}
