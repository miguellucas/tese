﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;
using Painter.Communication;

namespace Painter.Utils
{
    public class Hologram : MonoBehaviour
    {
        [Header("Hologram Manipulation")]
        public GameObject arrow;
        public Transform center;
        public float maxScale = 3f;
        public float velocityMultiplier = 8f;
        public Text velocityText;
        public float stabilityThreshold = 0.001f;

        [Header("Mirroring Object")]
        public bool allowMirroring = true;
        [ConditionalField("allowMirroring", true, ComparisonType.Equals)]
        public string objectToMirrorName = null;
        [ConditionalField("allowMirroring", true, ComparisonType.Equals)]
        public bool allowMirrorTranslation = false;
        [ConditionalField("allowMirroring", true, ComparisonType.Equals)]
        public bool allowMirrorRotation = false;
        [ConditionalField("allowMirroring", true, ComparisonType.Equals)]
        public bool allowMirrorScale = false;

        [Header("SteamVR Handler")]
        public SteamVR_Action_Boolean toggleVisibilityBtn = null;
        public SteamVR_Action_Boolean shootBallBtn = null;

        private GameObject mirroredObject;
        private Rigidbody mirroredObjectRigidbody;
        private GameObject hologramRenderer = null;

        private ToolSelector toolSelector;

        private Transform cylinder = null;
        private Transform cylinderObj = null;
        private Transform cone = null;
        private Vector3 originalConePosition = Vector3.zero;

        private float distanceIni;

        private BlinkController blinkController;

        public Rigidbody MirroredObjectRigidbody { get => mirroredObjectRigidbody; set => mirroredObjectRigidbody = value; }

        // Start is called before the first frame update
        void Start()
        {
            cylinder = arrow.transform.Find("CylinderContainer");
            cone = arrow.transform.Find("Cone");
            originalConePosition = cone.transform.localPosition;
            cylinderObj = cylinder.Find("Cylinder");

            //distanceIni = cylinderObj.gameObject.GetComponent<Renderer>().bounds.size.x + cone.gameObject.GetComponent<Renderer>().bounds.size.x; 

            //find object to mirror in the scene
            /*Object[] objects = FindObjectsOfType(typeof(MirroredObject));
            foreach (Object obj in objects) {
                MirroredObject thisMirroredObject = (MirroredObject) obj;
                if (thisMirroredObject.MirrorObjectType == mirroredObjectType) {
                    mirroredObject = thisMirroredObject;
                    break;
                }
            }*/
            //Get the hologram renderer and disable it by default
            hologramRenderer = transform.Find("Renderer").gameObject;
            //hologramRenderer.SetActive(false);
            //arrow.SetActive(false);
            toolSelector = transform.parent.GetComponent<ToolSelector>();

            blinkController = GetComponent<BlinkController>();
        }

        // Update is called once per frame
        void Update()
        {
            //update hologram position
            if (mirroredObject && hologramRenderer) {
                if (allowMirrorTranslation) {
                    hologramRenderer.transform.localPosition = mirroredObject.transform.localPosition;
                }
                if (allowMirrorRotation) {
                    hologramRenderer.transform.rotation = mirroredObject.transform.rotation;
                }
                if (allowMirrorScale) {
                    hologramRenderer.transform.localScale = mirroredObject.transform.localScale;
                }
            } else {
                Debug.LogWarning("Hologram: No object to mirror found!");
            }

            //shoot ball if hologram is showing
            if (!GameManager.instance.settings.isObserver && toolSelector.IsHologramActivated && shootBallBtn != null && shootBallBtn.stateDown) {
                applyHologramVelocity();
            }

            if (velocityText != null) {
                if (toolSelector.IsHologramActivated) {
                    Vector3 velocity = mirroredObjectRigidbody.velocity;
                    float velocityMagnitude = velocity.magnitude;
                    float newVelocityMagnitude = (mirroredObject.transform.forward * cylinder.localScale.y * velocityMultiplier).magnitude;
                    //TODO: velocityText is null sometimes
                    string text = "";
                    if (velocityMagnitude < newVelocityMagnitude) {
                        text += "<color=green>New Velocity: " + Math.Round(newVelocityMagnitude, 2) + "</color> m/s";
                    } else {
                        text += "<color=red>New Velocity: " + Math.Round(newVelocityMagnitude, 2) + "</color> m/s";
                    }
                    velocityText.text = text;
                } else {
                    velocityText.text = "";
                }
            }
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (mirroredObject == null) {
                mirroredObject = GameObject.Find(objectToMirrorName);
                mirroredObjectRigidbody = mirroredObject.GetComponent<Rigidbody>();
            }
        }

        public void rotateArrow(Vector3 point)
        {
            //since LookAt rotates the object so that the Forward vector faces the point, by rotating 90 deg in the X axis, it makes the object point the Up vector to the point
            arrow.transform.LookAt(point);
            arrow.transform.Rotate(new Vector3(1.0f, 0, 0), 90);

            Vector3 localUp = transform.worldToLocalMatrix.MultiplyVector(arrow.transform.up);
            GameManager.instance.hologramRotation = localUp;
        }

        public void processScaleArrow(Vector3 point)
        {
            //get distance from the center of the hologram to the hand center point
            float distance = Vector3.Distance(point, center.position);
            //get the new scale based on the previous distance (length of the vector) and the current scale
            float newScale = distance * cylinder.localScale.y / distanceIni;

            if (Mathf.Abs(newScale - cylinder.localScale.y) < stabilityThreshold) {
                return;
            }
            //do not let it go past this value
            if (newScale > maxScale) {
                newScale = maxScale;
            }

            //change the cylinder scale and place the cone on top of the cylinder
            cylinder.localScale = new Vector3(cylinder.localScale.x, newScale, cylinder.localScale.z);
            cone.localPosition = new Vector3(originalConePosition.x, originalConePosition.y * cylinder.localScale.y, originalConePosition.z);

            //reset the current distance
            distanceIni = distance;

            Vector3 localUp = transform.worldToLocalMatrix.MultiplyVector(arrow.transform.up);

            //a dummy was needed because I didn't want to mess the original orientation of mirroredObject
            GameObject dummy = new GameObject();
            dummy.transform.forward = localUp;
            GameManager.instance.hologramForce = dummy.transform.forward * newScale * velocityMultiplier;
            Destroy(dummy);

            if (!GameManager.instance.settings.isSinglePlayer) {
                NetworkManager.instance.sendHologramData(GameManager.instance.hologramForce);
            }
            
        }

        public void applyHologramVelocity()
        {
            float currentScale = cylinder.localScale.y;

            //Get the local rotation of the hologram arrow. The Up vector will become the forward vector of the real ball.
            Vector3 localUp = transform.worldToLocalMatrix.MultiplyVector(arrow.transform.up);
            mirroredObject.transform.forward = localUp;
            Vector3 newVelocity = mirroredObject.transform.forward * currentScale * velocityMultiplier;

            //apply the force to the ball
            //mirroredObjectRigidbody.AddForce(mirroredObject.transform.forward * currentScale * velocityMultiplier, ForceMode.Impulse);

            //restart the time if it is stopped
            if (TimeManager.instance.TimeStopped) {
                TimeManager.instance.restartTime();
                GameManager.instance.addResumedShot(mirroredObject.transform.position, newVelocity);
            } else {
                GameManager.instance.addShot(mirroredObject.transform.position, newVelocity);
            }

            //apply the velocity to the object
            mirroredObjectRigidbody.velocity = newVelocity;
            blinkController.NetworkVectorController.clearAllVectors();
        }

        public void startGrabMovement(Vector3 point)
        {
            float distance = Vector3.Distance(point, center.position);
            distanceIni = distance;

            AnalyticsManager.instance.addGrab(new Grab(GameManager.instance.hologramForce, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond));
            print("start grab at " + DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
        }

        public void finishGrabMovement()
        {
            AnalyticsManager.instance.finishLastGrab(GameManager.instance.hologramForce, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
            print("finish grab at " + DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
        }

        
    }
}