﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    public class HologramGrabber : MonoBehaviour
    {
        [Header("Hologram")]
        public Hologram hologram;
        public Transform rightHand;
        public Color highlightColor;
        [Range(0,1)]
        public float highlightAlpha = 0.9f;

        [Header("SteamVR Handler")]
        public SteamVR_Action_Boolean grabBtn = null;

        private bool canGrab = false;
        private GameObject coneObject;

        private Vector4 initialShaderColor;
        private float initialShaderAlpha;

        private Vector3 initialGrabberPosition;

        private Transform pivot;

        // Start is called before the first frame update
        void Start()
        {
            initialGrabberPosition = transform.localPosition;
            findPivot();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void FixedUpdate()
        {

            if (canGrab && grabBtn != null && grabBtn.stateDown) {
                transform.position = pivot.position;
                hologram.startGrabMovement(transform.position);
            }

            if (canGrab && grabBtn != null && grabBtn.state) {
                //coneObject.GetComponent<Renderer>().material.color = Color.red;
                hologram.rotateArrow(transform.position);
                hologram.processScaleArrow(transform.position);
            }
            else {
                //coneObject.GetComponent<Renderer>().material.color = Color.yellow;
            }

            if (canGrab && grabBtn != null && grabBtn.stateUp) {
                transform.position = Vector3.zero;
                transform.localPosition = initialGrabberPosition;
                hologram.finishGrabMovement();
            }


         }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "HologramCone") {
                coneObject = other.gameObject;
                
                //modify custom shader color and alpha values
                Material thisMaterial = coneObject.GetComponent<Renderer>().material;
                initialShaderColor = thisMaterial.GetVector("_MainColor");
                initialShaderAlpha = thisMaterial.GetFloat("_Alpha");
                thisMaterial.SetVector("_MainColor", highlightColor);
                thisMaterial.SetFloat("_Alpha", highlightAlpha);

                canGrab = true;
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "HologramCone") {
                coneObject.GetComponent<Renderer>().material.color = Color.white;
                coneObject.GetComponent<Renderer>().material.SetVector("_MainColor", initialShaderColor);
                coneObject.GetComponent<Renderer>().material.SetFloat("_Alpha", initialShaderAlpha);
                canGrab = false;
            }

        }

        private void findPivot()
        {
            pivot = hologram.transform.Find("Arrow").Find("Cone").Find("Pivot");
            //print("found pivot " + pivot.name);
        }
    }
}