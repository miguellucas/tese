﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class NetworkVectorController : MonoBehaviour
    {
        public int maximumVectors = 5;
        [Range(0,1)]
        public float alphaValue = 1;

        /*public Color closeColor = Color.green;
        public Color intermediateColor = Color.yellow;
        public Color farColor = Color.red;*/

        private GameObject vectorToCopy;

        public Queue<GameObject> currentNetworkVectors = new Queue<GameObject>();
        public List<Signal> currentSignals = new List<Signal>();

        private float velocityMultiplier;

        // Start is called before the first frame update
        void Start()
        {
            vectorToCopy = transform.parent.Find("Arrow").gameObject;
            velocityMultiplier = transform.parent.GetComponent<Hologram>().velocityMultiplier;
            GameManager.instance.NetworkVectorController = this;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void createVector(Color color, int signal)
        {
            GameObject newNetworkVector = Instantiate(vectorToCopy, transform.parent);

            Transform cylinder = newNetworkVector.transform.Find("CylinderContainer").Find("Cylinder");
            Transform cone = newNetworkVector.transform.Find("Cone");

            cone.gameObject.tag = "Untagged";

            Material cylinderMaterial = cylinder.gameObject.GetComponent<Renderer>().material;
            Material coneMaterial = cone.gameObject.GetComponent<Renderer>().material;

            cylinderMaterial.SetVector("_MainColor", color);
            coneMaterial.SetVector("_MainColor", color);

            cylinderMaterial.SetFloat("_Alpha", alphaValue);
            coneMaterial.SetFloat("_Alpha", alphaValue);

            currentNetworkVectors.Enqueue(newNetworkVector);

            if (currentNetworkVectors.Count > maximumVectors) {
                GameObject lastVector = currentNetworkVectors.Dequeue();
                Destroy(lastVector);
                currentSignals.RemoveAt(0);
                GameManager.instance.Signals.Add(new Signal("Dequeued"));
            }


            float currentScale = cylinder.localScale.y;

            //Get the local rotation of the hologram arrow. The Up vector will become the forward vector of the real ball.
            Vector3 localUp = transform.worldToLocalMatrix.MultiplyVector(newNetworkVector.transform.up);
            GameObject dummy = new GameObject();
            dummy.transform.forward = localUp;
            Vector3 velocity = dummy.transform.forward * currentScale * velocityMultiplier;



            Signal newSignal = new Signal(GameManager.instance.basketball.transform.position, velocity, signal);
            GameManager.instance.Signals.Add(newSignal);
            currentSignals.Add(newSignal);
            AnalyticsManager.instance.addSignal(newSignal);
        }

        public void clearAllVectors()
        {
            while (currentNetworkVectors.Count > 0) {
                GameObject lastVector = currentNetworkVectors.Dequeue();
                Destroy(lastVector);
            }
            currentSignals.Clear();

            GameManager.instance.Signals.Add(new Signal("Shot taken"));
        }
    }
}