﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace Painter.Utils
{
    public class CurrentVelocityVector : MonoBehaviour
    {
        [Header("Current Velocity Vector")]
        public bool enableCurrentVelocityVector = true;
        [ConditionalField("enableCurrentVelocityVector", true, ComparisonType.Equals)]
        public float minVectorSize = 0;
        [ConditionalField("enableCurrentVelocityVector", true, ComparisonType.Equals)]
        public float maxVectorSize = 2;
        [ConditionalField("enableCurrentVelocityVector", true, ComparisonType.Equals)]
        public float normalizingFactor = 5;
        [ConditionalField("enableCurrentVelocityVector", true, ComparisonType.Equals)]
        public float minVelocityThreshold = 0.01f;

        [Header("UI")]
        public Text currentVelocityText;

        private Transform cylinder = null;
        private Transform cone = null;
        private Vector3 originalConePosition = Vector3.zero;

        private Hologram hologram;

        void Start()
        {
            cylinder = transform.Find("CylinderContainer");
            cone = transform.Find("Cone");
            originalConePosition = cone.transform.localPosition;

            //requires a parent object with the Hologram class
            if (!transform.parent.gameObject.GetComponent<Hologram>()) {
                // Throw an error
                Debug.LogError("Parent does not contain Hologram component", transform.parent);
                enableCurrentVelocityVector = false;
                return;
            }

            hologram = transform.parent.GetComponent<Hologram>();
        }

        void Update()
        {
            if (enableCurrentVelocityVector) {
                toggleVectorVisibility(true);
                updateVector();
            } else {
                toggleVectorVisibility(false);
            }

        }

        public void toggleVectorVisibility(bool state)
        {
            cylinder.gameObject.SetActive(state);
            cone.gameObject.SetActive(state);
            currentVelocityText.text = "Current Velocity: < " + minVelocityThreshold + " m/s";
        }

        public void updateVector()
        {
            if (hologram.MirroredObjectRigidbody.velocity.magnitude < minVelocityThreshold) {
                toggleVectorVisibility(false);
                return;
            }

            toggleVectorVisibility(true);
            transform.rotation = Quaternion.LookRotation(hologram.MirroredObjectRigidbody.velocity);
            transform.Rotate(new Vector3(1.0f, 0, 0), 90);

            float magnitude = hologram.MirroredObjectRigidbody.velocity.magnitude;
            scaleArrow(magnitude);

            string text = "Current Velocity: " + Math.Round(magnitude,2) + " m/s";
            currentVelocityText.text = text;
        }

        public void scaleArrow(float magnitude)
        {
            //normalize the velocity by a factor so the arrow growth is not so sudden
            magnitude /= normalizingFactor;

            //if the mirror object is stopped or close to it, show a minimum arrow
            //if not, resize the arrow between minVectorSize and maxVectorSize
            if (magnitude < minVelocityThreshold) {
                magnitude = 0;
            } else {
                if (magnitude < minVectorSize) {
                    magnitude = minVectorSize;
                } else if (magnitude > maxVectorSize) {
                    magnitude = maxVectorSize;
                }
            }

            cylinder.localScale = new Vector3(cylinder.transform.localScale.x, magnitude, cylinder.transform.localScale.z);
            cone.localPosition = new Vector3(originalConePosition.x, originalConePosition.y * cylinder.transform.localScale.y, originalConePosition.z);
        }
    }
}
