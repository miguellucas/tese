﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;

namespace Painter.Utils
{
    public class MirrorObject : MonoBehaviour
    {

        /*[SerializeField]
        private string mirroredObjectType;
        [SerializeField]
        private GameObject mirrorObject;
        [SerializeField]
        private bool toggleVisibilityOnBtnPress = true;
        //[ConditionalField("toggleVisibilityOnBtnPress")]
        [SerializeField]
        private SteamVR_Action_Boolean toggleVisibilityBtn = null;


        private MirroredObject mirroredObject;
        private bool isHologramActivated = false;


        // Start is called before the first frame update
        void Start()
        {
            //GameObject asd = SceneManager.GetSceneByName("cenas").GetRootGameObjects()[0];
            Object[] objects = FindObjectsOfType(typeof(MirroredObject));

            foreach (Object obj in objects) {
                MirroredObject thisMirroredObject = (MirroredObject) obj;
                if (thisMirroredObject.MirrorObjectType == mirroredObjectType) {
                    mirroredObject = thisMirroredObject;
                    break;
                }
            }

            if (toggleVisibilityOnBtnPress) {
                mirrorObject.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (mirroredObject) {
                if (mirroredObject.AllowMirrorTranslation) {
                    mirrorObject.gameObject.transform.localPosition = mirroredObject.transform.localPosition;
                }
                if (mirroredObject.AllowMirrorRotation) {
                    mirrorObject.gameObject.transform.rotation = mirroredObject.transform.rotation;
                }
                if (mirroredObject.AllowMirrorScale) {
                    mirrorObject.gameObject.transform.localScale = mirroredObject.transform.localScale;
                }
            } else {
                Debug.LogWarning("Mirror Object: No object to mirror found!");
            }

            //Debug.Log(JsonManager.instance.MaxGravity);

            if (toggleVisibilityOnBtnPress && toggleVisibilityBtn.stateDown) {
                //make hologram disappear
                print("HOLOGRAM PRESSED");
                isHologramActivated = !isHologramActivated;
                mirrorObject.SetActive(isHologramActivated);
            }
        }*/
    }
}