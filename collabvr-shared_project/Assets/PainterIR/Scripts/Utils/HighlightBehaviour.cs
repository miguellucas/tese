﻿using UnityEngine;

namespace Painter.Utils
{
    public abstract class HighlightBehaviour : MonoBehaviour
    {
        public abstract void StartHighlightEffect(Transform position);
        public abstract void StopHighlightEffect();
    }
}
