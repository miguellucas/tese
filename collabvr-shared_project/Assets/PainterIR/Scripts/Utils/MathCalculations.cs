﻿using System;
using UnityEngine;

namespace Painter.Utils
{
    public abstract class MathCalculations
    {
        public static bool IsWithinCameraFOV(Camera camera, Vector3 position, float fovRatio)
        {
            // Camera's forward direction
            var camera_direction = camera.transform.forward;

            // Direction from the camera to this object
            var my_direction = position - camera.transform.position;
            my_direction.Normalize();

            // Dot product of the 2 will result ina positive value 
            // if object is in front of the camera
            var dot_directions =
                Vector3.Dot(camera_direction, my_direction);

            // Camera's field of view
            var field_of_view =
                Mathf.Cos(Camera.main.fieldOfView * Mathf.Deg2Rad * fovRatio / 2.0f);

            return dot_directions > field_of_view;
        }
    }
}
