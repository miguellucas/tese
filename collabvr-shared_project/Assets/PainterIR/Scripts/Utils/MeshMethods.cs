﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Painter.Utils;
using Photon.Realtime;

public abstract class MeshMethods
{
    public static void SpawnCopyMesh(GameObject copy_this, out MeshRenderer renderer)
    {
        // Spawn a copy of this object
        var empty_gameobject = new GameObject();
        empty_gameobject.transform.parent = copy_this.transform;
        empty_gameobject.transform.localPosition = Vector3.zero;
        empty_gameobject.transform.localRotation = Quaternion.identity;
        empty_gameobject.transform.localScale = Vector3.one;

        // Add the mesh components to the copy
        var mesh_filter = empty_gameobject.AddComponent<MeshFilter>();
        mesh_filter.mesh = copy_this.GetComponent<MeshFilter>().mesh;
        renderer = empty_gameobject.AddComponent<MeshRenderer>();
    }

}
