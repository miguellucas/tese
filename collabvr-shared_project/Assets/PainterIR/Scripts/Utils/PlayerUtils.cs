﻿
using Photon.Realtime;
using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    public abstract class PlayerUtils
    {
        public static string RootName(Player player)
        {
            return ("Painter#" + player.ActorNumber);
        }
        
        public static Transform GetUserRoot(Player player)
        {
            string playerName = PlayerUtils.RootName(player);
            GameObject playerRoot = GameObject.Find(playerName);
            return playerRoot == null ? null : playerRoot.transform;
        }

        public static Color GetUserColor(Player player)
        {
            int value = player.ActorNumber % 6;
            switch(value)
            {
                case 0:
                    return Color.yellow;
                case 1:
                    return Color.blue;
                case 2:
                    return Color.cyan;
                case 3:
                    return Color.magenta;
                case 4:
                    return Color.green;
                case 5:
                    return Color.red;
                default:
                    return Color.gray;
            }
        }

        public static bool IsDeviceAR()
        {
            return Application.isMobilePlatform;
        }

        public static bool IsDeviceVR()
        {
            return !IsDeviceAR();
        }

        public static bool IsDeviceAttached()
        {
            return OpenVR.IsHmdPresent();
        }

    }
}
