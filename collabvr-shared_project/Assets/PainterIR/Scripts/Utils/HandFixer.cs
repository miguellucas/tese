﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Valve.VR;

namespace Painter.Utils
{

    public class HandFixer : MonoBehaviour
    {
        public float timeToWaitBeforeFix = 6f;

        private float timer = 0;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;

            if (timer >= timeToWaitBeforeFix) {
                if (GameManager.instance.settings.isObserver) {
                    Transform transform = PlayerUtils.GetUserRoot(PhotonNetwork.LocalPlayer);
                    SteamVR_Behaviour_Skeleton skeleton = transform.Find("Left Hand").Find("vr_glove_left").GetComponent<SteamVR_Behaviour_Skeleton>();
                    skeleton.mirroring = SteamVR_Behaviour_Skeleton.MirrorType.None;
                    Debug.Log("Fixing Hand Skeleton");
                }
                
                Destroy(this.gameObject);
            }
        }
    }
}