﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Painter.Utils
{
    public class ExperimentManager : MonoBehaviour
    {
        public static ExperimentManager instance = null;

        public bool experimentMode = false;

        public Transform[] shootingPositions0_3m;
        public Transform[] shootingPositions3_6m;
        public Transform[] shootingPositions6_infm;
        public Transform[] shootingPositions0_15v;
        public Transform[] shootingPositions15_infv;
        public Transform[] shootingPositionsTimeControl;
        public Transform[] shootingPositions3_6m_collab;
        public Transform[] shootingPositions6_infm_collab;
        public Transform[] shootingPositionsTimeControl_collab;

        public List<ExperimentPosition> experimentPositions = new List<ExperimentPosition>();

        private bool isDistancePhase1 = false;
        private bool isDistancePhase2 = false;
        private bool isDistancePhase3 = false;
        private bool isVelocityPhase1 = false;
        private bool isVelocityPhase2 = false;
        private bool isTimeControlPhase = false;
        private bool isCollabDistancePhase1 = false;
        private bool isCollabDistancePhase2 = false;
        private bool isCollabTimeControlPhase = false;

        private Vector3 nextPosition;
        private int currentIndex = 0;
        private Transform[] currentPositions;
        private ExperimentPosition.PositionCategory currentCategory;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space)) {
                experimentMode = true;
                beginExperiment();
            }
        }

        private void beginExperiment()
        {
            GameManager.instance.Shots = new List<GameManager.Shot>();
            GameManager.instance.Signals = new List<Signal>();

            beginDistancePhase();
        }

        private void beginDistancePhase()
        {
            isDistancePhase1 = true;
            currentPositions = shootingPositions0_3m;
            currentCategory = ExperimentPosition.PositionCategory.Distance_0_3;
            setNextPosition();
        }

        public void setNextPosition(GameManager.Shot shot = null)
        {
            if (GameManager.instance.settings.isSinglePlayer || !GameManager.instance.settings.isObserver) {
                if (shot != null && (isTimeControlPhase || isCollabTimeControlPhase) && !(shot is GameManager.ResumedShot)) {
                    return;
                }

                if (shot != null) {
                    experimentPositions.Add(new ExperimentPosition(currentPositions[currentIndex-1], shot, currentCategory));
                    AnalyticsManager.instance.addShot(new AnalyticsShot(shot.Velocity, shot.Distance, shot.ShotResult, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond, currentCategory));
                }

                if (currentIndex >= currentPositions.Length) {
                    changeCurrentPositions();
                }
                
                GameManager.instance.basketball.transform.position = currentPositions[currentIndex++].position;
                GameManager.instance.basketball.GetComponent<Rigidbody>().velocity = Vector3.zero;
                GameManager.instance.basketball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
        }

        public void changeCurrentPositions()
        {
            if (isDistancePhase1) {
                isDistancePhase1 = false;
                isDistancePhase2 = true;
                currentPositions = shootingPositions3_6m;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Distance_3_6;
            } else if (isDistancePhase2) {
                isDistancePhase2 = false;
                isDistancePhase3 = true;
                currentPositions = shootingPositions6_infm;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Distance_6_inf;
            } else if (isDistancePhase3) {
                isDistancePhase3 = false;
                isVelocityPhase1 = true;
                currentPositions = shootingPositions0_15v;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Velocity_0_15;
            } else if (isVelocityPhase1) {
                isVelocityPhase1 = false;
                isVelocityPhase2 = true;
                currentPositions = shootingPositions15_infv;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Velocity_15_inf;
            } else if (isVelocityPhase2) {
                isVelocityPhase2 = false;
                isTimeControlPhase = true;
                currentPositions = shootingPositionsTimeControl;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Time_Control;
            } else if (isTimeControlPhase) {
                isTimeControlPhase = false;
                isCollabDistancePhase1 = true;
                currentPositions = shootingPositions3_6m_collab;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Collab_3_6;
            } else if (isCollabDistancePhase1) {
                isCollabDistancePhase1 = false;
                isCollabDistancePhase2 = true;
                currentPositions = shootingPositions6_infm_collab;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Collab_6_inf;
            } else if (isCollabDistancePhase2) {
                isCollabDistancePhase2 = false;
                isCollabTimeControlPhase = true;
                currentPositions = shootingPositionsTimeControl_collab;
                currentIndex = 0;
                currentCategory = ExperimentPosition.PositionCategory.Collab_Time_Control;
            } else if (isCollabTimeControlPhase) {
                isCollabTimeControlPhase = false;
                experimentMode = false;
                //currentPositions = shootingPositionsTimeControl_collab;
                currentIndex = 0;
            }
        }
    }
}