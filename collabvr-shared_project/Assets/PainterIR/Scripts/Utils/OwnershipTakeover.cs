﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnershipTakeover : MonoBehaviourPun
{
    public void requestOwnership()
    {
        photonView.RequestOwnership();
    }
}
