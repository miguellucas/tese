﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class BasketballBall : MonoBehaviour
    {
        public ParticleSystem shotMadeEffect;
        public AudioClip shotMadeSound;

        private bool passedTop = false;
        private bool passedBottom = false;
        private float secondsBeforeExpiry = 2f;

        private AudioSource audioSource;

        // Start is called before the first frame update
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "HoopPointsTop") {
                print("Passed top");
                if (!passedBottom) {
                    passedTop = true;
                }
                StartCoroutine(expireLastShot());
            }

            if (other.tag == "HoopPointsBottom") {
                print("Passed bottom");
                if (passedTop) {
                    print("Passed bottom after top");
                    GameManager.instance.scoreAttempt();
                    shotMadeEffect.Play();
                    audioSource.PlayOneShot(shotMadeSound);
                }

                passedBottom = true;
                StartCoroutine(expireLastShot());
            }

            if (other.tag == "HoopAttempts") {
                print("Passed attempt");
                GameManager.instance.addAttempt();
                GameManager.instance.BallInsideAttemptArea = true;
            }

            if (other.tag == "HoopClose") {
                GameManager.instance.closeAttempt();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "HoopAttempts") {
                GameManager.instance.BallInsideAttemptArea = false;
            }
        }

        private IEnumerator expireLastShot()
        {
            yield return new WaitForSeconds(secondsBeforeExpiry);

            passedTop = false;
            passedBottom = false;
        }
    }
}