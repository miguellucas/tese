﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class Grab
    {

        private long timestamp_ini;
        private long timestamp_fin;
        private SerializableVector3 velocity_vectorial_ini;
        private SerializableVector3 velocity_vectorial_fin;

        private float velocity_ini;
        private float velocity_fin;
        private long time_grabbed;
        private float velocity_difference;
        private SerializableVector3 velocity_difference_vectorial;

        private bool finished = false;
        

        public Grab(Vector3 velocity, long timestamp)
        {
            Velocity_vectorial_ini = velocity;
            Timestamp_ini = timestamp;

            Velocity_ini = velocity.magnitude;
        }

        public long Timestamp_ini { get => timestamp_ini; set => timestamp_ini = value; }
        public long Timestamp_fin { get => timestamp_fin; set => timestamp_fin = value; }
        public SerializableVector3 Velocity_vectorial_ini { get => velocity_vectorial_ini; set => velocity_vectorial_ini = value; }
        public SerializableVector3 Velocity_vectorial_fin { get => velocity_vectorial_fin; set => velocity_vectorial_fin = value; }
        public float Velocity_ini { get => velocity_ini; set => velocity_ini = value; }
        public float Velocity_fin { get => velocity_fin; set => velocity_fin = value; }
        public long Time_grabbed { get => time_grabbed; set => time_grabbed = value; }
        public float Velocity_difference { get => velocity_difference; set => velocity_difference = value; }
        public SerializableVector3 Velocity_difference_vectorial { get => velocity_difference_vectorial; set => velocity_difference_vectorial = value; }
        public bool Finished { get => finished; set => finished = value; }

        public void finishGrab(Vector3 velocity, long timestamp)
        {
            Velocity_vectorial_fin = velocity;
            Timestamp_fin = timestamp;

            Velocity_fin = velocity.magnitude;

            Time_grabbed = Timestamp_fin - Timestamp_ini;
            Velocity_difference_vectorial = (SerializableVector3) Velocity_vectorial_fin.toVector3() - Velocity_vectorial_ini.toVector3();
            Velocity_difference = Velocity_fin - Velocity_ini;

            finished = true;
        }
        
    }
}

