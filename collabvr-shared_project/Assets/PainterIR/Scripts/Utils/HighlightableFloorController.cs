﻿using UnityEngine;

namespace Painter.Utils
{
    [RequireComponent(typeof(MeshRenderer))]
    public class HighlightableFloorController : HighlightBehaviour
    {
        #region Singleton

        private static HighlightableFloorController instance;
        public static HighlightableFloorController Instance
        {
            get
            {
                return instance;
            }
        }

        public HighlightableFloorController()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>HighlightableFloorController</b>: Multiple instances of HighlightableFloorController not supported.");
                return;
            }
#endif

            instance = this;
        }

        #endregion

        private MeshRenderer m_meshRenderer;
        private bool m_enabled = false;
        
        private Transform m_highlightPoint;

        [Range(0.5f, 10f)]
        public float RangeTime = 1.0f;
        public float NextTimeDelay = 1.0f;
        private float m_time = 0.0f;
        private float m_waitTime = 0.0f;

        // Start is called before the first frame update
        private void Start()
        {
            m_meshRenderer = GetComponent<MeshRenderer>();
        }

        // Update is called once per frame
        private void Update()
        {
            if (m_meshRenderer == null || !m_enabled)
                return;

            if (m_highlightPoint == null)
            {
                StopHighlightEffect();
            }

            if (m_time > 1.0f)
            {
                m_time = 1.0f;
            }

            m_meshRenderer.material.SetVector("_Position", m_highlightPoint.position);
            m_meshRenderer.material.SetFloat("_MTime", m_time);

            if (m_time >= 1.0f)
            {
                m_waitTime += Time.deltaTime;
                if (m_waitTime >= NextTimeDelay)
                {
                    m_waitTime = 0.0f;
                    m_time = 0.0f;
                }
            }
            else
            {
                m_time += Time.deltaTime / RangeTime;
            }
        }

        public override void StartHighlightEffect(Transform position)
        {
            m_time = 0.0f;
            m_enabled = true;
            m_highlightPoint = position;
        }

        public override void StopHighlightEffect()
        {
            m_time = 0.0f;
            m_enabled = false;
            m_meshRenderer.material.SetFloat("_MTime", 1.0f);
        }
    }
}