﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class AnalyticsTimeControl
    {
        private float distance;
        private float velocity_ini;
        private SerializableVector3 velocity_vectorial_ini;
        private float velocity_fin;
        private SerializableVector3 velocity_vectorial_fin;
        private bool finished = false;

        public AnalyticsTimeControl(float distance, Vector3 velocity)
        {
            this.distance = distance;
            this.velocity_vectorial_ini = velocity;
            this.velocity_ini = velocity.magnitude;
        }

        public void finishTimeControl(Vector3 velocity)
        {
            velocity_vectorial_fin = velocity;
            velocity_fin = velocity.magnitude;
        }

        public float Distance { get => distance; set => distance = value; }
        public float Velocity_Ini { get => velocity_ini; set => velocity_ini = value; }
        public SerializableVector3 Velocity_vectorial_Ini { get => velocity_vectorial_ini; set => velocity_vectorial_ini = value; }
        public float Velocity_fin { get => velocity_fin; set => velocity_fin = value; }
        public SerializableVector3 Velocity_vectorial_fin { get => velocity_vectorial_fin; set => velocity_vectorial_fin = value; }
        public bool Finished { get => finished; set => finished = value; }
    }
}

