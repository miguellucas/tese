﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class AnalyticsShot
    {
        private float distance;
        private SerializableVector3 velocity_vectorial;
        private float velocity_magnitude;
        private GameManager.Shot.ShotOutcome shot_result;
        private long timestamp;
        private long time_since_last_shot;
        private ExperimentPosition.PositionCategory category;
        private List<Grab> grabs = new List<Grab>();
        private List<Signal> signals = new List<Signal>();
        private List<AnalyticsTimeControl> timeControlUsages = new List<AnalyticsTimeControl>();

        private long meanGrabbingTime;
        private float meanVelocityDifference;
        private SerializableVector3 meanVelocityVectorialDifference;
        private int totalGrabs;

        public AnalyticsShot(Vector3 velocity, float distance, GameManager.Shot.ShotOutcome shot_result, long timestamp, ExperimentPosition.PositionCategory category)
        {
            this.distance = distance;
            this.velocity_vectorial = velocity;
            velocity_magnitude = velocity.magnitude;
            this.shot_result = shot_result;
            this.timestamp = timestamp;
            this.category = category;
        }

        public float Distance { get => distance; set => distance = value; }
        public SerializableVector3 Velocity_vectorial { get => velocity_vectorial; set => velocity_vectorial = value; }
        public float Velocity_magnitude { get => velocity_magnitude; set => velocity_magnitude = value; }
        public GameManager.Shot.ShotOutcome Shot_result { get => shot_result; set => shot_result = value; }
        public long Timestamp { get => timestamp; set => timestamp = value; }
        public long Time_since_last_shot { get => time_since_last_shot; set => time_since_last_shot = value; }
        public ExperimentPosition.PositionCategory Category { get => category; set => category = value; }
        public List<Grab> Grabs { get => grabs; set => grabs = value; }
        public List<Signal> Signals { get => signals; set => signals = value; }
        /*public long MeanGrabbingTime { get => meanGrabbingTime; set => meanGrabbingTime = value; }
        public float MeanVelocity { get => meanVelocityDifference; set => meanVelocityDifference = value; }
        public SerializableVector3 MeanVelocityVectorial { get => meanVelocityVectorialDifference; set => meanVelocityVectorialDifference = value; }
        public int TotalGrabs { get => totalGrabs; set => totalGrabs = value; }*/
        public List<AnalyticsTimeControl> TimeControlUsages { get => timeControlUsages; set => timeControlUsages = value; }

        public void analyze()
        {
            int totalGrabsAnalyzed = 0;
            long totalTimeGrabbed = 0;
            float totalVelocityDifference = 0;
            Vector3 totalVelocityVectorDifference = Vector3.zero;

            foreach(Grab grab in grabs) {
                if (grab.Time_grabbed == 0 || grab.Velocity_difference == 0) {
                    continue;
                }

                totalGrabsAnalyzed++;
                totalTimeGrabbed += grab.Time_grabbed;
                totalVelocityDifference += grab.Velocity_difference;
                totalVelocityVectorDifference += grab.Velocity_difference_vectorial;

                meanGrabbingTime = totalTimeGrabbed / totalGrabsAnalyzed;
                meanVelocityDifference = totalVelocityDifference / totalGrabsAnalyzed;
                meanVelocityVectorialDifference = totalVelocityVectorDifference / totalGrabsAnalyzed;
                totalGrabs = totalGrabsAnalyzed;

            }
        }
    }
}