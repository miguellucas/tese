﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Painter.VR.Core;
using Painter.Communication;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Threading;
using Photon.Pun;

namespace Painter.Utils
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance = null;

        
        [Header("Basketball Section")]
        public GameObject basketball;
        public GameObject hoopCenter;
        public bool seeTrajectory = true;
        public Vector3 hologramRotation = Vector3.up;
        public Vector3 hologramForce = Vector3.up;
        public float distance3Pts;
        [Header("Wind Section")]
        public WindArea windArea;
        [Header("Player Configurations")]
        public PlayerSettings settings;

        public WindArea WindArea { get => windArea; set => windArea = value; }
        public bool BallInsideAttemptArea { get => ballInsideAttemptArea; set => ballInsideAttemptArea = value; }
        public List<Shot> Shots { get => shots; set => shots = value; }
        public List<Signal> Signals { get => signals; set => signals = value; }
        public NetworkVectorController NetworkVectorController { get => networkVectorController; set => networkVectorController = value; }

        private bool ballInsideAttemptArea = false;

        private GameObject trajectoryObject;

        private List<Shot> shots = new List<Shot>();
        private List<Signal> signals = new List<Signal>();
        private NetworkVectorController networkVectorController;

        [Serializable]
        public class Shot
        {
            public enum ShotOutcome
            {
                NONE = 0,
                STOPPED = 1,
                MISS = 2,
                CLOSE = 3,
                MADE = 4
            }

            static int nextId;
            [SerializeField]
            private int id;
            [SerializeField]
            private int ownerId;
            [SerializeField]
            private float distance;
            [SerializeField]
            private SerializableVector3 velocity;
            [SerializeField]
            private int points;
            [SerializeField]
            private ShotOutcome shotResult;
            [SerializeField]
            private bool finished;
            [SerializeField]
            private int signalReceived;
            [SerializeField]
            private long timestamp;

            public float Distance { get => distance; set => distance = value; }
            public SerializableVector3 Velocity { get => velocity; set => velocity = value; }
            public ShotOutcome ShotResult { get => shotResult; set => shotResult = value; }
            public int Points { get => points; set => points = value; }
            public bool Finished { get => finished; set => finished = value; }
            public int Id { get => id; set => id = value; }
            public int OwnerId { get => ownerId; set => ownerId = value; }
            public long Timestamp { get => timestamp; set => timestamp = value; }
            public int SignalReceived { get => signalReceived; set => signalReceived = value; }

            public Shot(float distance, Vector3 velocity, int points = 0)
            {
                this.id = Interlocked.Increment(ref nextId);
                this.ownerId = PhotonNetwork.LocalPlayer.ActorNumber;
                //print("Owner id = " + this.ownerId);
                this.distance = distance;
                this.velocity = velocity;
                this.points = points;
                this.shotResult = ShotOutcome.NONE;
                this.finished = false;

                if (GameManager.instance.networkVectorController.currentSignals.Count > 0) {
                    this.signalReceived = GameManager.instance.networkVectorController.currentSignals[GameManager.instance.networkVectorController.currentSignals.Count - 1].signal_type;
                } else {
                    this.signalReceived = 0;
                }

                this.timestamp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            }

            public static object Deserialize(byte[] data)
            {
                MemoryStream memStream = new MemoryStream();
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(data, 0, data.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                object obj = (object) binForm.Deserialize(memStream);

                return obj;
            }

            public static byte[] Serialize(object customType)
            {
                var c = (Shot) customType;

                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, c);

                return ms.ToArray();
            }

            public override bool Equals(object obj)
            {
                //Check for null and compare run-time types.
                if ((obj == null) || !this.GetType().Equals(obj.GetType())) {
                    return false;
                } else {
                    Shot s = (Shot) obj;
                    return (id == s.Id && ownerId == s.OwnerId);
                }
            }

            public override int GetHashCode()
            {
                var hashCode = 1426122841;
                hashCode = hashCode * -1521134295 + id.GetHashCode();
                hashCode = hashCode * -1521134295 + ownerId.GetHashCode();
                return hashCode;
            }
        }

        [Serializable]
        public class ResumedShot : Shot
        {
            [SerializeField]
            private int resumedPoints;
            [SerializeField]
            private float resumedDistance;

            public float ResumedDistance { get => resumedDistance; set => resumedDistance = value; }
            public int ResumedPoints { get => resumedPoints; set => resumedPoints = value; }

            public ResumedShot(float distance, Vector3 velocity, int points = 0) : base(distance, velocity, points)
            {
                resumedPoints = 0;
                resumedDistance = 0;
            }

            public static object Deserialize(byte[] data)
            {
                MemoryStream memStream = new MemoryStream();
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(data, 0, data.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                object obj = (object) binForm.Deserialize(memStream);

                return obj;
            }

            public static byte[] Serialize(object customType)
            {
                var c = (ResumedShot) customType;

                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, c);

                return ms.ToArray();
            }
        }

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        void Start()
        {
            trajectoryObject = basketball.transform.Find("Trajectory").gameObject;
            settings = new PlayerSettings();
        }

        private void Update()
        {
            if (seeTrajectory && !trajectoryObject.activeSelf) {
                trajectoryObject.SetActive(true);
            } else if (!seeTrajectory && trajectoryObject.activeSelf) {
                trajectoryObject.SetActive(false);
            }
        }

        public void addShot(Vector3 position, Vector3 velocity)
        {
            float distance = Vector3.Distance(new Vector3(position.x, 0, position.z), new Vector3(hoopCenter.transform.position.x, 0, hoopCenter.transform.position.z));
            Shot newShot = new Shot(distance, velocity, distance > distance3Pts ? 3 : 2);

            if (BallInsideAttemptArea) {
                newShot.ShotResult = Shot.ShotOutcome.MISS;
            }

            shots.Add(newShot);
            StartCoroutine(finishAttempt(shots.Count-1));
        }

        public void addResumedShot(Vector3 position, Vector3 velocity)
        {
            float distance = Vector3.Distance(new Vector3(position.x, 0, position.z), new Vector3(hoopCenter.transform.position.x, 0, hoopCenter.transform.position.z));
            ResumedShot newShot = new ResumedShot(distance, velocity, distance > distance3Pts ? 3 : 2);

            if (BallInsideAttemptArea) {
                newShot.ShotResult = Shot.ShotOutcome.MISS;
            }

            newShot.ResumedPoints = shots[shots.Count - 1].Points;
            newShot.ResumedDistance = shots[shots.Count - 1].Distance;

            shots.Add(newShot);
            StartCoroutine(finishAttempt(shots.Count - 1));
            AnalyticsManager.instance.finishTimeControlUsage(velocity);
        }

        public void addAttempt()
        {
            if (!shots[shots.Count - 1].Finished && shots[shots.Count - 1].ShotResult <= Shot.ShotOutcome.MISS) {
                shots[shots.Count - 1].ShotResult = Shot.ShotOutcome.MISS;
            }
        }

        public IEnumerator finishAttempt(int index)
        {
            if (shots[index].Finished)
                yield return null;

            yield return new WaitForSeconds(5f);
            

            if (shots.Count > 0 && !shots[index].Finished) {
                shots[index].Finished = true;
                print("Finishing shot #" + (shots.Count - 1) + " with id " + shots[index].Id);
                if (!settings.isSinglePlayer && shots[index].OwnerId == PhotonNetwork.LocalPlayer.ActorNumber) {
                    //NetworkManager.instance.syncAllShots();
                    NetworkManager.instance.syncLastShot(shots[index]);
                }

                if (ExperimentManager.instance.experimentMode) {
                    ExperimentManager.instance.setNextPosition(shots[index]);
                }
                
            }
        }

        public void scoreAttempt()
        {
            print("trying to score");
            if (!shots[shots.Count - 1].Finished) {
                shots[shots.Count - 1].ShotResult = Shot.ShotOutcome.MADE;
                print("scored!");
                StartCoroutine(finishAttempt(shots.Count - 1));
                if (!settings.isSinglePlayer && shots[shots.Count - 1].OwnerId == PhotonNetwork.LocalPlayer.ActorNumber) {
                    NetworkManager.instance.addMadeShot();
                }
            }
        }

        public void closeAttempt()
        {
            if (!shots[shots.Count - 1].Finished) {
                shots[shots.Count - 1].ShotResult = Shot.ShotOutcome.CLOSE;
                //finishAttempt();
            }
        }

        public void stopAttempt()
        {
            /*while (TimeManager.instance.TimeStopped) {
                shots[shots.Count - 1].StopTokens.Push(true);
                print("Stopping shot #" + (shots.Count - 1));
                yield return new WaitForSeconds(2f);
            }*/

            shots[shots.Count - 1].ShotResult = Shot.ShotOutcome.STOPPED;
            shots[shots.Count - 1].Finished = true;

            if (!settings.isSinglePlayer && shots[shots.Count - 1].OwnerId == PhotonNetwork.LocalPlayer.ActorNumber) {
                NetworkManager.instance.syncLastShot(shots[shots.Count - 1]);
            }
            
        }

        public void syncShot(Shot networkShot)
        {
            bool found = false;

            for (int i = 0; i < shots.Count; i++) { 
                if (shots[i] == networkShot) {
                    //this shot substitutes all the current info to the one received
                    shots[i] = networkShot;
                    found = true;
                    return;
                }
            }

            if (!found) {
                shots.Add(networkShot);
            }
        }

        void OnApplicationQuit()
        {
            Debug.Log("Application ending after " + Time.time + " seconds");
            //JsonManager.instance.logShots(shots);
            //JsonManager.instance.logSignals(signals);

            AnalyticsManager.instance.parseCenas();
        }

    }
}