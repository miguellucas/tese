﻿using Photon.Pun;
using ExitGames.Client.Photon;
using UnityEngine;
using UnityEngine.SceneManagement;
using Painter.Utils;
using System.Collections.Generic;

namespace Painter.Communication
{
    [RequireComponent(typeof(PhotonView))]
    public class NetworkManager : MonoBehaviourPun
    {
        public static NetworkManager instance = null;

        public bool changeRolesAfterSuccessShot = false;
        [ConditionalField("changeRolesAfterSuccessShot", true, ComparisonType.Equals)]
        public int nShotsForChangeRoles = 1;

        public Hologram hologram;
        private BlinkController blinkController;

        public const int SIGNAL_CLOSE = 1;
        public const int SIGNAL_INTERMEDIATE = 2;
        public const int SIGNAL_FAR = 3;

        private int currentMadeShots = 0;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            PhotonPeer.RegisterType(typeof(GameManager.Shot), (byte) 'S', GameManager.Shot.Serialize, GameManager.Shot.Deserialize);
            PhotonPeer.RegisterType(typeof(GameManager.ResumedShot), (byte) 'R', GameManager.ResumedShot.Serialize, GameManager.ResumedShot.Deserialize);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                sendCloseSignal();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2)) {
                sendIntermediateSignal();
            }

            if (Input.GetKeyDown(KeyCode.Alpha3)) {
                sendFarSignal();
            }

            if (Input.GetKeyDown(KeyCode.C)) {
                sendChangeRole();
            }
        }

        public void sendCloseSignal()
        {
            print("Sending close signal");
            sendSignal(SIGNAL_CLOSE);
        }

        public void sendFarSignal()
        {
            print("Sending far signal");
            sendSignal(SIGNAL_FAR);
        }

        public void sendIntermediateSignal()
        {
            print("Sending intermediate signal");
            sendSignal(SIGNAL_INTERMEDIATE);
        }

        private void sendSignal(int signal)
        {
            // Run rpc method on all other users
            photonView.RPC("ReceiveSignal", RpcTarget.Others, signal);
        }

        [PunRPC]
        public void ReceiveSignal(int signal)
        {
            print("Received " + signal);

            if (hologram == null) {
                findHologram();
            }

            switch (signal) {
                case SIGNAL_CLOSE:
                    blinkController.startBlinkEffect(SIGNAL_CLOSE);
                    break;
                case SIGNAL_INTERMEDIATE:
                    blinkController.startBlinkEffect(SIGNAL_INTERMEDIATE);
                    break;
                case SIGNAL_FAR:
                    blinkController.startBlinkEffect(SIGNAL_FAR);
                    break;
                default:
                    print("UNKNOWN SIGNAL RECEIVED");
                    break;
            }
        }

        public void sendSetManipulator()
        {
            photonView.RPC("SetManipulator", RpcTarget.Others);
        }

        [PunRPC]
        private void SetManipulator()
        {
            GameManager.instance.settings.setManipulator();
        }

        public void sendChangeRole()
        {
            photonView.RPC("ChangeRole", RpcTarget.All);
        }

        [PunRPC]
        private void ChangeRole()
        {
            GameManager.instance.settings.changeRole();
        }

        public void sendHologramData(Vector3 velocity)
        {
            //Debug.Log("sending velocity: " + velocity);
            photonView.RPC("ReceiveHologramVelocity", RpcTarget.OthersBuffered, velocity);
        }

        [PunRPC]
        private void ReceiveHologramVelocity(Vector3 velocity)
        {
            //Debug.Log("Received velocity: " + velocity);
            GameManager.instance.hologramForce = velocity;
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            
        }

        public void addMadeShot()
        {
            if (changeRolesAfterSuccessShot) {
                currentMadeShots++;

                if (currentMadeShots >= nShotsForChangeRoles) {
                    sendChangeRole();
                    currentMadeShots = 0;
                }
            }
        }

        public void syncLastShot(GameManager.Shot shot)
        {
            if (shot.OwnerId == PhotonNetwork.LocalPlayer.ActorNumber) {
                photonView.RPC("receiveLastShot", RpcTarget.Others, shot);
            }
        }

        [PunRPC]
        private void receiveLastShot(GameManager.Shot shot)
        {
            GameManager.instance.syncShot(shot);
            print("NETWORK MANAGER: Syncing last shot #" + shot.Id + " from " + shot.OwnerId);
        }


        /*public void syncAllShots()
        {
            //convert the list to an array because Lists are not supported for RPC calls
            photonView.RPC("receiveAllShots", RpcTarget.Others, GameManager.instance.Shots.ToArray());
        }

        [PunRPC]
        private void receiveAllShots(GameManager.Shot[] shots)
        {
            //convert the array back to a List
            GameManager.instance.Shots = new List<GameManager.Shot>(shots);
            print("NETWORK MANAGER: Syncing shots");
        }*/

        public void sendChangeTimeState()
        {
            if (GameManager.instance.settings.canControlTime) {
                photonView.RPC("receiveTimeState", RpcTarget.Others, TimeManager.instance.TimeStopped);
            }
        }

        [PunRPC]
        private void receiveTimeState(bool newTimeState)
        {
            Debug.Log("Changing time state to " + newTimeState);
            TimeManager.instance.changeTimeState();
        }

        private void findHologram()
        {
            if (hologram == null) {
                GameObject obj = GameObject.Find("Basket Ball Hologram");
                if (obj != null) {
                    hologram = obj.GetComponent<Hologram>();
                }
            }

            if (hologram != null) {
                blinkController = hologram.GetComponent<BlinkController>();
                Debug.Log("NETWORK MANAGER: Hologram found. Assigning blink controller");
            } else {
                Debug.Log("NETWORK MANAGER: No hologram detected");
            }
        }
    }
}