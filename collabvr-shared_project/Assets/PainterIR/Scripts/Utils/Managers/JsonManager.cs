﻿
using Photon.Realtime;
using UnityEngine;
using Valve.VR;
using System;
using System.Collections.Generic;
using System.IO;

namespace Painter.Utils
{
    public class JsonManager : MonoBehaviour
    {

        public static JsonManager instance = null;

        public string jsonFileLocation;
        public string shotsLogFileLocation;
        public string signalsLogFileLocation;
        public TextAsset JsonFile;
        [Header("Gravity")]
        public float minGravity = 0;
        public float maxGravity = 50;
        public float defaultGravity = 9.8f;
        public float height = 3.15f;
        
        [Header("Wind")]
        public float minWindIntensity = 0f;
        public float maxWindIntensity = 20f;
        public float defaultWindIntensity = 1f;

        private JSONObject jsonObject;

        public JSONObject JSONObject { get => jsonObject; set => jsonObject = value; }

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            if (!Application.isEditor) {
                string json_path = Application.dataPath + jsonFileLocation;

                //Output the Game data path to the console
                Debug.Log("JSON MANAGER: dataPath : " + json_path);

                string fileText = "";

                try {
                    // Create an instance of StreamReader to read from a file.
                    // The using statement also closes the StreamReader.
                    using (StreamReader sr = new StreamReader(json_path)) {
                        string line;
                        // Read and display lines from the file until the end of
                        // the file is reached.
                        while ((line = sr.ReadLine()) != null) {
                            fileText += line + "\n";
                        }
                        Debug.Log("JSON MANAGER: Finished reading file");
                        Debug.Log(fileText);
                        Debug.Log(JsonFile.text);
                        loadJson(fileText);
                    }
                } catch (Exception e) {
                    // Let the user know what went wrong.
                    Debug.Log("The file could not be read:");
                    Debug.Log(e.Message);
                }


            } else {
                loadJson(JsonFile.text);
            }
        }

        void Start()
        {
            
        }

        public void loadJson(string text)
        {
            //JSONObject = new JSONObject(JsonFile.text);
            JSONObject = new JSONObject(text);
        }

        public void logShots(List<GameManager.Shot> shots)
        {
            string log_path = Application.dataPath + shotsLogFileLocation;
            // Write each directory name to a file.
            using (StreamWriter sw = new StreamWriter(log_path)) {
               
                foreach (GameManager.Shot shot in shots) {
                    string json = JsonUtility.ToJson(shot);
                    sw.WriteLine(json);
                }

                Debug.Log("logging to: " + log_path);
            }
        }

        public void logSignals(List<Signal> signals)
        {
            string log_path = Application.dataPath + signalsLogFileLocation;
            // Write each directory name to a file.
            using (StreamWriter sw = new StreamWriter(log_path)) {

                foreach (Signal signal in signals) {
                    string json = JsonUtility.ToJson(signal);
                    sw.WriteLine(json);
                }

                Debug.Log("logging to: " + log_path);
            }
        }
    }

}
