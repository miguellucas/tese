﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Painter.Communication;

namespace Painter.Utils
{
    public class TimeManager : MonoBehaviour
    {
        public static TimeManager instance = null;

        [Header("Pause objects")]
        public GameObject[] objectsToPause;

        [Header("Controls")]
        public KeyCode keyboardTimeControl;
        public SteamVR_Action_Boolean timeBtn;

        private Dictionary<GameObject, PausedVelocity> pausedObjects = new Dictionary<GameObject, PausedVelocity>(new GameObjectEqualityComparer());

        private bool timeStopped = false;

        public bool TimeStopped { get => timeStopped; set => timeStopped = value; }
        public Dictionary<GameObject, PausedVelocity> PausedObjects { get => pausedObjects; set => pausedObjects = value; }

        public struct PausedVelocity
        {
            public int objId;
            public Vector3 velocity;
            public Vector3 angularVelocity;
            public Vector3 position;

            public PausedVelocity(int objId, Vector3 velocity, Vector3 angularVelocity, Vector3 position)
            {
                this.objId = objId;
                this.velocity = velocity;
                this.angularVelocity = angularVelocity;
                this.position = position;
            }
        }


        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(keyboardTimeControl) || (timeBtn != null && timeBtn.stateDown)) {
                if (GameManager.instance.settings.canControlTime) {
                    changeTimeState();
                    
                }
                
                /*if (timeStopped) {
                    Debug.Log("time normal");
                    resumeTime();
                } else {
                    pauseTime();
                }

                timeStopped = !timeStopped;*/
            }
        }

        public void changeTimeState()
        {
            if (timeStopped) {
                resumeTime();
            } else {
                pauseTime();
            }

            timeStopped = !timeStopped;
            NetworkManager.instance.sendChangeTimeState();
        }

        public void changeTimeState(bool newTimeState)
        {
            if (!newTimeState) {
                resumeTime();
            } else {
                pauseTime();
            }

            timeStopped = newTimeState;
        }

        public void pauseTime()
        {
            Debug.Log("time paused");
            if (pausedObjects.Count == 0) {
                foreach (GameObject obj in objectsToPause) {
                    Rigidbody rigidbody = obj.GetComponent<Rigidbody>();
                    PausedVelocity pausedVelocity = new PausedVelocity(obj.GetInstanceID(), rigidbody.velocity, rigidbody.angularVelocity, obj.transform.position);

                    AnalyticsManager.instance.addTimeControlUsage(new AnalyticsTimeControl(Vector3.Distance(obj.transform.position, GameManager.instance.hoopCenter.transform.position), GameManager.instance.hologramForce));
                    pausedObjects.Add(obj, pausedVelocity);

                    rigidbody.velocity = Vector3.zero;
                    rigidbody.angularVelocity = Vector3.zero;
                    rigidbody.isKinematic = true;
                }
            }

            GameManager.instance.stopAttempt();
            
        }

        /// <summary>
        /// Restarts time ignoring all paused objects states, only removing kinematic status from rigidbodies
        /// </summary>
        public void restartTime()
        {
            Debug.Log("restarting time");
            foreach (KeyValuePair<GameObject, PausedVelocity> pair in pausedObjects) {
                Rigidbody rigidbody = pair.Key.GetComponent<Rigidbody>();
                rigidbody.isKinematic = false;
            }

            pausedObjects.Clear();
            timeStopped = false;
            NetworkManager.instance.sendChangeTimeState();
        }

        /// <summary>
        /// Restarts time considering all paused objects states
        /// </summary>
        public void resumeTime()
        {
            Debug.Log("resuming time");
            Vector3 lastPos = Vector3.zero;
            Vector3 lastVel = Vector3.zero;
            foreach (KeyValuePair<GameObject, PausedVelocity> pair in pausedObjects) {

                Rigidbody rigidbody = pair.Key.GetComponent<Rigidbody>();
                rigidbody.isKinematic = false;
                rigidbody.velocity = pair.Value.velocity;
                rigidbody.angularVelocity = pair.Value.angularVelocity;

                lastPos = pair.Value.position;
                lastVel = pair.Value.velocity;
            }

            pausedObjects.Clear();
            //timeStopped = false;

            GameManager.instance.addResumedShot(lastPos, lastVel);
            //NetworkManager.instance.sendChangeTimeState();
        }

        public class GameObjectEqualityComparer : IEqualityComparer<GameObject>
        {
            public int GetHashCode(GameObject obj) { return obj.GetInstanceID().GetHashCode(); }
            public bool Equals(GameObject obj1, GameObject obj2) { return obj1.GetInstanceID() == obj2.GetInstanceID(); }
        }
    }
}