﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Painter.Utils
{
    public class AnalyticsManager : MonoBehaviour
    {
        public static AnalyticsManager instance = null;

        public List<Grab> grabs = new List<Grab>();
        public List<AnalyticsShot> shots = new List<AnalyticsShot>();
        public List<Signal> signals = new List<Signal>();
        public List<AnalyticsTimeControl> timeControls = new List<AnalyticsTimeControl>();

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void addGrab(Grab grab)
        {
            if (grabs.Count > 1 && !grabs[grabs.Count - 1].Finished) {
                return;
            }

            grabs.Add(grab);
        }

        public void finishLastGrab(Vector3 velocity, long timestamp)
        {
            grabs[grabs.Count - 1].finishGrab(velocity, timestamp);
        }

        public void addSignal(Signal signal)
        {
            signals.Add(signal);
        }

        public void addTimeControlUsage(AnalyticsTimeControl analyticsTimeControl)
        {
            timeControls.Add(analyticsTimeControl);
        }

        public void finishTimeControlUsage(Vector3 velocity)
        {
            timeControls[timeControls.Count - 1].finishTimeControl(velocity);
        }

        public void addShot(AnalyticsShot shot)
        {
            if (shots.Count > 0) {
                shot.Time_since_last_shot = shot.Timestamp - shots[shots.Count - 1].Timestamp;
            } else {
                shot.Time_since_last_shot = 0;
            }

            shot.Grabs = new List<Grab>(grabs);
            grabs.Clear();

            shot.Signals = new List<Signal>(signals);
            signals.Clear();

            shot.TimeControlUsages = new List<AnalyticsTimeControl>(timeControls);
            timeControls.Clear();

            shot.analyze();

            shots.Add(shot);
        }

        public void parseCenas()
        {
            //List<Shot> list = new List<dynamic>();
            int lastTimestamp = 0;
            /*foreach (AnalyticsShot shot in shots) {
                /*lastTimestamp = pos.Shot.Timestamp - lastTimestamp;

                var signal = new {
                    position = 2,
                    velocity = 3,
                    signal_type = 1
                };
                var shot = new {
                    id = pos.Shot.Id,
                    distance = pos.Shot.Distance,
                    velocity_vectorial = pos.Shot.Velocity,
                    velocity_magnitude = pos.Shot.Velocity.toVector3().magnitude,
                    shot_result = pos.Shot.ShotResult,
                    timestamp = pos.Shot.Timestamp,
                    time_since_last_shot = lastTimestamp,
                    category = pos.Category
                };*/

                

               /* list.Add(shot);
            }*/

            logShots();
                
        }

        public void logShots()
        {
            string log_path = Application.dataPath + "/Resources/shots.json";
            // Write each directory name to a file.
            using (StreamWriter sw = new StreamWriter(log_path)) {

                foreach (AnalyticsShot shot in shots) {
                    string json = Valve.Newtonsoft.Json.JsonConvert.SerializeObject(shot, Valve.Newtonsoft.Json.Formatting.Indented);
                    sw.WriteLine(json);
                }

                Debug.Log("logging to: " + log_path);
            }
        }

        public void testLogGrab()
        {
            string log_path = Application.dataPath + "/Resources/grabs.json";
            // Write each directory name to a file.
            using (StreamWriter sw = new StreamWriter(log_path)) {

                foreach (Grab grab in grabs) {
                    string json = Valve.Newtonsoft.Json.JsonConvert.SerializeObject(grab);
                    sw.WriteLine(json);
                }

                Debug.Log("logging to: " + log_path);
            }
        }
    }
}