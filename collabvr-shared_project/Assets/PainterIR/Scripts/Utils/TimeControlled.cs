﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControlled : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U)) {
            Time.timeScale = 0;
            Debug.Log("time 0");
        }
        if (Input.GetKeyDown(KeyCode.O)) {
            Debug.Log("time normal");
            Time.timeScale = 1f;
        }
    }
}
