﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class HighlightController : MonoBehaviour
    {
        #region Singleton

        private static HighlightController instance;
        public static HighlightController Instance
        {
            get
            {
                return instance;
            }
        }

        public HighlightController()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>HighlightController</b>: Multiple instances of HighlightController not supported.");
                return;
            }
#endif

            instance = this;

            m_listHighlightables = new LinkedList<Transform>();
        }

        #endregion
        
        private LinkedList<Transform> m_listHighlightables;

        public HighlightBehaviour[] highlightBehaviours;


        private void Start()
        {
            StartCoroutine(PeriodicCheck());
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }


        public void CallAtentionTo(Transform me, bool isPriority = false)
        {
            var containsMe = m_listHighlightables.Contains(me);
            var first = m_listHighlightables.Count > 0 ? m_listHighlightables.First.Value : null;

            // Don't add me if I'm already Highlighted
            if (containsMe  &&
                first == me)
                return;

            if (isPriority)
            {
                // Remove me to add me at first place
                if (containsMe)
                    m_listHighlightables.Remove(me);
                m_listHighlightables.AddFirst(me);
            }
            // Only add me last if I'm not already there
            // and I'm not priority
            else if(!containsMe)
            {
                m_listHighlightables.AddLast(me);
            }

            if(first != m_listHighlightables.First.Value)
            {
                UpdateEffect();
            }
        }

        public void RemoveAttentionTo(Transform me)
        {
            m_listHighlightables.Remove(me);
            UpdateEffect();
        }

        private void UpdateEffect()
        {
            if (m_listHighlightables.Count <= 0)
            {
                IndicatorController.Instance.StopHighlightEffect();
                foreach(var hb in highlightBehaviours)
                {
                    hb.StopHighlightEffect();
                }
            }
            else
            {
                IndicatorController.Instance.StartHighlightEffect(m_listHighlightables.First.Value);
                foreach (var hb in highlightBehaviours)
                {
                    hb.StartHighlightEffect(m_listHighlightables.First.Value);
                }
            }
        }


        private IEnumerator PeriodicCheck()
        {
            while(true)
            {
                yield return new WaitForSeconds(1);

                if(m_listHighlightables.Count > 0 && m_listHighlightables.First == null)
                {
                    RemoveAttentionTo(null);
                }
            }
        }
    }
}
