﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Painter.Utils
{
    public class ScoreboardController : MonoBehaviour
    {
        public float switchModeTime = 10f;
        public int nTableRecords = 5;

        public Color colorMissionNotCompleted;
        public Color colorMissionCompleted;

        private GameObject pointsSection;
        private Text pointsText;
        private Text attemptsText;
        private Text percentageText;
        private Text distanceText;
        private Text velocityText;

        private GameObject tableSection;
        private Text titleText;
        private Text tableText;
        private string TITLE_TABLE;

        private GameObject missionsSection;
        private Text missionsText;

        private GameObject missionsSection2;
        private Text missionsText2;

        private float timer = 0f;

        private List<GameObject> sections;
        private int currentSection = 0;

        // Start is called before the first frame update
        void Start()
        {
            Transform statsObject = transform.Find("Canvas").Find("Stats");
            pointsSection = statsObject.Find("Points").gameObject;
            tableSection = statsObject.Find("Table").gameObject;
            missionsSection = statsObject.Find("Missions1").gameObject;
            missionsSection2 = statsObject.Find("Missions2").gameObject;

            pointsText = pointsSection.transform.Find("PointsText").GetComponent<Text>();
            attemptsText = pointsSection.transform.Find("AttemptsText").GetComponent<Text>();
            percentageText = pointsSection.transform.Find("PercentageText").GetComponent<Text>();
            distanceText = pointsSection.transform.Find("DistanceText").GetComponent<Text>();
            velocityText = pointsSection.transform.Find("VelocityText").GetComponent<Text>();

            titleText = tableSection.transform.Find("Title").GetComponent<Text>();
            tableText = tableSection.transform.Find("TableText").GetComponent<Text>();

            missionsText = missionsSection.transform.Find("MissionsText").GetComponent<Text>();

            missionsText2 = missionsSection2.transform.Find("MissionsText").GetComponent<Text>();

            sections = new List<GameObject> { pointsSection, tableSection, missionsSection, missionsSection2 };
        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;

            if (timer >= switchModeTime) {
                timer = 0;
                switchScoreboardMode();
                updateScoreboard();
                
            }
        }

        public void updateScoreboard()
        {
            int totalPoints = 0;
            int totalAttempts = 0;
            int totalShotsMade = 0;

            

            foreach(GameManager.Shot shot in GameManager.instance.Shots) {
                if (shot.ShotResult > GameManager.Shot.ShotOutcome.STOPPED) {
                    totalAttempts += 1;
                }

                if (shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                    int shotPoints;

                    if (shot is GameManager.ResumedShot) {
                        shotPoints = ((GameManager.ResumedShot) shot).ResumedPoints;
                    } else {
                        shotPoints = shot.Points;
                    }
                    totalPoints += shotPoints;
                    totalShotsMade += 1;
                }
            }

            updatePointsSection(totalPoints, totalAttempts, totalShotsMade);
            if (GameManager.instance.Shots.Count > 0) {
                updateLastAttemptSection(GameManager.instance.Shots[GameManager.instance.Shots.Count - 1]);
            }
            
            updateTableSection();
            updateMissionsSection();
        }

        public void updatePointsSection(int totalPoints, int totalAttempts, int totalShotsMade)
        {
            double percentage = Math.Round((double) totalShotsMade / totalAttempts, 2) * 100;
            if (Double.IsNaN(percentage)) {
                percentage = 0;
            }
            pointsText.text = totalPoints + " PTS";
            attemptsText.text = totalShotsMade + "/" + totalAttempts + " Attempts";
            percentageText.text = percentage + " %";

        } 

        public void updateLastAttemptSection(GameManager.Shot lastShot)
        {
            double distance;
            double velocity;

            if (lastShot is GameManager.ResumedShot) {
                distance = ((GameManager.ResumedShot) lastShot).ResumedDistance;
            } else {
                distance = lastShot.Distance;
            }

            velocity = lastShot.Velocity.toVector3().magnitude;

            distanceText.text = "Dist: " + Math.Round(distance, 2) + " m";
            velocityText.text = "Vel: " + Math.Round(velocity, 2) + " m/s";
        }

        public void updateTableSection()
        {
            List<GameManager.Shot> clonedList = new List<GameManager.Shot>(GameManager.instance.Shots);
            clonedList.Reverse();
            string text = "";

            int index = 0;
            foreach(GameManager.Shot shot in clonedList) {
                if (shot.Finished && shot.ShotResult > GameManager.Shot.ShotOutcome.STOPPED) {
                    index++;
                    text += "#" + index + " - ";

                    switch (shot.ShotResult) {
                        case GameManager.Shot.ShotOutcome.MISS:
                            text += "<color=red>" + shot.ShotResult + "</color>";
                            break;
                        case GameManager.Shot.ShotOutcome.CLOSE:
                            text += "<color=orange>" + shot.ShotResult + "</color>";
                            break;
                        case GameManager.Shot.ShotOutcome.MADE:
                            text += "<color=green>" + shot.ShotResult + "</color>";
                            break;
                    }

                    double distance;
                    if (shot is GameManager.ResumedShot) {
                        distance = ((GameManager.ResumedShot) shot).ResumedDistance;
                    } else {
                        distance = shot.Distance;
                    }

                    text += " - " + Math.Round(distance, 2) + " m - " + Math.Round(shot.Velocity.toVector3().magnitude, 2) + " m/s";
                    text += "\n"; //add a newline
                }

                if (index >= nTableRecords) {
                    break;
                }
            }

            tableText.text = text;
        }

        public void updateMissionsSection()
        {
            int shotsAttemptedDistance0_3 = 0;
            int shotsMadeDistance0_3 = 0;
            int shotsAttemptedDistance3_6 = 0;
            int shotsMadeDistance3_6 = 0;
            int shotsAttemptedDistance6_inf = 0;
            int shotsMadeDistance6_inf = 0;
            int shotsAttemptedVelocity0_15 = 0;
            int shotsMadeVelocity0_15 = 0;
            int shotsAttemptedVelocity15_inf = 0;
            int shotsMadeVelocity15_inf = 0;
            int shotsAttemptedTimeControl = 0;
            int shotsMadeTimeControl = 0;
            int shotsAttemptedCollab3_6 = 0;
            int shotsMadeCollab3_6 = 0;
            int shotsAttemptedCollab6_inf = 0;
            int shotsMadeCollab6_inf = 0;
            int shotsAttemptedCollabTimeControl = 0;
            int shotsMadeCollabTimeControl = 0;

            foreach (ExperimentPosition experimentPosition in ExperimentManager.instance.experimentPositions) {
                switch (experimentPosition.Category) {
                    case ExperimentPosition.PositionCategory.Distance_0_3:
                        shotsAttemptedDistance0_3++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeDistance0_3++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Distance_3_6:
                        shotsAttemptedDistance3_6++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeDistance3_6++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Distance_6_inf:
                        shotsAttemptedDistance6_inf++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeDistance6_inf++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Velocity_0_15:
                        shotsAttemptedVelocity0_15++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeVelocity0_15++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Velocity_15_inf:
                        shotsAttemptedVelocity15_inf++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeVelocity15_inf++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Time_Control:
                        shotsAttemptedTimeControl++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeTimeControl++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Collab_3_6:
                        shotsAttemptedCollab3_6++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeCollab3_6++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Collab_6_inf:
                        shotsAttemptedCollab6_inf++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeCollab6_inf++;
                        }
                        break;
                    case ExperimentPosition.PositionCategory.Collab_Time_Control:
                        shotsAttemptedCollabTimeControl++;
                        if (experimentPosition.Shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                            shotsMadeCollabTimeControl++;
                        }
                        break;

                }
                
            }

            string text = "";
            string text2 = "";

            if (shotsAttemptedDistance0_3 >= ExperimentManager.instance.shootingPositions0_3m.Length) {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text += "1. Attempt " + ExperimentManager.instance.shootingPositions0_3m.Length +" shots at less than 3 m (" + shotsMadeDistance0_3 + "/" + shotsAttemptedDistance0_3 +")</color>\t\n";

            if (shotsAttemptedDistance3_6 >= ExperimentManager.instance.shootingPositions3_6m.Length) {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text += "2. Attempt " + ExperimentManager.instance.shootingPositions3_6m.Length + " shots between 3 and 6 m (" + shotsMadeDistance3_6 + "/" + shotsAttemptedDistance3_6 + ")</color>\t\n";

            if (shotsAttemptedDistance6_inf >= ExperimentManager.instance.shootingPositions6_infm.Length) {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text += "3. Attempt " + ExperimentManager.instance.shootingPositions6_infm.Length + " shots at more than 6 m (" + shotsMadeDistance6_inf + "/" + shotsAttemptedDistance6_inf + ")</color>\t\n";

            if (shotsAttemptedVelocity0_15 >= ExperimentManager.instance.shootingPositions0_15v.Length) {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text += "4. Attempt " + ExperimentManager.instance.shootingPositions0_15v.Length + " shots at less than 15 m/s (" + shotsMadeVelocity0_15 + "/" + shotsAttemptedVelocity0_15 + ")</color>\t\n";

            if (shotsAttemptedVelocity15_inf >= ExperimentManager.instance.shootingPositions15_infv.Length) {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text += "5. Attempt " + ExperimentManager.instance.shootingPositions0_15v.Length + " shots at more than 15 m/s (" + shotsMadeVelocity15_inf + "/" + shotsAttemptedVelocity15_inf + ")</color>\t\n";

            missionsText.text = text;


            if (shotsAttemptedTimeControl >= ExperimentManager.instance.shootingPositionsTimeControl.Length) {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text2 += "6. Attempt " + ExperimentManager.instance.shootingPositionsTimeControl.Length + " shots using time control (" + shotsMadeTimeControl + "/" + shotsAttemptedTimeControl + ")</color>\t\n";

            if (shotsAttemptedCollab3_6 >= ExperimentManager.instance.shootingPositions3_6m_collab.Length) {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text2 += "7. Attempt " + ExperimentManager.instance.shootingPositions3_6m_collab.Length + " shots between 3 and 6 m after receiving your partner's signal (" + shotsMadeCollab3_6 + "/" + shotsAttemptedCollab3_6 + ")</color>\n\n";

            if (shotsAttemptedCollab6_inf >= ExperimentManager.instance.shootingPositions6_infm_collab.Length) {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text2 += "8. Attempt " + ExperimentManager.instance.shootingPositions6_infm_collab.Length + " shots at more than 6 m after receiving your partner's signal (" + shotsMadeCollab6_inf + "/" + shotsAttemptedCollab6_inf + ")</color>\n\n";

            if (shotsAttemptedCollabTimeControl >= ExperimentManager.instance.shootingPositionsTimeControl_collab.Length) {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionCompleted) + ">";
            } else {
                text2 += "<color=#" + ColorUtility.ToHtmlStringRGB(colorMissionNotCompleted) + ">";
            }
            text2 += "9. Attempt " + ExperimentManager.instance.shootingPositionsTimeControl_collab.Length + " shots using time control after receiving your partner's signal (" + shotsMadeCollabTimeControl + "/" + shotsAttemptedCollabTimeControl + ")</color>\n";

            missionsText2.text = text2;
        }

        /*public void updateMissionsSection()
        {
            int totalPoints = 0;
            int totalAttempts = 0;
            int totalShotsMade = 0;
            int total3Pts = 0;
            int shotsMoreThan20Vel = 0;
            int shotsLessThan5Vel = 0;
            int shotsOver10m = 0;

            foreach (GameManager.Shot shot in GameManager.instance.Shots) {
                if (shot.ShotResult > GameManager.Shot.ShotOutcome.STOPPED) {
                    totalAttempts += 1;
                }

                if (shot.ShotResult == GameManager.Shot.ShotOutcome.MADE) {
                    int shotPoints;

                    if (shot is GameManager.ResumedShot) {
                        shotPoints = ((GameManager.ResumedShot) shot).ResumedPoints;
                    } else {
                        shotPoints = shot.Points;
                    }

                    totalPoints += shotPoints;

                    totalShotsMade += 1;

                    if (shotPoints == 3) {
                        total3Pts++;
                    }

                    if (shot.Velocity.toVector3().magnitude >= 20) {
                        shotsMoreThan20Vel++;
                    }

                    if (shot.Velocity.toVector3().magnitude <= 5) {
                        shotsLessThan5Vel++;
                    }

                    if (shot.Distance >= 10) {
                        shotsOver10m++;
                    }
                }
            }

            string text = "";

            if (totalAttempts >= 1) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "1. Attempt 1 shot (" + totalAttempts + "/1)</color>\n";

            if (totalShotsMade >= 2) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "2. Make 2 shots (" + totalShotsMade + "/2)</color>\n";

            if (shotsLessThan5Vel >= 1) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "3. Hit a shot with less than 5 m/s (" + shotsLessThan5Vel + "/1)</color>\n";

            if (shotsOver10m >= 1) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "4. Hit a shot at a minimum of 10 m (" + shotsOver10m + "/1)</color>\n";

            //TODO
            if (!GameManager.instance.settings.isSinglePlayer) {
                if (shotsMoreThan20Vel >= 1) {
                    text += "<color=green>";
                } else {
                    text += "<color=red>";
                }

                text += "5. Hit a shot after receiving a green signal (" + shotsMoreThan20Vel + "/1)</color>\n";
            }

            /*if (total3Pts >= 3) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "2. Hit 3 3-Point shots (" + total3Pts + "/3)</color>\n";

            if (totalPoints >= 2) {
                text += "<color=green>";
            } else {
                text += "<color=red>";
            }
            text += "5. Score 2 points (" + totalPoints + "/2)</color>\n";*/

        /*missionsText.text = text;
    }*/

        public void switchScoreboardMode()
        {
            TITLE_TABLE = "Last " + nTableRecords;
            titleText.text = TITLE_TABLE;

            currentSection++;
            if (currentSection >= sections.Count) {
                currentSection = 0;
            }

            for (int i = 0; i < sections.Count; i++) {
                sections[i].SetActive(currentSection == i);
            }
            /*pointsSection.SetActive(!pointsSection.activeSelf);
            lastAttemptSection.SetActive(!lastAttemptSection.activeSelf);
            tableSection.SetActive(!tableSection.activeSelf);

            if (pointsSection.activeSelf) {
                titleText.text = TITLE_POINTS;
            } else {
                titleText.text = TITLE_TABLE;
            }*/
        }
    }
}