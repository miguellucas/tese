﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(CharacterController))]
public class TouchPadLocomotion : MonoBehaviour
{
    [SerializeField]
    private float speed = 2.0f;

    [SerializeField]
    private SteamVR_Action_Boolean movePress = null;
    [SerializeField]
    private SteamVR_Action_Vector2 moveValue = null;

    private CharacterController characterController = null;
    private Transform cameraRig = null;
    private Transform head = null;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        cameraRig = SteamVR_Render.Top().origin;
        head = SteamVR_Render.Top().head;
    }

    // Update is called once per frame
    void Update()
    {
        //handleHead();
        calculateMovement();
        handleHeight();
    }

    private void handleHead()
    {
        Vector3 oldPosition = cameraRig.position;
        Quaternion oldRotation = cameraRig.rotation;

        transform.eulerAngles = new Vector3(0, head.rotation.eulerAngles.y, 0);

        cameraRig.position = oldPosition;
        cameraRig.rotation = oldRotation;
    }

    private void calculateMovement()
    {
        //get movement orientation
        Vector3 orientationEuler = new Vector3(0, head.eulerAngles.y, 0);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;

        //if the touchpad value is not 0 and the the touchpad is not pressed
        if (moveValue.axis != Vector2.zero && !movePress.state) {
            //get axis values to vector3
            Vector3 touchpadValues = new Vector3(moveValue.axis.x, 0, moveValue.axis.y);
            //calculate orientation
            movement += orientation * (speed * touchpadValues) * Time.deltaTime;
        }

        //apply movement
        characterController.Move(movement);
    }

    private void handleHeight()
    {
        //get the head height in local space
        float headHeight = Mathf.Clamp(head.localPosition.y, 1, 2);
        characterController.height = headHeight;

        //cut the height in half
        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;

        newCenter.x = head.localPosition.x;
        newCenter.z = head.localPosition.z;

        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0) * newCenter;

        //apply
        characterController.center = newCenter;
    }
}
