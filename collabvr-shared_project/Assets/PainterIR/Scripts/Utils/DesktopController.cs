﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Painter.Utils;
using Painter.Communication;

namespace Painter.Core
{
    public class DesktopController : MonoBehaviour
    {

        private Hologram hologram;
        private GameObject pivot;

        // Start is called before the first frame update
        void Start()
        {
            hologram = transform.Find("Tools").Find("Basket Ball Hologram").GetComponent<Hologram>();
            pivot = hologram.transform.Find("Arrow").Find("Cone").Find("Pivot").gameObject;
        }

        // Update is called once per frame
        void Update()
        {
            /*if (Input.GetKeyDown(KeyCode.C)) {
                NetworkManager.instance.sendChangeRole();
            }*/

            if (Input.GetKeyDown(KeyCode.P)) {

                Vector3 point = pivot.transform.position + new Vector3(0.5f, 0.5f, 0.5f);
                hologram.processScaleArrow(point);
            }
        }
    }
}