﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class WindAffected : MonoBehaviour
    {
        [Tooltip("Multiply the wind force on this specific object by this factor")]
        [SerializeField]
        private float windFactor = 1;

        private bool inWindArea = false;
        private Rigidbody rigidbody;

        public float WindFactor { get => windFactor; set => windFactor = value; }

        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if (inWindArea) {
                float force = Random.Range(0, GameManager.instance.WindArea.Intensity);
                Vector3 windForce = GameManager.instance.WindArea.Direction * force * WindFactor;
                rigidbody.AddForce(windForce);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "WindArea") {
                inWindArea = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "WindArea") {
                inWindArea = false;
            }
        }
    }
}