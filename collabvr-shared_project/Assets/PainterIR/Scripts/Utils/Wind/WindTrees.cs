﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class WindTrees : MonoBehaviour
    {
        private ParticleSystem particleSystem;


        private void Awake()
        {
            particleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
        }

        private void Start()
        {
            //change speed of the leaves according to wind intensity
            var main = particleSystem.main;
            main.startSpeedMultiplier *= GameManager.instance.WindArea.Intensity;

            //change the number of leaves generated according to wind intensity
            var emission = particleSystem.emission;
            emission.rateOverTime = GameManager.instance.WindArea.Intensity * GameManager.instance.WindArea.IntensityMultiplier;

            //rotate particle emission towards wind direction
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, GameManager.instance.WindArea.Direction, Mathf.PI, 0);
            particleSystem.transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }
}