﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class WindArea : MonoBehaviour
    {
        [SerializeField]
        private float intensity = 0;
        [SerializeField]
        private Vector3 direction = new Vector3(0, 0, 0);
        [SerializeField]
        private float intensityMultiplier = 0.2f;

        private WindZone windZone;

        public float Intensity { get => intensity; set => intensity = value; }
        public Vector3 Direction { get => direction; set => direction = value; }
        public float IntensityMultiplier { get => intensityMultiplier; set => intensityMultiplier = value; }

        private void Awake()
        {
            windZone = GetComponent<WindZone>();
            
        }

        private void Start()
        {
            //change windzone turbulence for tree shaking effect
            windZone.windTurbulence = GameManager.instance.WindArea.intensity * IntensityMultiplier;
            //normalize the direction vector
            Vector3 direction = GameManager.instance.WindArea.Direction;
            direction.Normalize();
            Direction = direction;
        }
    }
}