﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Painter.Utils;
using Painter.Communication;

namespace Painter.Utils
{
    public class HighlightTransparencyController : HighlightBehaviour
    {
        private HashSet<GameObject> m_walls;

        [Header("Transparency Effect Parameters")]
        public Material transparentMaterial;
        public LayerMask layersForTransparentObjects;

        public override void StartHighlightEffect(Transform position)
        {
            m_walls = new HashSet<GameObject>(GetAllWalls());
            foreach (var obj in m_walls)
            {
                // Add the transparent highlight script to the object
                // if it doesnt have one
                var script = obj.GetComponent<HighlightTransparent>();
                if (script == null)
                {
                    script = obj.AddComponent<HighlightTransparent>();
                }

                // Set script variables and enable transparency
                script.highlight = position;
                script.transparentMaterial = transparentMaterial;
                script.SetTransparent(true);
            };
        }

        public override void StopHighlightEffect()
        {
            RevertAllObjects();
        }

        
        #region Transparent Effect methods
        
        private GameObject[] GetAllWalls()
        {
            var objects = FindObjectsOfType<GameObject>();
            var walls = new List<GameObject>();
            foreach (var obj in objects)
            {
                if ((layersForTransparentObjects | 1 << obj.layer) == layersForTransparentObjects)
                    walls.Add(obj);
            }
            return walls.ToArray();
        }
        
        private void RevertObject(HighlightTransparent transparentObject)
        {
            transparentObject.SetTransparent(false);
        }

        private void RemoveObject(HighlightTransparent transparentObject)
        {
            RevertObject(transparentObject);
        }

        private void RevertAllObjects()
        {
            foreach (var obj in m_walls)
            {
                var script = obj.GetComponent<HighlightTransparent>();
                RevertObject(script);
            };
        }

        #endregion
    }
}
