﻿using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Painter.Communication;

namespace Painter.Utils
{
    public class SpringButton : MonoBehaviour
    {
        [System.Serializable]
        public class ButtonEvent : UnityEvent { }

        public float pressLength;
        public bool pressed;

        public bool freezeLocalPositionX;
        public bool freezeLocalPositionY;
        public bool freezeLocalPositionZ;

        public ButtonEvent downEvent;

        public AudioClip clickSound;

        private Vector3 startPos;
        private Rigidbody rb;
        private AudioSource audioSource;

        void Start()
        {
            startPos = transform.localPosition;
            rb = GetComponent<Rigidbody>();
            audioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            // If our distance is greater than what we specified as a press
            // set it to our max distance and register a press if we haven't already

            float distance = Mathf.Abs(transform.localPosition.z - startPos.z);
            if (distance >= pressLength) {
                // Prevent the button from going past the pressLength
                transform.localPosition = new Vector3(0, 0, startPos.z + pressLength);
                if (!pressed) {
                    pressed = true;
                    // If we have an event, invoke it
                    downEvent?.Invoke();
                    if (clickSound != null) {
                        audioSource.PlayOneShot(clickSound);
                    }
                }
            } else {
                // If we aren't all the way down, reset our press
                pressed = false;
            }
            // Prevent button from springing back up past its original position
            if (transform.localPosition.z < startPos.z) {
                transform.localPosition = new Vector3(0, 0, startPos.z);
            }

            /*Vector3 localVelocity = transform.InverseTransformDirection(rb.velocity);
            localVelocity.x = freezeLocalPositionX ? 0 : localVelocity.x;
            localVelocity.y = freezeLocalPositionY ? 0 : localVelocity.y;
            localVelocity.z = freezeLocalPositionZ ? 0 : localVelocity.z;

            rb.velocity = transform.TransformDirection(localVelocity);*/
            transform.localPosition = new Vector3(freezeLocalPositionX ? 0 : transform.localPosition.x, freezeLocalPositionY ? 0 : transform.localPosition.y, freezeLocalPositionZ ? 0 : transform.localPosition.z);
        }

        public void sendCloseSignal()
        {
            print("Sending close signal");
            NetworkManager.instance.sendCloseSignal();
        }

        public void sendFarSignal()
        {
            print("Sending far signal");
            NetworkManager.instance.sendFarSignal();
        }

        public void sendIntermediateSignal()
        {
            print("Sending intermediate signal");
            NetworkManager.instance.sendIntermediateSignal();
        }
    }
}