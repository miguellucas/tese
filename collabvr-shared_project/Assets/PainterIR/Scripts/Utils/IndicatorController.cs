﻿using Painter.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Painter.Utils
{
    public class IndicatorController : HighlightBehaviour
    {
        private static IndicatorController instance;
        public static IndicatorController Instance
        {
            get
            {
                return instance;
            }
        }

        public IndicatorController()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>IndicatorController</b>: Multiple instances of IndicatorController not supported.");
                return;
            }
#endif

            instance = this;
        }

        public MonoBehaviour arrowRenderer;

        public float radius = 300.0f;

        [Range(0.05f, 1.0f)]
        public float fovRatio = 0.8f;
        private Transform m_highlightPoint;
        private bool m_isLooking = false;

        private void Awake()
        {
            arrowRenderer.enabled = m_isLooking;
        }

        private void Update()
        {
            if (m_highlightPoint == null && m_isLooking)
            {
                StopHighlightEffect();
                return;
            }

            if (m_isLooking)
            {
                UpdateIndicator(GetCamera(), m_highlightPoint);
            }
        }

        private void UpdateIndicator(Camera cam, Transform look)
        {
            // Determine if that falls withing the user's FOV
            var show = !IsWithingFOV(cam, look.position);

            // If within FOV disable the image and do nothing more until next frame
            if (!show)
            {
                arrowRenderer.enabled = false;
                return;
            }

            // If out of FOV determine the direction the user has to look at
            // Which is going from 3D coordinates to 2D coordinates
            arrowRenderer.enabled = true;

            // Initially try the vector that goes from the camera to the position
            // Without z value and normalized
            // Change for better results, if needed
            CalculateTransform(cam, look.position);
        }

        private void CalculateTransform(Camera cam, Vector4 look)
        {
            var viewport_position = cam.WorldToViewportPoint(look);
            viewport_position.x *= 2.0f;
            viewport_position.x -= 1.0f;
            viewport_position.y *= 2.0f;
            viewport_position.y -= 1.0f;

            var tod_viewport = new Vector2(viewport_position.x, viewport_position.y).normalized;

            if (viewport_position.z < 0)
            {
                tod_viewport.Set(-tod_viewport.x, -tod_viewport.y);
            }

            transform.localPosition =
                new Vector3(
                    radius * tod_viewport.x,
                    radius * tod_viewport.y,
                    transform.localPosition.z);
            transform.localRotation =
                Quaternion.Euler(
                    transform.localRotation.eulerAngles.x,
                    transform.localRotation.eulerAngles.y,
                    Mathf.Atan2(-tod_viewport.x, tod_viewport.y) * Mathf.Rad2Deg);
        }

        private Camera GetCamera()
        {
            return Camera.main;
        }

        private bool IsWithingFOV(Camera cam, Vector3 look_position)
        {
            return MathCalculations.IsWithinCameraFOV(cam, look_position, fovRatio);
        }


        public override void StartHighlightEffect(Transform position)
        {
            m_isLooking = true;
            m_highlightPoint = position;
        }

        public override void StopHighlightEffect()
        {
            m_isLooking = false;
            arrowRenderer.enabled = false;
        }
    }
}
