﻿using UnityEngine;
using System.Collections.Generic;

namespace Painter.Utils
{
    public class Trajectory : MonoBehaviour
    {
        public Transform endPoint;
        [Range(0, 1)]
        public float percentageVisible = 1;
        [Range(0, 1)]
        public float partialPercentageVisible = 0.25f;
        public float maxSegments = 50;
        // Length scale for each segment
        public float segmentScale = 1;
        // Reference to the LineRenderer we will use to display the simulated path
        public Color trajectoryColor = Color.red;

        public bool seeSecondaryTrajectory = false;
        [ConditionalField("seeSecondaryTrajectory", true, ComparisonType.Equals)]
        public Color secondaryTrajectoryColor = Color.blue;
        public bool seeRebounds = true;

        private LineRenderer sightLine;
        private LineRenderer secondarySightLine;
        // gameobject we're actually pointing at (may be useful for highlighting a target, etc.)
        private Collider _hitObject;
        public Collider hitObject { get { return _hitObject; } }

        private Vector3 currentGravity;
        private float drag;

        private void Start()
        {
            sightLine = GetComponent<LineRenderer>();
            findObjectGravity();
            findObjectDrag();

            secondarySightLine = transform.parent.Find("SecondaryTrajectory").GetComponent<LineRenderer>(); ;
        }
        void FixedUpdate()
        {
            if (!GameManager.instance.settings.seeTrajectory) {
                return;
            }

            UpdateTrajectory(transform.position, GameManager.instance.hologramForce, currentGravity);
            if (seeSecondaryTrajectory && TimeManager.instance.TimeStopped) {
                secondarySightLine.enabled = true;
                Vector3 initialVelocity = Vector3.zero;
                foreach(GameObject obj in TimeManager.instance.PausedObjects.Keys) {
                    if (obj == transform.parent.gameObject) {
                        initialVelocity = TimeManager.instance.PausedObjects[obj].velocity;
                        break;
                    }
                }

                UpdateSecondaryTrajectory(transform.position, initialVelocity, currentGravity);
            } else {
                secondarySightLine.enabled = false;
            }
        }
        

        private void findObjectGravity()
        {
            JsonableGravity grav = transform.parent.GetComponent<JsonableGravity>();

            if (grav == null) {
                currentGravity = Physics.gravity;
            } else {
                currentGravity = new Vector3(0, grav.Gravity, 0);
            }
        }

        private void findObjectDrag()
        {
            Rigidbody rigidbody = transform.parent.GetComponent<Rigidbody>();

            if (rigidbody == null) {
                drag = 1;
            } else {
                drag = rigidbody.drag;
            }
        }

        /// <summary>
        /// Simulate the path of a launched ball.
        /// Slight errors are inherent in the numerical method used.
        /// </summary>
        void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
        {
            //float timeDelta = 1.0f / initialVelocity.magnitude; // for example
            float timeDelta = segmentScale;

            //sightLine.positionCount = 0;
            
            Color startColor = trajectoryColor;
            Color endColor = startColor;
            startColor.a = 1;
            endColor.a = 0;
            sightLine.startColor = startColor;
            sightLine.endColor = endColor;

            Vector3 position = initialPosition;
            Vector3 velocity = initialVelocity;

            float percentageVisible = GameManager.instance.settings.seePartialTrajectory ? partialPercentageVisible : this.percentageVisible;
            int nSegments = Mathf.RoundToInt(maxSegments * percentageVisible);

            List<Vector3> positions = new List<Vector3>();

            for (int i = 0; i < nSegments; ++i) {
                positions.Add(position);

                if (positions.Count > 3 && Vector3.Distance(position, positions[i - 1]) < timeDelta &&
                    Vector3.Distance(position, positions[i - 2]) < timeDelta && 
                    Vector3.Distance(position, positions[i - 3]) < timeDelta) {
                    break;
                }
                
                Vector3 tempPosition = position + velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
                float distanceToNextPos = Vector3.Distance(position, tempPosition);
                Vector3 tempVelocity = velocity + gravity * timeDelta;
                tempVelocity *= Mathf.Clamp01(1f - 1.05f * timeDelta);

                int layerMask = CreateLayerMask(true, 14, 15, 16, 17); // cast against all layers except layers 14,15,16,17
                RaycastHit hit;
                if (seeRebounds && Physics.Raycast(position, tempVelocity, out hit, distanceToNextPos, layerMask)) {
                    // remember who we hit
                    _hitObject = hit.collider;
                    // set next position to the position where we hit the physics object
                    position = position + velocity.normalized * hit.distance;
                    // correct ending velocity, since we didn't actually travel an entire segment
                    velocity = velocity - currentGravity * (segmentScale - hit.distance) / velocity.magnitude;
                    // flip the velocity to simulate a bounce
                    velocity = Vector3.Reflect(velocity, hit.normal);
                    //print(_hitObject.gameObject.name);
                    
                } else {
                    position = tempPosition;
                    velocity = tempVelocity;
                }
            }

            //estimate the number of segments based on the approximate distance between each segment
            //float distanceSegment = Vector3.Distance(positions[0], positions[1]);
            //float distanceToEndPos = Vector3.Distance(initialPosition, endPoint.position);

            //int totalSegments = Mathf.RoundToInt(distanceToEndPos * 2.5f / distanceSegment);
            //int nSegments = Mathf.RoundToInt(totalSegments * percentageVisible);

            //positions.RemoveRange(nSegments, positions.Count - nSegments);

            sightLine.positionCount = positions.Count;
            sightLine.SetPositions(positions.ToArray());
        }

        void UpdateSecondaryTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
        {
            float timeDelta = segmentScale;

            Color startColor = secondaryTrajectoryColor;
            Color endColor = startColor;
            startColor.a = 0.75f;
            endColor.a = 0;
            secondarySightLine.startColor = startColor;
            secondarySightLine.endColor = endColor;

            Vector3 position = initialPosition;
            Vector3 velocity = initialVelocity;

            float percentageVisible = GameManager.instance.settings.seePartialTrajectory ? partialPercentageVisible : this.percentageVisible;
            int nSegments = Mathf.RoundToInt(maxSegments * percentageVisible);

            List<Vector3> positions = new List<Vector3>();

            for (int i = 0; i < nSegments; ++i) {
                positions.Add(position);

                if (positions.Count > 3 && Vector3.Distance(position, positions[i - 1]) < timeDelta &&
                    Vector3.Distance(position, positions[i - 2]) < timeDelta &&
                    Vector3.Distance(position, positions[i - 3]) < timeDelta) {
                    break;
                }

                Vector3 tempPosition = position + velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
                float distanceToNextPos = Vector3.Distance(position, tempPosition);
                Vector3 tempVelocity = velocity + gravity * timeDelta;
                tempVelocity *= Mathf.Clamp01(1f - 1.05f * timeDelta);

                int layerMask = CreateLayerMask(true, 14, 15, 16, 17); // cast against all layers except layers 16
                RaycastHit hit;
                if (seeRebounds && Physics.Raycast(position, tempVelocity, out hit, distanceToNextPos, layerMask)) {
                    // remember who we hit
                    _hitObject = hit.collider;
                    // set next position to the position where we hit the physics object
                    position = position + velocity.normalized * hit.distance;
                    // correct ending velocity, since we didn't actually travel an entire segment
                    velocity = velocity - currentGravity * (segmentScale - hit.distance) / velocity.magnitude;
                    // flip the velocity to simulate a bounce
                    velocity = Vector3.Reflect(velocity, hit.normal);

                } else {
                    position = tempPosition;
                    velocity = tempVelocity;
                }
            }

            //estimate the number of segments based on the approximate distance between each segment
            //float distanceSegment = Vector3.Distance(positions[0], positions[1]);
            //float distanceToEndPos = Vector3.Distance(initialPosition, endPoint.position);

            //int totalSegments = Mathf.RoundToInt(distanceToEndPos * 2.5f / distanceSegment);
            //int nSegments = Mathf.RoundToInt(totalSegments * percentageVisible);

            //positions.RemoveRange(nSegments, positions.Count - nSegments);

            secondarySightLine.positionCount = positions.Count;
            secondarySightLine.SetPositions(positions.ToArray());
        }

        private static int CreateLayerMask(bool aExclude, params int[] aLayers)
        {
            int v = 0;
            foreach (var L in aLayers)
                v |= 1 << L;
            if (aExclude)
                v = ~v;
            return v;
        }

    }
}