﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Painter.Utils
{
    public class ToolSelector : MonoBehaviour
    {
        [Header("SteamVR Handler")]
        public SteamVR_Action_Boolean toggleVisibilityBtn = null;
        [Header("Tools")]
        public GameObject hologramObject;
        public GameObject communicationPanelObject;
        [Header("Debugging")]
        public KeyCode toolToggleKey;

        private bool isHologramActivated = false;
        private bool isCommunicationPanelActivated = false;

        public bool IsHologramActivated { get => isHologramActivated; set => isHologramActivated = value; }
        public bool IsCommunicationPanelActivated { get => isCommunicationPanelActivated; set => isCommunicationPanelActivated = value; }

        // Start is called before the first frame update
        void Start()
        {
            toggleHologramVisibility(false);
            toggleCommunicationPanelVisibility(false);
        }

        // Update is called once per frame
        void Update()
        {
            //activate or deactivate hologram
            if (toggleVisibilityBtn != null && toggleVisibilityBtn.stateDown) {
                if (GameManager.instance.settings.seeCommunicationPanel) {
                    toggleCommunicationPanelVisibility(!isCommunicationPanelActivated);
                } else if (GameManager.instance.settings.seeHologram) {
                    toggleHologramVisibility(!isHologramActivated);
                }
                
            }

            if (Input.GetKeyDown(toolToggleKey)) {
                if (GameManager.instance.settings.seeCommunicationPanel) {
                    toggleCommunicationPanelVisibility(!isCommunicationPanelActivated);
                } else if (GameManager.instance.settings.seeHologram) {
                    toggleHologramVisibility(!isHologramActivated);
                }
            }
        }

        public void toggleHologramVisibility(bool newState)
        {
            isHologramActivated = newState;

            /*foreach (Transform child in transform) {
                child.gameObject.SetActive(isHologramActivated);
            }*/
            hologramObject.SetActive(isHologramActivated);

            GameManager.instance.seeTrajectory = isHologramActivated;
        }

        public void toggleCommunicationPanelVisibility(bool newState)
        {
            isCommunicationPanelActivated = newState;

            /*foreach (Transform child in transform) {
                child.gameObject.SetActive(isHologramActivated);
            }*/
            communicationPanelObject.SetActive(isCommunicationPanelActivated);
        }
    }
}