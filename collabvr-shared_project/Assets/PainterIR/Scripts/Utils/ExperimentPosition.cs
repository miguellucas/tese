﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Utils
{
    public class ExperimentPosition
    {
        private Transform position;
        private GameManager.Shot shot;
        private PositionCategory category;

        public enum PositionCategory
        {
            Distance_0_3,
            Distance_3_6,
            Distance_6_inf,
            Velocity_0_15,
            Velocity_15_inf,
            Time_Control,
            Collab_3_6,
            Collab_6_inf,
            Collab_Time_Control
        }

        public Transform Position { get => position; set => position = value; }
        public GameManager.Shot Shot { get => shot; set => shot = value; }
        public PositionCategory Category { get => category; set => category = value; }

        public ExperimentPosition(Transform position, GameManager.Shot shot, PositionCategory positionCategory)
        {
            this.position = position;
            this.shot = shot;
            this.category = positionCategory;
        }
    }
}