﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Painter.Utils;
using UnityEngine.SceneManagement;
public class MultipleVRInstances : MonoBehaviour
{

    private bool hasFocus = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!Application.isFocused /*&& !GameManager.instance.settings.isSinglePlayer && !GameManager.instance.settings.isObserver*/) {
            if (!hasFocus) return;
            StartCoroutine(SwitchOutOfVr());
            hasFocus = false;
            Debug.Log("Application lost focus");
        } else {
            if (hasFocus) return;
            Debug.Log(XRDevice.model);
            StartCoroutine(SwitchToVR());
            hasFocus = true;
            Debug.Log("Application gained focus");
        }
    }


    IEnumerator SwitchToVR()
    {
        XRSettings.LoadDeviceByName("OpenVR"); // Or whatever device you wanna use

        // Wait one frame!
        yield return null;

        // Now it's ok to enable VR mode.
        XRSettings.enabled = true;
        SceneManager.LoadScene("LoadPainterVR", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("LoadPainterDesktop");
    }

    IEnumerator SwitchOutOfVr()
    {
        XRSettings.LoadDeviceByName(""); // Empty string loads the "None" device.

        // Wait one frame!
        yield return null;

        SceneManager.LoadScene("LoadPainterDesktop", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("LoadPainterVR");
    }
}
