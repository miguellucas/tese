﻿using UnityEngine;
using System;

namespace Painter.Utils
{
    [Serializable]
    public class Signal
    {
        public SerializableVector3 position;
        public SerializableVector3 velocity_vectorial;
        public float velocity;
        public int signal_type;
        public string special_signal;

        public Signal(Vector3 position, Vector3 velocity, int signal_type)
        {
            this.position = position;
            this.velocity_vectorial = velocity;
            this.velocity = velocity.magnitude;
            this.signal_type = signal_type;
            this.special_signal = "";
        }

        public Signal(string special_signal)
        {
            this.position = Vector3.zero;
            this.velocity_vectorial = Vector3.zero;
            this.velocity = 0;
            this.signal_type = 0;
            this.special_signal = special_signal;
        }
    }
}