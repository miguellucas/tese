﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

namespace Painter.Network.Utils
{
    /// <summary>
    /// This class is responsible for handling the instantiation data for certain GameObjects
    /// </summary>
    public class NetworkInstantiationDataHandler : MonoBehaviour, IPunInstantiateMagicCallback
    {
        /// <summary>
        /// Coroutine that waits for a parent object to be spawned in and attaches itself to it.
        /// </summary>
        /// <param name="parentName">The search is done through the name of the parent GameObject.</param>
        /// <returns></returns>
        private IEnumerator FindParent(string parentName)
        {
            yield return null;

            GameObject parentObject;
            while(!(parentObject = GameObject.Find(parentName)))
            {
                yield return null;
            }
            
            transform.parent = parentObject.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }


        public virtual void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            object[] data = gameObject.GetPhotonView().InstantiationData;
            if (data != null && data.Length > 1)
            {
                {
                    string parentName = (string)data[0];
                    if(parentName != "")
                        StartCoroutine(FindParent(parentName));
                }
                {
                    string mName = (string)data[1];
                    gameObject.name = mName;
                }
            }
        }


        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}
