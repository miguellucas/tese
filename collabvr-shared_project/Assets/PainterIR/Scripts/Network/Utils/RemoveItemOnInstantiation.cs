﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

namespace Painter.Network.Utils
{
    /// <summary>
    /// This class is responsible for handling the instantiation data for certain GameObjects
    /// </summary>
    public class RemoveItemOnInstantiation : NetworkInstantiationDataHandler
    {
        public MonoBehaviour[] removeBehaviours;

        private void RemoveBehaviours()
        {
            foreach(var behaviour in removeBehaviours)
            {
                Destroy(behaviour);
            }
        }

        public override void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            if (info.Sender != PhotonNetwork.LocalPlayer)
                RemoveBehaviours();
            base.OnPhotonInstantiate(info);
        }
    }
}
