﻿using UnityEngine;

namespace Painter.Network.Utils
{
    /// <summary>
    /// This is a simple follow class that will move according to another Transform.
    /// </summary>
    public class GraphicsObject : MonoBehaviour
    {
        /// <summary>
        /// The object to follow.
        /// </summary>
        public Transform follow;

        private void LateUpdate()
        {
            transform.position = follow.position;
            transform.rotation = follow.rotation;
        }
    }
}
