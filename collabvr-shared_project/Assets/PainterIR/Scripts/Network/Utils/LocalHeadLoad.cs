﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class LocalHeadLoad : MonoBehaviour
{
    public Material invisibleShadowCaster;    
    
    private void Start()
    {
        PhotonView photonView = GetComponent<PhotonView>();
        if (!photonView.IsMine)
            return;
        ChangeMaterial(invisibleShadowCaster, this.transform);
    }

    private static void ChangeMaterial(Material newMaterial, Transform transform)
    {
        // Depth first approach
        foreach(Transform child in transform)
        {
            ChangeMaterial(newMaterial, child);
        }

        // Change the material
        MeshRenderer renderer = transform.gameObject.GetComponent<MeshRenderer>();
        if (renderer == null)
            return;
        renderer.material = newMaterial;
    }
}
