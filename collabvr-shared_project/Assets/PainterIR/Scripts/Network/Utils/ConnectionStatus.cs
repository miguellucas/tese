﻿using UnityEngine;
using Photon.Pun;
using System.Collections;
using UnityEngine.Events;
using Photon.Realtime;

namespace Painter.Network.Utils
{
    /// <summary>
    /// ConnectionStatus is a singleton object that should be placed in a scene only once.
    /// It checks for the connection status of a client and notifies all other script using an UnityEvent eveytime the user leaves and joins a room.
    /// </summary>
    public class ConnectionStatus : MonoBehaviourPunCallbacks
    {
        #region Impose Singleton 

        private static ConnectionStatus instance;
        public static ConnectionStatus Instance
        {
            get
            {
                return instance;
            }
        }
        
        #endregion

        /// <summary>
        /// UnityEvent that is sent with a boolean indicating if a client just left or joined and room
        /// </summary>
        public class RoomUpdate : UnityEvent<bool> { };
        public RoomUpdate OnRoomUpdate;

        private bool isConnecting = false;
        private byte maxPlayersPerRoom = 4;
        private readonly string roomName = "CIRP Demo";

        /// <summary>
        /// Class contructor that checks if the singleton instance is already created.
        /// </summary>
        public ConnectionStatus()
        {
#if DEBUG
            if (instance != null)
            {
                Debug.LogError("<b>ConnectionStatus</b>: Multiple instances of ConnectionStatus are not supported.");
            }
#endif
            instance = this;

            if (OnRoomUpdate == null)
                OnRoomUpdate = new RoomUpdate();
        }

        public void OnDestroy()
        {
            instance = null;
        }

        /// <summary>
        /// Called from the OnJoinedRoom function and waits for a player to be ready before sending out an UnityEvent notifying connection.
        /// </summary>
        private IEnumerator WaitForPlayerReady()
        {
            while(!PhotonNetwork.IsConnectedAndReady)
            {
                yield return null;
            }

            OnRoomUpdate.Invoke(true);
        }

        #region Photon Callbacks

        public override void OnJoinedRoom()
        {
            StartCoroutine(WaitForPlayerReady());
        }

        public override void OnLeftRoom()
        {
            OnRoomUpdate.Invoke(false);
        }
        
        public override void OnConnectedToMaster()
        {
            Debug.Log("Launcher:OnConnectedToMaster() was called by PUN");
            if (isConnecting)
            {
                // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            // progressLabel.SetActive(false);
            // mainPanel.SetActive(true);
            Debug.LogWarningFormat("Launcher:OnDisconnected() was called by PUN with reason {0}", cause);
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.x");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            PhotonNetwork.CreateRoom(roomName, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }

        #endregion
    }
}
