﻿using UnityEngine;
using Photon.Pun;
using System;
using System.Collections;

namespace Painter.Network.Utils
{
    [Serializable]
    public struct InstantiationPrefabs
    {
        /// <summary>
        /// This prefab is used for when the player is in a local scene (not connected)
        /// </summary>
        public GameObject localPrefab;
        /// <summary>
        /// This string represents the name of the prefab stored in the resources folder
        /// This is the way Photon loads a gameobject in all clients of a room
        /// </summary>
        public string networkPrefab;
    }
    
    [Serializable]
    public struct InstantiationInfo
    {
        public InstantiationPrefabs prefabs;
        
        public NetworkInstantiationData networkData;
        public LocalInstantiationData localData;

        public Vector3 position;
        public Quaternion rotation;
    }
    
    [Serializable]
    public struct NetworkInstantiationData
    {
        public string parentName;
        public string gameObjectName;
    }
    
    [Serializable]
    public struct LocalInstantiationData
    {
        public Transform parent;
        public string name;
    }
    
    public abstract class NetworkInstantiation
    {
        /// <summary>
        /// Instantiate a local game object using the GameObject.Instantiate method.
        /// </summary>
        /// <param name="info">The information that is used to know the type of instantiation and the instantiation values.</param>
        /// <returns>The game object that was instantiated</returns>
        private static GameObject InstantiateLocal(InstantiationInfo info)
        {
            GameObject obj = GameObject.Instantiate(info.prefabs.localPrefab, info.position, info.rotation, info.localData.parent);
            obj.name = info.localData.name;
            return obj;
        }

        /// <summary>
        /// Instantiate a networked object using the PhotonNetwork.Instantiate method.
        /// </summary>
        /// <param name="info">The information that is used to know the type of instantiation and the instantiation values.</param>
        /// <returns>The game object that was instantiated</returns>
        private static GameObject InstantiateNetworked(InstantiationInfo info)
        {
            GameObject obj = PhotonNetwork.Instantiate(info.prefabs.networkPrefab, info.position, info.rotation, 0, new[]{ info.networkData.parentName, info.networkData.gameObjectName });
            return obj;
        }

        /// <summary>
        /// Using the instantion info the function decides whether it should instantiate a local object or a networked one.
        /// Networked object should use the instantiation data sent that let's them know the name of the parent object and
        /// the name of the object that was spawned.
        /// </summary>
        /// <param name="info">The information that is used to know the type of instantiation and the instantiation values.</param>
        /// <returns>The game object that was instantiated</returns>
        public static GameObject Instantiate(InstantiationInfo info)
        {
            if(info.prefabs.localPrefab != null)
            {
                return InstantiateLocal(info);
            }
            else
            {
                return InstantiateNetworked(info);
            }
        }
    }
}
