﻿using Photon.Pun;
using UnityEngine;

namespace Painter.Core
{
    /// <summary>
    /// This class can be added to a GameObject with the intent of calling RPC methods on the owner of this PhotonView.
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    public class RPCMethodsAtOwner : MonoBehaviour
    {
        private PhotonView photonView;
        
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }

        public void RunRPCOnMe(string methodName, params object[] parameters)
        {
            photonView.RPC(methodName, photonView.Owner, parameters);
        }
    }
}
