﻿using Photon.Pun;
using Photon.Realtime;

namespace Painter.Core
{
    /// <summary>
    /// Interface that is used for calling event functions related to the ownership of objects.
    /// The script that calls these events is Ownership_Handler.
    /// A script implementing this interface should be placed on objects that have have ownership transfers.
    /// </summary>
    public interface IOwnershipReceiver
    {
        /// <summary>
        /// When the ownership of the object is requested this function is called.
        /// </summary>
        /// <param name="requestingPlayer">The client that is making the request</param>
        void OwnershipRequested(Player requestingPlayer);

        /// <summary>
        /// When the ownership of the object is transfered to another client this function is called.
        /// </summary>
        /// <param name="previousOwner">The client that previouslt owned this object</param>
        void OwnershipTransfered(Player previousOwner);
    }
}
