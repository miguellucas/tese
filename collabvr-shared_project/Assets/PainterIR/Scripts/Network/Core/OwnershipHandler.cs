﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Painter.Core
{
    /// <summary>
    /// This script should be placed once in every networked scene.
    /// It calls the methods in scripts that implement IOwnership_Receiver (if any) when there's a Photon Ownership callback related to that object.
    /// </summary>
    public class OwnershipHandler : MonoBehaviour, IPunOwnershipCallbacks
    {
        public void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        public void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        /// <summary>
        /// The ownership for an object has been requested.
        /// Call the OwnershipRequested function on all scripts that implement it in that gameobject.
        /// </summary>
        /// <param name="targetView">The view from the object that has it's ownership being requested</param>
        /// <param name="requestingPlayer">The client that is requesting the ownership</param>
        public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
        {
            var receivers = targetView.GetComponents<IOwnershipReceiver>();
            foreach (var receiver in receivers)
                receiver.OwnershipRequested(requestingPlayer);
        }

        /// <summary>
        /// The ownership for an object has been transfered.
        /// Call the OwnershipTransfered function on all scripts that implement it in that gameobject.
        /// </summary>
        /// <param name="targetView">The view from the object that had it's ownership transfered</param>
        /// <param name="requestingPlayer">The client that is previously owned this object</param>
        public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
        {
            var receivers = targetView.GetComponents<IOwnershipReceiver>();
            foreach(var receiver in receivers)
            {
                receiver.OwnershipTransfered(previousOwner);
            }
        }
    }
}
