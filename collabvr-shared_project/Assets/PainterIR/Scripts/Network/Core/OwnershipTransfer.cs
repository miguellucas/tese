﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;

namespace Painter.Core
{
    public delegate void OnOwnershipTransfered();
    /// <summary>
    /// Ownershi_Receiver is an implementation of IOwnership_Receiver for Pun based networking
    /// This class gives ownership of an object to which ever player is hovering the object 
    /// when the current owner is not using it.
    /// The first player in the list will be the next owner.
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    public class OwnershipTransfer : MonoBehaviourPunCallbacks, IOwnershipReceiver
    {
        private bool isConnected;

        private List<KeyValuePair<Player, int>> contestingPlayers;
        
        private OnOwnershipTransfered callbacks;


        /// <summary>
        /// Separate thread that runs until the player is connected and ready to play
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitConnect()
        {
            while (!PhotonNetwork.IsConnectedAndReady)
            {
                yield return null;
            }

            isConnected = true;
        }
        

        public override void OnEnable()
        {
            contestingPlayers = new List<KeyValuePair<Player, int>>();
            StartCoroutine(WaitConnect());
        }

        public override void OnDisable()
        {
            contestingPlayers.Clear();
            StopAllCoroutines();
        }



        public void AskOwnershipTransfer(Player player, bool isInteraction, OnOwnershipTransfered oot = null)
        {
            if(oot != null)
                callbacks += oot;
            photonView.RPC("RegisterOwnershipRequest", RpcTarget.All, isInteraction);
        }

        /// <summary>
        /// This function can be called by other scripts to change a player's contesting status.
        /// The contesting is done in order of arrival.
        /// If a player's status reachs 0, then the player is removed from the list.
        /// </summary>
        /// <param name="requestingPlayer"></param>
        /// <parem name="isGrab"></parem>
        [PunRPC]
        private void RegisterOwnershipRequest(bool isGrab, PhotonMessageInfo info)
        {
            PlayerContesting(info.Sender, isGrab);
        }



        /// <summary>
        /// This function determines whether or not the ownership should be transfered
        /// to another player. It can be the next player in line, or whatever other method.
        /// Child classes can override this method for some custom logic.
        /// </summary>
        protected virtual void TryToTransferOwnership()
        {
            // Ignore if:
            // It is not mine (IsMine can be true if this is an element of the scene)
            if ((!photonView.IsSceneView && !photonView.IsMine) ||
            // OR constesting players is empty
                contestingPlayers.Count <= 0 ||
            // OR (since I'm the owner) the next element is me (I can be the owner if this is an element of the scene)
                (contestingPlayers[0].Key == PhotonNetwork.LocalPlayer))
            {
                return;
            }

            // Transfer ownership to the next player in the list
            photonView.TransferOwnership(contestingPlayers[0].Key);
        }

        /// <summary>
        /// This is an highly inefficient method of AT LEAST O(n)
        /// I just don't know the complexity of the List.RemoveAt(int) method
        /// </summary>
        /// <param name="contesting">The client that is contesting this object for ownership</param>
        /// <param name="isInteraction">Boolean flag that let's the logic know if the score should increment instead of decrementing</param>
        private void UpdatePlayers(Player contesting, bool isInteraction)
        {
            int index = 0;
            foreach(KeyValuePair<Player, int> players in contestingPlayers)
            {
                if(players.Key == contesting)
                {
                    int nextValue = players.Value;
                    nextValue += isInteraction ? 1 : -1;

                    if (nextValue <= 0)
                    {
                        contestingPlayers.RemoveAt(index);
                    }
                    else
                    {
                        contestingPlayers[index] = new KeyValuePair<Player, int>(players.Key, nextValue);
                    }
                    return;
                }
                index++;
            }

            if(isInteraction)
            {
                contestingPlayers.Add(new KeyValuePair<Player, int>(contesting, 1));
            }
        }

        /// <summary>
        /// This function can be called by other scripts to change a player's contesting status.
        /// The contesting is done in order of arrival.
        /// If a player's status reachs 0, then the player is removed from the list.
        /// </summary>
        /// <param name="player"></param>
        /// <parem name="isIncrement"></parem>
        private void PlayerContesting(Player player, bool isInteraction)
        {
            if (!isConnected)
                return;

            UpdatePlayers(player, isInteraction);
            TryToTransferOwnership();
        }

        /// <summary>
        /// This function can get called to check if the ownership should be transfered at any given time.
        /// </summary>
        public void OwnershipCheck()
        {
            TryToTransferOwnership();
        }


        public virtual void OwnershipRequested(Player requestingPlayer)
        {
            if (!isConnected)
                return;
            
            UpdatePlayers(requestingPlayer, true);
            TryToTransferOwnership();
        }

        public virtual void OwnershipTransfered(Player previousOwner)
        {
            if (photonView.IsMine && callbacks != null)
            {
                callbacks.Invoke();
                foreach(OnOwnershipTransfered c in callbacks.GetInvocationList())
                    callbacks -= c;
            }
        }
    }
}
