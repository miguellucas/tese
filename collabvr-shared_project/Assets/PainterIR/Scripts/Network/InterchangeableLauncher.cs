﻿//using Painter.AR.Core;
using Painter.Utils;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterchangeableLauncher : MonoBehaviourPunCallbacks
{
    public bool forceVRLoading = false;
    public bool forceDesktopLoading = false;
    #region Private Fields

    /// <summary>
    /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
    /// </summary>
    private string gameVersion = "1";

    /// <summary>
    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    /// </summary>
    private bool isConnecting;

    private byte maxPlayersPerRoom = 24;



    #endregion


    // Start is called before the first frame update
    IEnumerator Start()
    {
        if (forceDesktopLoading) {
            SceneManager.LoadScene("LoadPainterDesktop", LoadSceneMode.Additive);
        } else if (forceVRLoading) {
            if (!SceneManager.GetSceneByName("LoadPainterVR").isLoaded) {
                SceneManager.LoadScene("LoadPainterVR", LoadSceneMode.Additive);
            }
        } else {
            if (PlayerUtils.IsDeviceAR()) {
                SceneManager.LoadScene("LoadPainterAR", LoadSceneMode.Additive);
            } else {
                if (PlayerUtils.IsDeviceAttached()) {
                    if (!SceneManager.GetSceneByName("LoadPainterVR").isLoaded) {
                        SceneManager.LoadScene("LoadPainterVR", LoadSceneMode.Additive);
                    }
                } else {
                    if (!SceneManager.GetSceneByName("LoadPainterDesktop").isLoaded) {
                        SceneManager.LoadScene("LoadPainterDesktop", LoadSceneMode.Additive);
                    }
                }
            }
        }

        yield return new WaitForSecondsRealtime(2.0f);

        Connect();
    }


    #region Public Methods

    /// <summary>
    /// Start the connection process.
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    /// </summary>
    public void Connect()
    {
        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            // PhotonNetwork.GameVersion = gameVersion;
            // PhotonNetwork.ConnectUsingSettings();
        
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    #endregion




    // below, we implement some callbacks of the Photon Realtime API.
    // Being a MonoBehaviourPunCallbacks means, we can override the few methods which are needed here.


    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room. Calling: PhotonNetwork.JoinRandomRoom();");
        PhotonNetwork.JoinOrCreateRoom("room", new RoomOptions() { MaxPlayers = 0 }, null);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby(). This client is connected and does get a room-list, which gets stored as PhotonNetwork.GetRoomList(). This script now calls: PhotonNetwork.JoinRandomRoom();");
        PhotonNetwork.JoinOrCreateRoom("room", new RoomOptions() { MaxPlayers = 0 }, null);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed() was called by PUN. No random room available, so we create one. Calling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
        PhotonNetwork.CreateRoom("room", new RoomOptions() { MaxPlayers = 0 }, null);
    }

    // the following methods are implemented to give you some context. re-implement them as needed.
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("OnDisconnected(" + cause + ")");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name);
        Debug.Log(PhotonNetwork.CurrentRoom.MaxPlayers);
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        Debug.Log("OnJoinedRoom() called by PUN. Now this client is in a room. From here on, your game would be running.");
    }



#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(InterchangeableLauncher), true)]
    public class ConnectAndJoinRandomInspector : Editor
    {
        void OnEnable() { EditorApplication.update += Update; }
        void OnDisable() { EditorApplication.update -= Update; }

        bool IsConnectedCache = false;

        void Update()
        {
            if (IsConnectedCache != PhotonNetwork.IsConnected)
            {
                Repaint();
            }
        }

        public override void OnInspectorGUI()
        {
            this.DrawDefaultInspector(); // Draw the normal inspector

            if (Application.isPlaying && !PhotonNetwork.IsConnected)
            {
                if (GUILayout.Button("Connect"))
                {
                    ((InterchangeableLauncher)this.target).Connect();
                }
            }
        }
    }
#endif

    private void Update()
    {

        
    }

    
}
