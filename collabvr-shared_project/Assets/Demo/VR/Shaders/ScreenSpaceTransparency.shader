﻿Shader "Highlight/Screen Space Transparency"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

		_LeftScreenPosition("Left Screen Position", Vector) = (0, 0, 0, 0)
		_RightScreenPosition("Right Screen Position", Vector) = (0, 0, 0, 0)

		_OutlineColor("Outline Color", Color) = (0,0,1,1)
		_OutlineWidth("Outline Width", Range(0,1)) = 0.05
        _NoiseTex ("Outline Noise", 2D) = "black" {}
		_ScreenMinDistance("Screen Min Distance", float) = 0.1
		_ScreenMaxDistance("Screen Max Distance", float) = 0.3

    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 200        
		
        // Set up alpha blending
        Blend SrcAlpha OneMinusSrcAlpha

		// extra pass that renders to depth buffer only
		Pass {
			ZWrite On
			ColorMask 0
		}

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _NoiseTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_NoiseTex;
			float4 screenPos;
			float3 viewDir;
			float3 worldNormal;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

		float4 _LeftScreenPosition;
		float4 _RightScreenPosition;

		fixed4 _OutlineColor;
		fixed _OutlineWidth;
		float _ScreenMinDistance;
		float _ScreenMaxDistance;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

		inline void Remap(float In, float2 InMinMax, float2 OutMinMax, out float Out)
		{
			Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			// Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
			o.Alpha = 1.0;


			float dNV = 1.0 - dot(IN.viewDir, IN.worldNormal);
						
			float3 fragScreenPosition = IN.screenPos.xyz / IN.screenPos.w;
			float2 fragScreenUV = fragScreenPosition.xy;
			float mAlpha;
			float3 sposition;

#if UNITY_SINGLE_PASS_STEREO

			float4 scaleOffset = unity_StereoScaleOffset[unity_StereoEyeIndex];
			fragScreenUV = (fragScreenUV - scaleOffset.zw) / scaleOffset.xy;

			sposition = unity_StereoEyeIndex == 0 ? 
				_LeftScreenPosition.xyz : 
				_RightScreenPosition.xyz;

#else

			sposition = _LeftScreenPosition.xyz;

#endif

			float bias = 0.2;
			mAlpha = IN.screenPos.w - IN.screenPos.z - bias <= sposition.z ?
				distance(fragScreenUV, sposition.xy) :
				_ScreenMaxDistance;

			float mTime = _Time.x - floor(_Time.x);
			float2 noiseUV = IN.uv_NoiseTex + normalize(sposition.xy - fragScreenUV) * mTime;

			fixed distBias = tex2D(_NoiseTex, noiseUV).r * (_ScreenMaxDistance - _ScreenMinDistance);
			o.Albedo = mAlpha < _ScreenMinDistance + distBias + _OutlineWidth ? _OutlineColor : o.Albedo;
			mAlpha = mAlpha < _ScreenMinDistance + distBias ? 0.0 : 1.0;
            o.Alpha = max(mAlpha, dNV);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
