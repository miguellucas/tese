﻿Shader "Placement/Error"
{
    Properties
    {
		_Color ("Color", Color) = (1,1,1,1)

		_FresnelScale ("Fresnel Scale", Float) = 0.5
		_FresnelPower ("Fresnel Power", Float) = 1

		_BiasMaximum ("Fresnel Bias Maximum", Range(0.25, 1)) = 0.5
		_BiasScale ("Fresnel Bias Scale", Range(0.05, 0.25)) = 0.2
    }
    SubShader
    {		
		Tags { "RenderType" = "Transparent" "Queue" = "Overlay" "IgnoreProjector" = "True"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
			ZWrite Off 
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members fresnel)
// #pragma exclude_renderers d3d11
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				float fresnel : TEXCOORD1;
			};

			fixed4 _Color;
			fixed _MTime;
			
			fixed _IncrementThreshold;

			fixed _FresnelScale;
			fixed _FresnelPower;

			fixed _BiasMaximum;
			fixed _BiasScale;

            v2f vert (appdata v)
            {
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
                o.uv = v.uv;

				float3 i = normalize(ObjSpaceViewDir(v.pos));
				o.fresnel = (_BiasMaximum - (_CosTime.w * _BiasScale) + _FresnelScale * pow(1 + dot(i, v.normal), _FresnelPower));
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 color = _Color;
				color.a = saturate(1 - i.fresnel);
                return color;
            }
            ENDCG
        }
    }
}
