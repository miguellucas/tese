﻿Shader "Highlight/1 Transparency"
{
    Properties
    {
		_Color ("Color", Color) = (1,1,1,1)
		_MTime ("Time", Range(0, 1)) = 0

		_IncrementThreshold ("Time increment threshold", Range(0, 1)) = 0

		_FresnelScale ("Fresnel Scale", Float) = 1
		_FresnelPower ("Fresnel Power", Float) = 1
    }
    SubShader
    {		
		Tags { "RenderType" = "Transparent" "Queue" = "Overlay" "IgnoreProjector" = "True"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
			ZWrite Off 
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members fresnel)
// #pragma exclude_renderers d3d11
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				float fresnel : TEXCOORD1;
			};

			fixed4 _Color;
			fixed _MTime;
			
			fixed _IncrementThreshold;

			fixed _FresnelScale;
			fixed _FresnelPower;

            v2f vert (appdata v)
            {
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
                o.uv = v.uv;

				float oneMinusIncrement = 1.0 - _IncrementThreshold;
				float bias = _MTime < _IncrementThreshold ? 
					1.0 - ( _MTime / _IncrementThreshold ) : 
					((_MTime - _IncrementThreshold) / oneMinusIncrement);
				float3 i = normalize(ObjSpaceViewDir(v.pos));
				o.fresnel = (bias + _FresnelScale * pow(1 + dot(i, v.normal), _FresnelPower));
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 color = _Color;
				color.a = saturate(1 - i.fresnel);
                return color;
            }
            ENDCG
        }
    }
}
