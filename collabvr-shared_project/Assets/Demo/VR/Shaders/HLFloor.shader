﻿Shader "Highlight/Floor"
{
    Properties
    {
		_MTime("Time", Range(0, 1)) = 0
		_Position("Position", Vector) = (0,0,0,1)

		_EffectWidth("Effect Width", Range(0, 10)) = 0.5
		_EffectMaximumDistance("Effect Maximum Distance", Range(1, 1000)) = 5
		_EffectColor("Effect Color", Color) = (1,1,1,1)
    }
    SubShader
    {		
		Tags { "RenderType" = "Transparent" "Queue" = "Overlay" "IgnoreProjector" = "True"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
			ZWrite Off 
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members fresnel)
// #pragma exclude_renderers d3d11
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 color: TEXCOORD1;
			};

			fixed _MTime;
			fixed4 _Position;

			fixed _EffectWidth;
			fixed _EffectMaximumDistance;
			fixed4 _EffectColor;

            v2f vert (appdata v)
            {
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
				o.color = mul(unity_ObjectToWorld, v.pos);
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 color = _EffectColor;
				
				fixed half_width = _EffectWidth / 2;

				// Given the time calculate the perfect distance
				fixed perfect_distance = lerp(_EffectMaximumDistance, -half_width, _MTime);

				// Given the fragment distance to the position, the perfect distance
				// and the width, calculate if the fragment should be shown
				fixed m_distance = distance(i.color, _Position);

				// Between 0 and _EffectWidth is inside
				fixed relative_width = (m_distance - perfect_distance) + _EffectWidth;
				color.a = relative_width < 0 || relative_width > _EffectWidth ? 0 : 1;
				
				//color.a = 1;

                return color;
            }
            ENDCG
        }
    }
}
