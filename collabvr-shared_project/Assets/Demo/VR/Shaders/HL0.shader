﻿Shader "Highlight/0 Transparency"
{
    Properties
    {
		_Color ("Color", Color) = (1,1,1,1)
		_MTime ("Time", Range(0, 1)) = 0

		_NoiseTexture ("Noise", 2D) = "white" {}
    }
    SubShader
    {		
		Tags { "RenderType" = "Transparent" "Queue" = "Overlay" "IgnoreProjector" = "True"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
			ZWrite Off 
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			sampler2D _NoiseTexture;
			fixed4 _Color;
			fixed _MTime;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 color = _Color;
				fixed alphaCutout = tex2D(_NoiseTexture, i.uv).r;
				if(_MTime > 0.0 && _MTime >= alphaCutout) color.a = 0.0;
                return color;
            }
            ENDCG
        }
    }
}
