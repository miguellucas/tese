﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Player/See Behind Walls" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_OtherColor("Color Behind Objects", Color) = (1,1,1,1)
		_OutlineColor("Outline Color", Color) = (1,0,1,1)
		_OutlineAmount("Outline Amount", Range(1, 100)) = 1.1
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		// Draw when behind other objects
		ZWrite Off
		ZTest Greater

		CGPROGRAM
		#pragma surface surf NoLighting vertex:vert
		#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		fixed4 _OutlineColor;
		fixed _OutlineAmount;

		void vert(inout appdata_full v) {
			v.vertex.xyz = v.vertex.xyz * _OutlineAmount;
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = _OutlineColor.rgb;
			o.Alpha = _OutlineColor.a;
		}

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}
		ENDCG

		// Draw when behind other objects
		ZWrite Off
		ZTest Greater

		CGPROGRAM
		#pragma surface surf NoLighting
		#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		fixed4 _OtherColor;

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = _OtherColor.rgb;
			o.Alpha = _OtherColor.a;
		}

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		ENDCG

		// Draw when in front of other objects
		ZWrite On
		ZTest Less
		CGPROGRAM

		#pragma surface surf Standard
		
		sampler2D _MainTex;

		struct Input
		{
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}

		ENDCG
	}
	FallBack "Diffuse"
}