﻿using System.Collections;
using UnityEngine;

namespace Painter.Demo
{
    public class PunchableFace : MonoBehaviour
    {
        public float recoveryForce;

        protected Vector3 initialForce;
        protected Vector3 accumulatedForce;

        private IEnumerator m_punchCoroutine = null;

        public void PunchFace(Vector3 force)
        {
            if (m_punchCoroutine == null)
            {
                UpdatePunchForce(force);

                m_punchCoroutine = PunchCoroutine();
                StartCoroutine(m_punchCoroutine);
            }
        }

        protected virtual void UpdatePunchForce(Vector3 newForce)
        {
            accumulatedForce = newForce;
            initialForce = newForce.normalized;
        }

        protected virtual IEnumerator PunchCoroutine()
        {
            transform.localPosition +=
                accumulatedForce * Time.deltaTime;

            bool isReturning = false;

            while (transform.localPosition != Vector3.zero)
            {
                yield return new WaitForFixedUpdate();

                // var dot_prod = Vector3.Dot(accumulatedForce.normalized, transform.localPosition.normalized);
                // var position_to_origin = transform.localPosition.magnitude;
                var step_distance = recoveryForce * Time.deltaTime;
                
                if(!isReturning)
                {
                    accumulatedForce -= initialForce * step_distance;

                    if (accumulatedForce.magnitude < step_distance)
                    {
                        accumulatedForce = Vector3.zero;
                        isReturning = true;
                    }
                    else
                    {
                        transform.localPosition +=
                            accumulatedForce * Time.fixedDeltaTime;
                    }

                }

                if (isReturning)
                {
                    if (transform.localPosition.magnitude < step_distance)
                    {
                        transform.localPosition = Vector3.zero;
                    }
                    else
                    {
                        transform.localPosition -= transform.localPosition.normalized * step_distance;
                    }
                }

            }

            m_punchCoroutine = null;
        }
    }
}
