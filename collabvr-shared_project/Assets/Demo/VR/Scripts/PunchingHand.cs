﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Painter.Demo
{
    public class PunchingHand : MonoBehaviour
    {
        public float punchingForce = 1.0f;

        private HashSet<PunchableFace> m_punchableFaces = new HashSet<PunchableFace>();

        private void OnTriggerEnter(Collider other)
        {
            var face = other.GetComponent<PunchableFace>();
            if (face != null)
            {
                if(m_punchableFaces.Contains(face))
                {
                    return;
                }

                m_punchableFaces.Add(face);
                face.PunchFace((other.bounds.center - transform.position).normalized * punchingForce);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var face = other.GetComponent<PunchableFace>();
            if(face != null)
            {
                m_punchableFaces.Remove(face);
            }
        }
    }
}
